INSERT INTO role (name)
VALUES ('ROLE_ADMIN');
INSERT INTO role (name)
VALUES ('ROLE_TRANSACTION_STAFF');
INSERT INTO role (name)
VALUES ('ROLE_EDUCATION_STAFF');
INSERT INTO role (name)
VALUES ('ROLE_MENTOR');
INSERT INTO role (name)
VALUES ('ROLE_MENTEE');

-- insert data for table User
INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('dvx123', '1234Abc#', 'Dương Việt Xô', '904559269', NULL, TRUE, 0, '1979-11-21', NULL, 'gdtuongan@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('đtla123', '1234Abc#', 'Đào Thị Lan Anh', '961716320', NULL, TRUE, 1, '1983-01-30', NULL, 'anh.dtl0183@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ntth123', '1234Abc#', 'Nguyễn Thị Thúy Hồng', '969235986', NULL, TRUE, 1, '1982-09-01', NULL, 'hongnguyenthithuy82@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ntt123', '1234Abc#', 'NGUYỄN THỊ TÂM', '886962299', NULL, TRUE, 1, '1990-08-14', NULL, 'tamnt0890@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('pxh123', '1234Abc#', 'Phạm Xuân Hiếu', '359092717', NULL, TRUE, 0, '1999-11-26', NULL, 'hieuphamxuan1999@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ltht123', '1234Abc#', 'Lê Thị Hoài Thu', '967295572', NULL, TRUE, 1, '1995-02-07', NULL, 'Lhthu7295@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ttbh123', '1234Abc#', 'Trần Thị Bích Hằng', '981756159', NULL, TRUE, 1, '1977-06-26', NULL, 'tbhang@yahoo.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('tlt123', '1234Abc#', 'Trần Lê Tuấn', '943434993', NULL, TRUE, 0, '1993-01-28', NULL, 'tuantrann93@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ttt123', '1234Abc#', 'Trần Thanh Trang', '967266396', NULL, TRUE, 1, '1996-04-01', NULL, 'THANHTRANG.SINGER175@GMAIL.COM', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ttl123', '1234Abc#', 'Trần Thị Loan', '973897829', NULL, TRUE, 1, '1990-06-02', NULL, 'huongngoclan.atgt@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ttnm123', '1234Abc#', 'Trương Thị Ngọc Minh', '913930069', NULL, TRUE, 1, '1991-09-01', NULL, 'ngocminh19ftu@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ntnh123', '1234Abc#', 'Nguyễn Thị Ngọc Hà', '903233498', NULL, TRUE, 1, '1980-11-13', NULL, 'ngochariver@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('nvs123', '1234Abc#', 'Nguyễn Văn Sơn', '344997788', NULL, TRUE, 0, '1974-10-06', NULL, 'sonnguyen061974@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ntd123', '1234Abc#', 'Nguyễn Thị Dung', '948718929', NULL, TRUE, 1, '1981-04-20', NULL, 'nguyenthidungaia78@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ntth123', '1234Abc#', 'Nguyễn Thị Thanh Hà', '888818883', NULL, TRUE, 1, '1986-07-12', NULL, 'nguyen.vivian8883@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('btth123', '1234Abc#', 'Bùi Thị Thu Hiền', '355738432', NULL, TRUE, 1, '1999-12-30', NULL, 'hienbui@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ttm123', '1234Abc#', 'Trần Thị Mai', '973728288', NULL, TRUE, 1, '1988-01-07', NULL, 'Tranmai070188@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('hvn123', '1234Abc#', 'Hà Văn Nghĩa', '915220902', NULL, TRUE, 0, '1983-04-03', NULL, 'allopen.cntt@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ltl123', '1234Abc#', 'Lê Thị Lệ', '968992457', NULL, TRUE, 1, '2000-06-23', NULL, 'lethile23062000hn@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ktl123', '1234Abc#', 'Khuất Tuấn Linh', '966337366', NULL, TRUE, 0, '2000-08-16', NULL, 'khuattuanlinh168@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('nvc123', '1234Abc#', 'Nguyễn Văn Cường', '815657555', NULL, TRUE, 0, '1992-08-07', NULL, 'nguyenvancuongc282@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ptn123', '1234Abc#', 'Phạm Thanh Nga', '818324079', NULL, TRUE, 1, '1979-10-05', NULL, 'ngaphamthanh05101979@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('tst123', '1234Abc#', 'Trần Sơn Tùng', '933068668', NULL, TRUE, 0, '1988-06-20', NULL, 'samthaido@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('cln123', '1234Abc#', 'Cao Lê Na', '941806313', NULL, TRUE, 1, '1991-10-03', NULL, 'caolena2016@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('nthp123', '1234Abc#', 'Nguyễn Thị Hoài Phương', '865109796', NULL, TRUE, 1, '2002-10-25', NULL, 'phuonghoai.nguyen2510@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ntp123', '1234Abc#', 'Nguyễn Thị Phương', '988194071', NULL, TRUE, 1, '1984-01-06', NULL, 'ngaphuong01984@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('đđh123', '1234Abc#', 'Đặng Đức Hạnh', '989337883', NULL, TRUE, 1, '1983-11-08', NULL, 'duchanh.bando@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('đmv123', '1234Abc#', 'Đào Minh Việt', '983185525', NULL, TRUE, 0, '1982-09-27', NULL, 'daominhviet82@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ntmh123', '1234Abc#', 'Nguyễn Thị Mai Hương', '347731233', NULL, TRUE, 1, '1973-01-16', NULL, 'thienkhoihuong@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('lvt123', '1234Abc#', 'Lê Văn Thanh', '397609731', NULL, TRUE, 0, '2001-01-25', NULL, 'fctt25012001@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('lnd123', '1234Abc#', 'Lâm Ngọc Duy', '836633456', NULL, TRUE, 0, '2001-05-02', NULL, 'lamngocduy98@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ntn123', '1234Abc#', 'Ngô Trọng Nhân', '979262231', NULL, TRUE, 0, '1990-02-13', NULL, 'ngotrongnhan1417@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('tnn123', '1234Abc#', 'Trần Nhật Nam', '981368940', NULL, TRUE, 0, '2003-09-10', NULL, 'nhatnamt554@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('nnl123', '1234Abc#', 'Nguyễn Ngọc Ly', '985847286', NULL, TRUE, 1, '1992-12-05', NULL, 'mamvamo1617@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ttl123', '1234Abc#', 'Trương Thị Loan', '395255183', NULL, TRUE, 1, '2002-05-06', NULL, 'loansun06052002@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('đct123', '1234Abc#', 'Đặng Công Tùng', '965420234', NULL, TRUE, 0, '1987-09-25', NULL, 'tunghanh1997@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('cvđ123', '1234Abc#', 'Cung Văn Đạt', '976738386', NULL, TRUE, 0, '1986-07-10', NULL, 'Cungdat86@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('vth123', '1234Abc#', 'Vũ Thị Hậu', '943934656', NULL, TRUE, 1, '1984-10-13', NULL, 'Vuhau1210@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('đtt123', '1234Abc#', 'Đỗ Thị Thơm', '983636387', NULL, TRUE, 1, '1985-09-28', NULL, 'dothom280985@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('vmn123', '1234Abc#', 'Vũ Minh Ngọc', '868995703', NULL, TRUE, 1, '2001-03-28', NULL, 'sekiko2803@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('tk123', '1234Abc#', 'Trần Kiên', '902222632', NULL, TRUE, 0, '1984-04-28', NULL, 'Trankienbk2804@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ntt123', '1234Abc#', 'Nguyễn Thị Thu', '948563211', NULL, TRUE, 1, '1998-03-15', NULL, 'ngthu0315@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('tnv123', '1234Abc#', 'Trần Ngọc Vi', '982433366', NULL, TRUE, 0, '1986-06-22', NULL, 'tranngocvi@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('hnh123', '1234Abc#', 'Hoàng Nhật Hào', '913226544', NULL, TRUE, 0, '1993-04-03', NULL, 'nhat_hao@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('llt123', '1234Abc#', 'Lê Lý Thu', '915334455', NULL, TRUE, 1, '1987-05-17', NULL, 'lelythu@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('btt123', '1234Abc#', 'Bùi Thị Thanh', '965221344', NULL, TRUE, 1, '1995-11-23', NULL, 'buithanh23@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('tvn123', '1234Abc#', 'Trịnh Văn Nam', '913779865', NULL, TRUE, 0, '1996-12-12', NULL, 'trinhvannam@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('nnd123', '1234Abc#', 'Nguyễn Như Đạt', '911233445', NULL, TRUE, 0, '1994-08-21', NULL, 'nguyendatn94@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('thv123', '1234Abc#', 'Trần Hữu Vĩ', '942123456', NULL, TRUE, 0, '1998-01-30', NULL, 'tranhuuvi30@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('ltb123', '1234Abc#', 'Lương Thị Bích', '981223344', NULL, TRUE, 1, '1991-07-15', NULL, 'luongbich@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);

INSERT INTO user (user_name, password, name, phone, address, status, gender, day_of_birth, avatar, email, education_level, rating, experience, goal, interest, date_created, date_modified, secret, cost_per_hour, role_id)
VALUES ('nnt123', '1234Abc#', 'Nguyễn Ngọc Tâm', '912344555', NULL, TRUE, 1, '1996-03-22', NULL, 'ngocnguyen322@gmail.com', NULL, NULL, NULL, NULL, NULL, '2024-04-14', '2024-04-14', '123456', NULL, 5);