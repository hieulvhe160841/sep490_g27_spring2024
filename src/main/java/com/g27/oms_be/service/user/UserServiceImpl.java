package com.g27.oms_be.service.user;

import com.g27.oms_be.dto.certificate.CertificateDTO;
import com.g27.oms_be.dto.course.NumberOfCoursePerMonthAndYearDTO;
import com.g27.oms_be.dto.role.RoleDTO;
import com.g27.oms_be.dto.skill.SkillDTO;
import com.g27.oms_be.dto.support.SupportDTO;
import com.g27.oms_be.dto.user.admin.AddStaffDTO;
import com.g27.oms_be.dto.user.admin.NumberOfUserAndCoursePerMonthDTO;
import com.g27.oms_be.dto.user.admin.NumberOfUserPerMonthAndYearDTO;
import com.g27.oms_be.dto.user.admin.UserListDTO;
import com.g27.oms_be.dto.user.common.*;
import com.g27.oms_be.dto.user.mentor.EditProfileMentorDTO;
import com.g27.oms_be.dto.user.mentor.MentorDetailDTO;
import com.g27.oms_be.dto.user.mentor.MentorListDTO;
import com.g27.oms_be.entities.*;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.*;
import com.g27.oms_be.service.certificate.CertificateService;
import com.g27.oms_be.service.firebase.FirebaseService;
import com.g27.oms_be.service.role.RoleService;
import com.g27.oms_be.service.skill.SkillService;
import com.g27.oms_be.service.userSkill.UserSkillService;
import com.g27.oms_be.util.EmailUtil;
import com.g27.oms_be.util.OtpUtil;
import jakarta.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private RoleService roleService;
    private OtpUtil otpUtil;
    private EmailUtil emailUtil;
    private Map<String, String> storageOtp = new HashMap<>();
    private SkillService skillService;
    private CertificateService certificateService;
    private CourseRequestRepository courseRequestRepository;
    private MentorRequestRepository mentorRequestRepository;
    private WithdrawalRequestRepository withdrawalRequestRepository;
    private FirebaseService firebaseService;
    private SkillRepository skillRepository;
    private CertificateRepository certificateRepository;
    private UserCertificateRepository userCertificateRepository;
    private UserSkillRepository userSkillRepository;
    private UserWalletRepository userWalletRepository;
    private SupportRepository supportRepository;
    private Map<String, RegisterDTO> storageDataSignup = new HashMap<>();
    private ScheduledExecutorService scheduledExecutorService;
    private static String CHAT_ENGINE_PRIVATE_KEY = "8bfc3459-5db6-4163-8653-485aba38201d";
    private RestTemplate restTemplate;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleService roleService, OtpUtil otpUtil, EmailUtil emailUtil, SkillService skillService, CertificateService certificateService, CourseRequestRepository courseRequestRepository, MentorRequestRepository mentorRequestRepository, WithdrawalRequestRepository withdrawalRequestRepository, FirebaseService firebaseService, UserCertificateRepository userCertificateRepository, SkillRepository skillRepository, CertificateRepository certificateRepository, UserSkillRepository userSkillRepository, UserSkillService userSkillService, UserWalletRepository userWalletRepository, SupportRepository supportRepository, ScheduledExecutorService scheduledExecutorService, RestTemplate restTemplate) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
        this.otpUtil = otpUtil;
        this.emailUtil = emailUtil;
        this.firebaseService = firebaseService;
        this.skillService = skillService;
        this.certificateService = certificateService;
        this.courseRequestRepository = courseRequestRepository;
        this.mentorRequestRepository = mentorRequestRepository;
        this.withdrawalRequestRepository = withdrawalRequestRepository;
        this.skillRepository = skillRepository;
        this.certificateRepository = certificateRepository;
        this.userCertificateRepository = userCertificateRepository;
        this.userSkillRepository = userSkillRepository;
        this.userWalletRepository = userWalletRepository;
        this.supportRepository = supportRepository;
        this.scheduledExecutorService = scheduledExecutorService;
        this.restTemplate = restTemplate;
    }

    @Override
    public RegisterDTO addUser(RegisterDTO registerDTO, String otp) {

        List<String> imgList = firebaseService.getAllImagesInFolder("");
        for (int i = 0; i < imgList.size(); i++) {
            if (imgList.get(i).contains("defaultAvatar")) {
                registerDTO.setAvatar(imgList.get(i));
            }
        }
        User user = Mapper.mapDtoToEntity(registerDTO, User.class);
        user.setUserName(registerDTO.getUserName().toLowerCase());
        user.setEmail(registerDTO.getEmail().toLowerCase());
        user.setPassword(passwordEncoder.encode(registerDTO.getPassword()));
        user.setDateCreated(LocalDate.now());
        user.setDateModified(LocalDate.now());
        user.setSecret(otp);
        user.setStatus(true);
        user.setOtpGeneratedTime(LocalDateTime.now());
        if (registerDTO.getRole() == null) {
            user.setRole(roleService.findByName("ROLE_MENTEE"));
        } else {
            System.out.println(registerDTO.getRole());
            user.setRole(roleService.findByName(registerDTO.getRole()));
        }
        userRepository.save(user);
        UserWallet userWallet = new UserWallet();
        userWallet.setUser(user);
        userWallet.setLastUpdateDate(LocalDate.now());
        userWallet.setBalance(0D);
        userWalletRepository.save(userWallet);
        return registerDTO;
    }

    @Override
    public RegisterDTO sendEmail(RegisterDTO registerDTO) {
        String otp = otpUtil.generate();
        try {
            emailUtil.sendOtp(registerDTO.getEmail(), otp);

        } catch (MessagingException e) {
            throw new RuntimeException("We can not send otp...");
        }
        registerDTO.setTimeGenerateOtp(LocalDateTime.now());
        storageOtp.put(registerDTO.getEmail().toLowerCase(), otp);
        storageDataSignup.put(registerDTO.getEmail(), registerDTO);
        System.out.println(storageDataSignup);
        return registerDTO;
    }

    @Override
    public String verifyEmail(RegisterDTO registerDTO, String otp) {
//        if (Duration.between(registerDTO.getTimeGenerateOtp(), LocalDateTime.now()).getSeconds() < (1 * 60 * 2)) {
//            if (storageOtp.get(registerDTO.getEmail()).equals(otp)) {
//                addUser(registerDTO, otp);
//                storageOtp.remove(registerDTO.getEmail());
//                storageDataSignup.remove(registerDTO.getEmail(), registerDTO);
//                return "OTP is verified";
//            } else {
//                return "OTP is not true,enter OTP again";
//            }
//        }
        return "Time is expired, try regenerate OTP again";
    }

    @Override
    public List<UserListDTO> getAllUser() {
        List<User> userList = userRepository.findAll();
        List<UserListDTO> userListDTOS = userList.stream().map(user -> {
            UserListDTO userDTO = Mapper.mapEntityToDto(user, UserListDTO.class);
            RoleDTO roleDTO = Mapper.mapEntityToDto(user.getRole(), RoleDTO.class);
            userDTO.setRoleDTO(roleDTO);
            return userDTO;
        }).collect(Collectors.toList());
        return userListDTOS;
    }

    @Override
    public UserDetailDTO getUserDetailByUserName(String userName) {
        User user = userRepository.findUserByUserName(userName.toLowerCase());
        if (user == null) {
            return null;
        }
        UserDetailDTO userDetailDTO = Mapper.mapEntityToDto(user, UserDetailDTO.class);
        RoleDTO roleDTO = Mapper.mapEntityToDto(user.getRole(), RoleDTO.class);
        userDetailDTO.setRoleDTO(roleDTO);
        return userDetailDTO;
    }

    @Override
    public AddStaffDTO addStaff(AddStaffDTO addStaffDTO) {
        String encryptPassword = passwordEncoder.encode(addStaffDTO.getPassword());
        User user = Mapper.mapDtoToEntity(addStaffDTO, User.class);
        Role role = Mapper.mapDtoToEntity(addStaffDTO.getRoleDTO(), Role.class);
        user.setUserName(addStaffDTO.getUserName().toLowerCase());
        user.setRole(role);
        user.setDateCreated(LocalDate.now());
        user.setDateModified(LocalDate.now());
        user.setPassword(encryptPassword);
        String secret = addStaffDTO.getUserName();
        user.setSecret(secret);
        userRepository.save(user);
        String url = "https://api.chatengine.io/users";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Private-Key", CHAT_ENGINE_PRIVATE_KEY);

        Map<String, String> body = new HashMap<String, String>();
        body.put("username", addStaffDTO.getUserName().toLowerCase());
        body.put("secret", secret);

        HttpEntity<Map<String, String>> requestBody = new HttpEntity<>(body, headers);
        ResponseEntity<Map> response = restTemplate.exchange(url, HttpMethod.POST, requestBody, Map.class);
        return addStaffDTO;
    }

    @Override
    public String verifyEmail(String email, String otp) {
        RegisterDTO registerDTO = storageDataSignup.get(email);
        if (Duration.between(registerDTO.getTimeGenerateOtp(), LocalDateTime.now()).getSeconds() < (1 * 60 * 2)) {
            if (storageOtp.get(email).equals(otp)) {
                addUser(registerDTO, otp);
                storageDataSignup.remove(email);
                return "Xác thực thành công : " + registerDTO.getUserName();
            } else {
                return "OTP không hợp lệ,hãy nhập lại OTP";
            }
        }
        return "Thời gian hết hạn, vui lòng tạo OTP mới";
    }

    @Override
    public String regenerateOtp(String email) {
        String otp = otpUtil.generate();
        try {
            emailUtil.sendOtp(email, otp);
        } catch (MessagingException e) {
            return "Không thể gửi otp";
        }
        storageOtp.put(email, otp);
        RegisterDTO registerDTO = storageDataSignup.get(email);
        registerDTO.setTimeGenerateOtp(LocalDateTime.now());
        storageDataSignup.remove(email);
        storageDataSignup.put(email, registerDTO);
        return "Mã otp đã được gửi tới email và có hiệu lực trong 2 phút !";
    }

    @Override
    public String forgotPassword(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            return null;
        } else {
            try {
                String otp = otpUtil.generate();
                emailUtil.sendSetResetPassword(email, otp);
                storageOtp.put(email, otp);
                scheduledExecutorService.schedule(() -> {
                    storageOtp.remove(email);
                }, 2, TimeUnit.MINUTES);
            } catch (MessagingException e) {
                throw new RuntimeException("Unable to reset password, please try again");
            }
            return "Please check your email to get OTP";
        }
    }

    @Override
    public Boolean verifyOtp(String email, String enterOtp) {
        try {
            String storedOtp = storageOtp.get(email);
            return storedOtp.equals(enterOtp);
        } catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public String checkLogin(LoginDTO loginDTO) {
        User user = userRepository.findUserByUserName(loginDTO.getUserName().toLowerCase());
        if (!passwordEncoder.matches(loginDTO.getPassword(), user.getPassword())) {
            return "Mật khâủ không đúng";
        }
        if (passwordEncoder.matches(loginDTO.getPassword(), user.getPassword()) && !user.isStatus()) {
            return "Người dùng này không có quyền đăng nhập ";
        }
        return null;
    }

    @Override
    public List<MentorDetailDTO> getTopRateMentor() {
        List<User> users = userRepository.findTop10ByRating();
        List<MentorDetailDTO> mentorDetailDTOS = new ArrayList<>();
        if (users != null) {
            for (User user : users) {
                MentorDetailDTO mentorDetailDTO = Mapper.mapEntityToDto(user, MentorDetailDTO.class);
                mentorDetailDTO.setUserDetail(findByUserName(user.getUserName()));
                mentorDetailDTO.setNumberOfDoneSupport((long) supportRepository.findByMentorId(user.getUserId()).size());
                mentorDetailDTOS.add(mentorDetailDTO);
            }
            return mentorDetailDTOS;
        }
        return null;
    }

    @Override
    public String resetPassword(ResetPasswordDTO resetPasswordDTO, String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            return null;
        } else {
            String newPassword = passwordEncoder.encode(resetPasswordDTO.getNewPassword());
            user.setPassword(newPassword);
            userRepository.save(user);
            storageOtp.remove(email);
            return "Khôi phục mật khẩu thành công";
        }
    }

    @Override
    public UserDetailDTO findByUserName(String userName) {
        if (userRepository.findUserByUserName(userName.toLowerCase()) != null) {
            return Mapper.mapEntityToDto(userRepository.findUserByUserName(userName), UserDetailDTO.class);
        }
        return null;
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email.toLowerCase());
    }

    @Override
    public boolean existsByPhone(String phone) {
        return userRepository.existsByPhone(phone);
    }

    @Override
    public boolean existsByUserName(String userName) {
        return userRepository.existsByUserName(userName.toLowerCase());
    }

    @Override
    public String changePassword(ChangePasswordDTO changePasswordDTO) {
        User user = userRepository.findUserByUserName(changePasswordDTO.getUserName());
        if (user != null) {
            if (!passwordEncoder.matches(changePasswordDTO.getOldPassword(), user.getPassword())) {
                return " Mật khẩu hiện tại không đúng. Hãy thử lại";
            }
            if (!changePasswordDTO.getNewPassword().equals(changePasswordDTO.getConfirmNewPassword())) {
                return "Không thể xác nhận mật khẩu. Hãy thử lại";
            }
            if (passwordEncoder.matches(changePasswordDTO.getNewPassword(), user.getPassword())) {
                return "Mật khẩu trùng với mật khẩu hiện tại!";
            }
            user.setPassword(passwordEncoder.encode(changePasswordDTO.getConfirmNewPassword()));
            userRepository.save(user);
            return "Mật khẩu đã được thay đổi!";
        }

        return "Không thể thay đổi mật khẩu";
    }

    @Override
    public Long percentageOfMentors() {
        long numberMentors = userRepository.countByRoleName("ROLE_MENTOR");
        return numberMentors;
    }

    @Override
    public Long percentageOfMentees() {
        long numberMentees = userRepository.countByRoleName("ROLE_MENTEE");
        return numberMentees;
    }

    @Override
    public Long numberOfAllUsers() {
        long numberUsers = userRepository.count();
        return numberUsers;
    }

    @Override
    public List<MentorListDTO> getAllMentorWithSkillAndCertificate() {
        List<MentorListDTO> mentorListDTOS = new ArrayList<>();
        List<User> mentors = userRepository.findALlMentor();
        for (int i = 0; i < mentors.size(); i++) {
            Set<SkillDTO> skills = skillService.findSkillsByUserId(mentors.get(i).getUserId());
            Set<CertificateDTO> certificates = certificateService.findCertificatesByUserId(mentors.get(i).getUserId());
            UserDetailDTO userDetailDTO = getUserDetailByUserName(mentors.get(i).getUserName());
            MentorDetailDTO mentorDetailDTO = new MentorDetailDTO();
            mentorDetailDTO.setUserDetail(userDetailDTO);
            mentorDetailDTO.setRating(mentors.get(i).getRating());
            mentorDetailDTO.setCostPerHour(mentors.get(i).getCostPerHour());
            mentorDetailDTO.setNumberOfDoneSupport((long) supportRepository.findByMentorId(mentors.get(i).getUserId()).size());
            MentorListDTO mentorListDTO = Mapper.mapEntityToDto(mentors.get(i), MentorListDTO.class);
            mentorListDTO.setSkills(skills);
            mentorListDTO.setCertificates(certificates);
            mentorListDTO.setMentorDetail(mentorDetailDTO);
            mentorListDTOS.add(mentorListDTO);
        }
        return mentorListDTOS;
    }

    @Override
    public List<NumberOfUserPerMonthAndYearDTO> countUsersRegisteredInMonth() {
        List<NumberOfUserPerMonthAndYearDTO> numberOfUserPerMonthAndYearDTOList = userRepository.countUsersByDateCreatedPerMonth(LocalDate.now().getYear());
        return numberOfUserPerMonthAndYearDTOList;
    }

    @Override
    public Long totalNumberOfRequest() {
        return mentorRequestRepository.count() + courseRequestRepository.count() + withdrawalRequestRepository.count();
    }

    @Override
    public UserDetailDTO getByUserId(Long userId) {
        return Mapper.mapEntityToDto(userRepository.findByUserId(userId), UserDetailDTO.class);
    }

    @Override
    public List<MentorDetailDTO> suggestMentor(String interest, String goal) {
        List<User> users;
        List<MentorDetailDTO> mentorDetailDTOS = new ArrayList<>();
        if (goal == null || interest == null) {
            users = userRepository.findTop10ByRating();
            map(users, mentorDetailDTOS);
            return mentorDetailDTOS;

        } else {
            List<SkillDTO> skills = skillService.availableSKills();
            for (SkillDTO skill : skills) {
                if (goal.contains(skill.getSkillName()) || interest.contains(skill.getSkillName())) {
                    users = userRepository.findByExperience(skill.getSkillName(), skill.getSkillName());
                    if (users.size() > 0) {
                        map(users, mentorDetailDTOS);
                        return mentorDetailDTOS;
                    }
                }
            }
            users = userRepository.findRandomMentor();
            map(users, mentorDetailDTOS);
            return mentorDetailDTOS;
        }
    }

    @Override
    public void map(List<User> users, List<MentorDetailDTO> mentorDetailDTOS) {
        for (User user : users) {
            UserDetailDTO userDetailDTO = getUserDetailByUserName(user.getUserName());
            MentorDetailDTO mentorDetailDTO = new MentorDetailDTO();
            mentorDetailDTO.setUserDetail(userDetailDTO);
            mentorDetailDTO.setCostPerHour(user.getCostPerHour());
            mentorDetailDTO.setRating(supportRepository.findAverageRatingMentor(user.getUserId()));
            mentorDetailDTOS.add(mentorDetailDTO);
        }
    }

    @Override
    public String updateStatus(String username) {
        User user = userRepository.findUserByUserName(username);
        if (user == null) {
            return null;
        }
        if (user.isStatus() == true) {
            user.setStatus(false);
        } else {
            user.setStatus(true);
        }
        userRepository.save(user);
        return "Status of user is updated";
    }

    @Override
    public List<NumberOfUserAndCoursePerMonthDTO> numberOfUserAndCoursePerMonthDtoList(List<NumberOfUserPerMonthAndYearDTO> numberOfUserRegisteredPerMonthAndYearList, List<NumberOfCoursePerMonthAndYearDTO> numberOfCoursePerMonthAndYearList) {
        List<NumberOfUserAndCoursePerMonthDTO> numberOfUserAndCoursePerMonthList = new ArrayList<>();
        for (int month = 1; month <= 12; month++) {
            Long numberOfPeopleRegisteredInMonth = 0L;
            Long numberOfCourseCreatedInMonth = 0L;
            for (NumberOfUserPerMonthAndYearDTO userDTO : numberOfUserRegisteredPerMonthAndYearList) {
                if (userDTO.getMonth().equals(month)) {
                    numberOfPeopleRegisteredInMonth = userDTO.getNumberOfPeopleRegisteredInMonth();
                    break;
                }
            }
            for (NumberOfCoursePerMonthAndYearDTO courseDTO : numberOfCoursePerMonthAndYearList) {
                if (courseDTO.getMonth().equals(month)) {
                    numberOfCourseCreatedInMonth = courseDTO.getNumberOfCourseCreatedInMonth();
                    break;
                }
            }
            NumberOfUserAndCoursePerMonthDTO numberOfUserAndCoursePerMonthDTO = new NumberOfUserAndCoursePerMonthDTO();
            numberOfUserAndCoursePerMonthDTO.setMonth(month);
            numberOfUserAndCoursePerMonthDTO.setNumberOfPeopleRegisteredInMonth(numberOfPeopleRegisteredInMonth);
            numberOfUserAndCoursePerMonthDTO.setNumberOfCourseCreatedInMonth(numberOfCourseCreatedInMonth);
            numberOfUserAndCoursePerMonthList.add(numberOfUserAndCoursePerMonthDTO);
        }
        return numberOfUserAndCoursePerMonthList;
    }

    @Override
    public MentorListDTO editProfileMentor(MentorListDTO mentorListDTO, List<MultipartFile> files, List<String> cert, MultipartFile avatar) throws IOException {
//        User user = userRepository.findUserByUserName(mentorListDTO.getMentorDetail().getUserDetail().getUserName());
//        for (int i = 0; i < files.size(); i++) {
//            String certificateUrl = firebaseService.uploadFile(files.get(i).getOriginalFilename(), files.get(i).getBytes());
//            Certificate certificate = certificateRepository.findByCertificateName(cert.get(i));
//            UserCertificate uc;
//            Optional<UserCertificate> updateUserCertificate = userCertificateRepository.findByUserNameAndCertificate(mentorListDTO.getMentorDetail().getUserDetail().getUserName(), cert.get(i));
//            if (updateUserCertificate.isPresent()) {
//                uc = updateUserCertificate.get();
//            } else {
//                uc = new UserCertificate();
//            }
//            uc.setCertificateUrl(certificateUrl);
//            uc.setUser(user);
//            uc.setCertificate(certificate);
//            userCertificateRepository.save(uc);
//
//        }
//        if (avatar != null) {
//            String updateAvatar = firebaseService.uploadFile(avatar.getOriginalFilename(), avatar.getBytes());
//            user.setAvatar(updateAvatar);
//        }
//        user.setGoal(mentorListDTO.getMentorDetail().getUserDetail().getGoal());
//        user.setName(mentorListDTO.getMentorDetail().getUserDetail().getName());
//        user.setCostPerHour(mentorListDTO.getMentorDetail().getCostPerHour());
//        user.setDayOfBirth(mentorListDTO.getMentorDetail().getUserDetail().getDayOfBirth());
//        user.setExperience(mentorListDTO.getMentorDetail().getUserDetail().getExperience());
//        user.setAddress(mentorListDTO.getMentorDetail().getUserDetail().getAddress());
//        user.setGender(mentorListDTO.getMentorDetail().getUserDetail().getGender());
//        user.setDateModified(LocalDate.now());
//
//        userRepository.save(user);

        return mentorListDTO;
    }

    @Override
    public MenteeDetailDTO editProfile(MenteeDetailDTO menteeDetail, MultipartFile avatar) throws IOException {
//        String folderPath = "image/" + menteeDetail.getUserName() + "/";
//        User user = userRepository.findUserByUserName(menteeDetail.getUserName());
//        if (avatar != null) {
//            String updateAvatar = firebaseService.uploadFile(avatar.getOriginalFilename(), avatar.getBytes());
//            user.setAvatar(updateAvatar);
//        }
//        user.setGoal(menteeDetail.getGoal());
//        user.setInterest(menteeDetail.getInterest());
//        user.setName(menteeDetail.getName());
//        user.setDayOfBirth(menteeDetail.getDayOfBirth());
//        user.setAddress(menteeDetail.getAddress());
//        user.setDateModified(LocalDate.now());
//        user.setPhone(menteeDetail.getPhone());
//        user.setGender(menteeDetail.getGender());
//        userRepository.save(user);
        return menteeDetail;
    }

    @Override
    public MentorListDTO getMentorDetailAndCertificates(String userName) {
        User user = userRepository.findUserByUserName(userName);
        if (user == null) {
            return null;
        }
        List<Support> supportList =supportRepository.findByMentorId(user.getUserId());
        Set<SupportDTO> supportDTOS = new HashSet<>();
        if(!supportList.isEmpty()){
            for (Support support: supportList) {
                  SupportDTO supportDTO = Mapper.mapEntityToDto(support, SupportDTO.class);
                  supportDTOS.add(supportDTO);
            }
        }

        UserDetailDTO userDetailDTO = getUserDetailByUserName(user.getUserName());
        MentorDetailDTO mentorDetailDTO = Mapper.mapEntityToDto(user, MentorDetailDTO.class);
        mentorDetailDTO.setRating(supportRepository.findAverageRatingMentor(user.getUserId()));
        mentorDetailDTO.setUserDetail(userDetailDTO);
        MentorListDTO mentorListDTO = new MentorListDTO();
        Set<SkillDTO> skills = skillService.findSkillsByUserId(user.getUserId());
        Set<CertificateDTO> certificates = certificateService.findCertificatesByUserId(user.getUserId());
        mentorListDTO.setMentorDetail(mentorDetailDTO);
        mentorListDTO.setCertificates(certificates);
        mentorListDTO.setSkills(skills);
        mentorListDTO.setSupports(supportDTOS);
        return mentorListDTO;
    }

    @Override
    public MentorListDTO getMentorDetailAndCertificatesByUserId(Long id) {
        Optional<User> optional = userRepository.findById(id);
        if (!optional.isPresent()) {
            return null;
        }
        User user = optional.get();
        UserDetailDTO userDetailDTO = getUserDetailByUserName(user.getUserName());
        MentorDetailDTO mentorDetailDTO = Mapper.mapEntityToDto(user, MentorDetailDTO.class);
        mentorDetailDTO.setRating(supportRepository.findAverageRatingMentor(id));
        mentorDetailDTO.setUserDetail(userDetailDTO);
        MentorListDTO mentorListDTO = new MentorListDTO();
        Set<SkillDTO> skills = skillService.findSkillsByUserId(user.getUserId());
        Set<CertificateDTO> certificates = certificateService.findCertificatesByUserId(user.getUserId());
        mentorListDTO.setMentorDetail(mentorDetailDTO);
        mentorListDTO.setCertificates(certificates);
        mentorListDTO.setSkills(skills);
        return mentorListDTO;
    }

    @Override
    public MentorListDTO editProfileMentor(EditProfileMentorDTO editProfileMentor, MultipartFile avatarFile, String username) throws IOException {
        User user = userRepository.findUserByUserName(username);
        if (!avatarFile.isEmpty()) {
            String avatar = firebaseService.uploadFile(avatarFile.getOriginalFilename(), avatarFile.getBytes());
            user.setAvatar(avatar);
        }
        user.setName(editProfileMentor.getName());
        user.setAddress(editProfileMentor.getAddress());
        user.setDayOfBirth(editProfileMentor.getDayOfBirth());
        user.setPhone(editProfileMentor.getPhone());
        user.setExperience(editProfileMentor.getExperience());
        user.setEducationLevel(editProfileMentor.getEducationLevel());
        user.setDateModified(LocalDate.now());
        user.setCostPerHour(editProfileMentor.getCostPerHours());
        userRepository.save(user);
        MentorListDTO mentorListDTO = getMentorDetailAndCertificatesByUserId(user.getUserId());
        return mentorListDTO;
    }

    @Override
    public UserDetailDTO editProfile(EditProfileDTO editProfileDTO, MultipartFile avatarFile, String username) throws IOException {
        User user = userRepository.findUserByUserName(username);
        if (!avatarFile.isEmpty()) {
            String avatar = firebaseService.uploadFile(avatarFile.getOriginalFilename(), avatarFile.getBytes());
            user.setAvatar(avatar);
        }
        user.setName(editProfileDTO.getName());
        user.setAddress(editProfileDTO.getAddress());
        user.setDayOfBirth(editProfileDTO.getDayOfBirth());
        user.setPhone(editProfileDTO.getPhone());
        user.setGoal(editProfileDTO.getGoal());
        user.setInterest(editProfileDTO.getInterest());
        user.setDateModified(LocalDate.now());
        userRepository.save(user);
        UserDetailDTO mentorListDTO = getUserDetailByUserName(user.getUserName());
        return mentorListDTO;
    }

    @Override
    public MentorListDTO editCertificate(List<MultipartFile> certificateFile, List<String> certificateType, String username) throws IOException {
        User user = userRepository.findUserByUserName(username);
        List<CertificateDTO> list = new ArrayList<>();
        for (int i = 0; i < certificateType.size(); i++) {
            String certificateUrl = firebaseService.uploadFile(certificateFile.get(i).getOriginalFilename(), certificateFile.get(i).getBytes());
            Certificate certificate = certificateRepository.findByCertificateName(certificateType.get(i));
            Optional<UserCertificate> optional = userCertificateRepository.findByUserNameAndCertificate(username, certificateType.get(i));
            UserCertificate uc;
            if (optional.isPresent()) {
                uc = optional.get();
            } else {
                uc = new UserCertificate();
            }
            uc.setCertificateUrl(certificateUrl);
            uc.setUser(user);
            uc.setCertificate(certificate);
            userCertificateRepository.save(uc);
        }
        MentorListDTO mentorListDTO = getMentorDetailAndCertificatesByUserId(user.getUserId());
        return mentorListDTO;
    }
}


