package com.g27.oms_be.service.user;

import com.g27.oms_be.dto.course.NumberOfCoursePerMonthAndYearDTO;
import com.g27.oms_be.dto.user.admin.AddStaffDTO;
import com.g27.oms_be.dto.user.admin.NumberOfUserAndCoursePerMonthDTO;
import com.g27.oms_be.dto.user.admin.NumberOfUserPerMonthAndYearDTO;
import com.g27.oms_be.dto.user.admin.UserListDTO;
import com.g27.oms_be.dto.user.common.*;
import com.g27.oms_be.dto.user.mentor.EditProfileMentorDTO;
import com.g27.oms_be.dto.user.mentor.MentorDetailDTO;
import com.g27.oms_be.dto.user.mentor.MentorListDTO;
import com.g27.oms_be.entities.User;
import jakarta.mail.MessagingException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface UserService {
    RegisterDTO addUser(RegisterDTO registerDTO, String otp);

    RegisterDTO sendEmail(RegisterDTO registerDTO);

    String verifyEmail(RegisterDTO registerDTO, String otp);

    List<UserListDTO> getAllUser();

    UserDetailDTO getUserDetailByUserName(String username);

    AddStaffDTO addStaff(AddStaffDTO addStaffDTO);

    String verifyEmail(String email, String otp);

    String regenerateOtp(String email);

    String forgotPassword(String email);

    String resetPassword(ResetPasswordDTO resetPasswordDTO, String email);

    UserDetailDTO findByUserName(String userName);

    boolean existsByEmail(String email);

    boolean existsByPhone(String phone);

    boolean existsByUserName(String userName);

    String changePassword(ChangePasswordDTO changePasswordDTO);

    Boolean verifyOtp(String email, String otp);

    String checkLogin(LoginDTO loginDTO);

    List<MentorDetailDTO> getTopRateMentor();

    Long percentageOfMentors();

    Long percentageOfMentees();

    Long numberOfAllUsers();

    List<MentorListDTO> getAllMentorWithSkillAndCertificate();

    List<NumberOfUserPerMonthAndYearDTO> countUsersRegisteredInMonth();

    Long totalNumberOfRequest();

    UserDetailDTO getByUserId(Long userId);

    List<MentorDetailDTO> suggestMentor(String interest, String goal);

    void map(List<User> users, List<MentorDetailDTO> mentorDetailDTOS);

    String updateStatus(String username);

    List<NumberOfUserAndCoursePerMonthDTO> numberOfUserAndCoursePerMonthDtoList(List<NumberOfUserPerMonthAndYearDTO> numberOfUserRegisteredPerMonthAndYearList, List<NumberOfCoursePerMonthAndYearDTO> numberOfCoursePerMonthAndYearList);

    MentorListDTO editProfileMentor(MentorListDTO mentorDTO, List<MultipartFile> files, List<String> cert, MultipartFile avatar) throws IOException;

    MenteeDetailDTO editProfile(MenteeDetailDTO menteeDetail, MultipartFile avatar) throws IOException;

    MentorListDTO getMentorDetailAndCertificates(String userName);

    MentorListDTO getMentorDetailAndCertificatesByUserId(Long id);

    MentorListDTO editProfileMentor(EditProfileMentorDTO editProfileMentor, MultipartFile avatar, String username) throws IOException;

    UserDetailDTO editProfile(EditProfileDTO editProfileDTO, MultipartFile avatar, String username) throws IOException;

    MentorListDTO editCertificate(List<MultipartFile> certificateFile, List<String> certificateType, String username) throws IOException;
}
