package com.g27.oms_be.service.user;

import com.g27.oms_be.security.JwtTokenProvider;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

@Service
public class LogoutServiceImpl implements LogoutHandler {
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String bearerToken = request.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            String jwt = bearerToken.substring(7);
//            System.out.println(jwt);
            String token = jwtTokenProvider.getUserNameFromJwt(jwt);
            if (token != null) {
//                System.out.println("not null");
                SecurityContextHolder.clearContext();
//                System.out.println(jwt);
            } else {
//                System.out.println("null");
            }
        }

    }
}
