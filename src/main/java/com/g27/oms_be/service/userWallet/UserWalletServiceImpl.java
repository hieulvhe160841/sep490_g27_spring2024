package com.g27.oms_be.service.userWallet;

import com.g27.oms_be.dto.userWallet.UserViewTheirWalletDTO;
import com.g27.oms_be.dto.userWallet.UserWalletDTO;
import com.g27.oms_be.entities.Denomination;
import com.g27.oms_be.entities.UserWallet;
import com.g27.oms_be.repository.DenominationRepository;
import com.g27.oms_be.repository.UserWalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UserWalletServiceImpl implements UserWalletService {
    private UserWalletRepository userWalletRepository;
    private DenominationRepository denominationRepository;

    @Autowired
    public UserWalletServiceImpl(UserWalletRepository userWalletRepository, DenominationRepository denominationRepository) {
        this.userWalletRepository = userWalletRepository;
        this.denominationRepository = denominationRepository;
    }

    @Override
    public UserWalletDTO updateBalanceForUserWalletAfterDenomination(Long userid, Long denominationId, Long transactionId) {
        Optional<UserWallet> optionalUserWallet = userWalletRepository.findByUserUserId(userid);
        Denomination denomination = denominationRepository.getReferenceById(denominationId);
        if (optionalUserWallet.isPresent() && denomination != null) {
            UserWallet userWallet = optionalUserWallet.get();
            userWallet.setBalance(userWallet.getBalance() + denomination.getValue());
            userWallet.setLastUpdateDate(LocalDate.now());

            userWalletRepository.save(userWallet);

            UserWalletDTO userWalletDTO = new UserWalletDTO();
            userWalletDTO.setUserWalletId(userWallet.getUserWalletId());
            userWalletDTO.setBalance(userWallet.getBalance());
            userWalletDTO.setLastUpdateDate(userWallet.getLastUpdateDate());
            userWalletDTO.setUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            return userWalletDTO;
        }
        return null;
    }

    @Override
    public UserViewTheirWalletDTO menteeViewTheirWallet() {
        Optional<UserWallet> optionalUserWallet = userWalletRepository.findByUserUserName(SecurityContextHolder.getContext().getAuthentication().getName());
        if (optionalUserWallet.isPresent()) {
            UserWallet userWallet = optionalUserWallet.get();
            UserViewTheirWalletDTO userViewTheirWalletDTO = new UserViewTheirWalletDTO();
            userViewTheirWalletDTO.setUserWalletId(userWallet.getUserWalletId());
            userViewTheirWalletDTO.setBalance(userWallet.getBalance());
            return userViewTheirWalletDTO;
        }
        return null;
    }

    @Override
    public UserWalletDTO getByUserName(String username) {
        Optional<UserWallet> userWallet = userWalletRepository.findByUserUserName(username);
        if (userWallet.isPresent()) {
            UserWallet userWallet1 = userWallet.get();
            UserWalletDTO userWalletDTO = new UserWalletDTO();
            userWalletDTO.setBalance(userWallet1.getBalance());
            userWalletDTO.setUserWalletId(userWallet1.getUserWalletId());
            userWalletDTO.setUsername(username);
            userWalletDTO.setLastUpdateDate(userWallet1.getLastUpdateDate());
            return userWalletDTO;
        }
        return null;
    }
}
