package com.g27.oms_be.service.userWallet;

import com.g27.oms_be.dto.userWallet.UserViewTheirWalletDTO;
import com.g27.oms_be.dto.userWallet.UserWalletDTO;

public interface UserWalletService {
    UserWalletDTO updateBalanceForUserWalletAfterDenomination(Long userid, Long denominationId, Long transactionId);

    UserViewTheirWalletDTO menteeViewTheirWallet();
    UserWalletDTO getByUserName(String userName);
}
