package com.g27.oms_be.service.skill;

import com.g27.oms_be.dto.skill.SkillDTO;
import com.g27.oms_be.dto.skill.UpdateSkillDTO;
import com.g27.oms_be.entities.Skill;

import java.util.List;
import java.util.Set;

public interface SkillService {
    SkillDTO addSkill(SkillDTO skillDTO);

    List<SkillDTO> getAllSkill();

    UpdateSkillDTO updateSkill(UpdateSkillDTO skillDTO, Long skillId);

    boolean checkDuplicateSkillName(String name);

    Set<SkillDTO> findSkillsByUserId(Long userId);

    SkillDTO skillDetail(Long skillId);

    Skill getByName(String name);

    boolean checkSkillExisted(Long skillId);

    boolean checkDupicateSkillNameExceptId(String skillName, Long skillId);

    List<SkillDTO>  availableSKills();
}
