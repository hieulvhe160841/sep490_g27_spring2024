package com.g27.oms_be.service.skill;

import com.g27.oms_be.dto.skill.SkillDTO;
import com.g27.oms_be.dto.skill.UpdateSkillDTO;
import com.g27.oms_be.entities.Skill;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.SkillRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@Service
public class SkillServiceImpl implements SkillService {

    private SkillRepository skillRepository;

    @Override
    public SkillDTO addSkill(SkillDTO skillDTO) {
        Skill skill = Mapper.mapDtoToEntity(skillDTO, Skill.class);
        skillRepository.save(skill);
        return skillDTO;
    }

    @Override
    public List<SkillDTO> getAllSkill() {
        List<Skill> skillList = skillRepository.findAll();
        List<SkillDTO> skillDTOList = new ArrayList<>();
        skillList.stream().forEach(skill -> skillDTOList.add(Mapper.mapEntityToDto(skill, SkillDTO.class)));
        return skillDTOList;
    }

    @Override
    public UpdateSkillDTO updateSkill(UpdateSkillDTO updateSkillDTO, Long skillId) {
        Optional<Skill> optionalSkill = skillRepository.findById(skillId);
        if (optionalSkill.isPresent()) {
            Skill skill = optionalSkill.get();
            if (updateSkillDTO.getSkillName() != null && !updateSkillDTO.getSkillName().trim().equals("")) {
                skill.setSkillName(updateSkillDTO.getSkillName());
            }
            if (updateSkillDTO.getSkillName() != null && !updateSkillDTO.getSkillName().trim().equals("")) {
                skill.setSkillDescription(updateSkillDTO.getSkillDescription());
            }
            if (updateSkillDTO.isSkillStatus()) {
                skill.setSkillStatus(updateSkillDTO.isSkillStatus());
            }
            skill.setSkillStatus(updateSkillDTO.isSkillStatus());
            skillRepository.save(skill);
        }
        return updateSkillDTO;
    }

    public boolean checkDuplicateSkillName(String skillName) {
        return skillRepository.findByskillName(skillName) == null;
    }

    @Override
    public Set<SkillDTO> findSkillsByUserId(Long userId) {
        return skillRepository.findSkillsByUserId(userId);
    }

    @Override
    public SkillDTO skillDetail(Long skillId) {
        Optional<Skill> optionalSkill = skillRepository.findById(skillId);
        if (optionalSkill.isPresent()) {
            SkillDTO skillDTO = Mapper.mapEntityToDto(optionalSkill.get(), SkillDTO.class);
            return skillDTO;
        }
        return null;
    }

    @Override
    public Skill getByName(String name) {
        return skillRepository.findByskillName(name);
    }

    @Override
    public boolean checkSkillExisted(Long skillId) {
        return skillRepository.existsById(skillId);
    }

    @Override
    public boolean checkDupicateSkillNameExceptId(String skillName, Long skillId) {
        return skillRepository.findByskillNameAndSkillIdNot(skillName, skillId) == null;
    }

    @Override
    public List<SkillDTO> availableSKills() {
        List<Skill> skills = skillRepository.findAvailableSkill();
        List<SkillDTO> skillDTOS = new ArrayList<>();
        for (Skill skill : skills) {
            SkillDTO skillDTO = Mapper.mapEntityToDto(skill, SkillDTO.class);
            skillDTOS.add(skillDTO);
        }
        return skillDTOS;
    }
}
