package com.g27.oms_be.service.firebase;

import com.google.cloud.storage.Storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface FirebaseService {
    Storage createStorage() throws IOException;

    String uploadFile(String fileName , byte[] fileContent) throws IOException;

    InputStream view(String fileName) throws IOException;

    String getFileExtension(String fileName);

    List<String> getAllImagesInFolder(String folderPath);

}
