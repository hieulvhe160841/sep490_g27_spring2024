package com.g27.oms_be.service.firebase;

import com.google.api.gax.paging.Page;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.WriteChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class FirebaseServiceImpl implements FirebaseService {
    private final String bucketName = "sep490-g27-spring2024.appspot.com";

    @Override
    public Storage createStorage() throws IOException {
        Storage storage = StorageOptions.newBuilder()
                .setCredentials(GoogleCredentials.fromStream(getClass().getResourceAsStream("/" +
                        "sep490-g27-spring2024-firebase-adminsdk-9kier-ea0799e99c.json")))
                .build()
                .getService();
        return storage;
    }

    @Override
    public String uploadFile(String fileName, byte[] fileContent) throws IOException {
        //upload an image to a folder
        String uuidFileName = UUID.randomUUID().toString() + getFileExtension(fileName);

        BlobInfo blobInfo = BlobInfo.newBuilder(bucketName, uuidFileName).build();
        try (WriteChannel writer = createStorage().writer(blobInfo)) {
            writer.write(ByteBuffer.wrap(fileContent, 0, fileContent.length));
        }
        return uuidFileName;
    }

    @Override
    //view image 
    public InputStream view(String fileName) throws IOException {
        Blob blob = createStorage().get(bucketName, fileName);

        if (blob != null) {
            return Channels.newInputStream(blob.reader());
        } else {
            return null;
        }

    }


    @Override
    //mo rong cua file(/.png/jpg,...)
    public String getFileExtension(String fileName) {
        int dotIndex = fileName.lastIndexOf('.');
        if (dotIndex > 0 && dotIndex < fileName.length() - 1) {
            return fileName.substring(dotIndex);
        }
        return "";
    }

    @Override
    //view list image in a selected folder
    public List<String> getAllImagesInFolder(String folderPath) {
        List<String> imageUrls = new ArrayList<>();

        try {
            // List blobs in the specified folder
            Page<Blob> blobs = createStorage().list(bucketName, Storage.BlobListOption.prefix(folderPath));
            System.out.println(blobs);
            // Extract URLs from the blobs
            for (Blob blob : blobs.iterateAll()) {
                String imageUrl = blob.getName();
                imageUrls.add(imageUrl);
            }
        } catch (IOException e) {
            return null;
        }

        return imageUrls;
    }

}
