package com.g27.oms_be.service.certificate;

import com.g27.oms_be.dto.certificate.CertificateDTO;
import com.g27.oms_be.entities.Certificate;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.CertificateRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class CertificateServiceImpl implements CertificateService {
    private CertificateRepository certificateRepository;

    public CertificateServiceImpl(CertificateRepository certificateRepository) {
        this.certificateRepository = certificateRepository;
    }

    @Override
    public Set<CertificateDTO> findCertificatesByUserId(Long userId) {
        return certificateRepository.findCertificateByUserId(userId);
    }

    @Override
    public List<CertificateDTO> getAllCertificates() {
        List<Certificate> certificates = certificateRepository.findAll();
        List<CertificateDTO> certificateDTOS = new ArrayList<>();
        for (Certificate certificate : certificates) {
            CertificateDTO certificateDTO = Mapper.mapEntityToDto(certificate, CertificateDTO.class);
            certificateDTOS.add(certificateDTO);
        }
        return certificateDTOS;
    }
}
