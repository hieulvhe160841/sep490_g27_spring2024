package com.g27.oms_be.service.certificate;

import com.g27.oms_be.dto.certificate.CertificateDTO;
import com.g27.oms_be.dto.skill.SkillDTO;

import java.util.List;
import java.util.Set;

public interface CertificateService {
    Set<CertificateDTO> findCertificatesByUserId(Long userId);
    List<CertificateDTO> getAllCertificates();
}
