package com.g27.oms_be.service.userCertificate;

import com.g27.oms_be.common.MentorRequestStatus;
import com.g27.oms_be.dto.user.mentor.MentorCertificateDTO;
import com.g27.oms_be.dto.userCertificate.UserCertificateDetailDTO;
import com.g27.oms_be.entities.Certificate;
import com.g27.oms_be.entities.MentorRequest;
import com.g27.oms_be.entities.User;
import com.g27.oms_be.entities.UserCertificate;
import com.g27.oms_be.repository.CertificateRepository;
import com.g27.oms_be.repository.MentorRequestRepository;
import com.g27.oms_be.repository.UserCertificateRepository;
import com.g27.oms_be.repository.UserRepository;
import com.g27.oms_be.service.firebase.FirebaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserCertificateServiceImpl implements UserCertificateService {
    private UserCertificateRepository userCertificateRepository;
    private CertificateRepository certificateRepository;
    private UserRepository userRepository;
    private FirebaseService firebaseService;
    private MentorRequestRepository mentorRequestRepository;

    @Autowired
    public UserCertificateServiceImpl(UserCertificateRepository userCertificateRepository, CertificateRepository certificateRepository, UserRepository userRepository, FirebaseService firebaseService, MentorRequestRepository mentorRequestRepository) {
        this.userCertificateRepository = userCertificateRepository;
        this.certificateRepository = certificateRepository;
        this.userRepository = userRepository;
        this.firebaseService = firebaseService;
        this.mentorRequestRepository = mentorRequestRepository;
    }

    @Override
    public List<UserCertificateDetailDTO> userCertificateListByUserId(Long userID) {
        List<UserCertificate> userCertificateList = userCertificateRepository.findByUserUserId(userID);
        List<UserCertificateDetailDTO> userCertificateDetailDTOList = userCertificateList.stream().map(userCertificate -> {
            UserCertificateDetailDTO userCertificateDetailDTO = new UserCertificateDetailDTO();
            userCertificateDetailDTO.setCertificateName(userCertificate.getCertificate().getCertificateName());
            userCertificateDetailDTO.setCertificateUrl(userCertificate.getCertificateUrl());
            return userCertificateDetailDTO;
        }).collect(Collectors.toList());
        return userCertificateDetailDTOList;
    }

    @Override
    public MentorCertificateDTO addCertificate(List<String> certName, List<MultipartFile> files, String username) throws IOException {
        User user = userRepository.findUserByUserName(username);
        List<UserCertificate> userCertificateList = userCertificateRepository.findByUserUserId(user.getUserId());
        if (!userCertificateList.isEmpty()) {
            for (UserCertificate usercertificate : userCertificateList) {
                userCertificateRepository.delete(usercertificate);
            }
        }
        Map<String, String> certNameAndUrl = new HashMap<>();
        for (int i = 0; i < files.size(); i++) {
            Certificate certificate = certificateRepository.findByCertificateName(certName.get(i));

            byte[] fileContent = files.get(i).getBytes();
            String certUrl = firebaseService.uploadFile(files.get(i).getOriginalFilename(), fileContent);
            UserCertificate userCertificate = new UserCertificate();
            userCertificate.setUser(user);
            userCertificate.setCertificate(certificate);
            userCertificate.setCertificateUrl(certUrl);
            userCertificateRepository.save(userCertificate);
            certNameAndUrl.put(certName.get(i), certUrl);

        }

        MentorCertificateDTO mentorCertificateDTO = new MentorCertificateDTO();
        mentorCertificateDTO.setCertificates(certNameAndUrl);
        mentorCertificateDTO.setUserName(username);
        MentorRequest mentorRequest = new MentorRequest();
        mentorRequest.setUser(user);
        mentorRequest.setMentorRequestStatus(MentorRequestStatus.WAITING.name());
        mentorRequestRepository.save(mentorRequest);
        return mentorCertificateDTO;
    }
}
