package com.g27.oms_be.service.userCertificate;

import com.g27.oms_be.dto.user.mentor.MentorCertificateDTO;
import com.g27.oms_be.dto.userCertificate.UserCertificateDetailDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface UserCertificateService {
    List<UserCertificateDetailDTO> userCertificateListByUserId(Long userID);
    MentorCertificateDTO addCertificate(List<String> certName, List<MultipartFile> file,String username) throws IOException;
}
