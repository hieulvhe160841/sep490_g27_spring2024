package com.g27.oms_be.service.support;

import com.g27.oms_be.entities.Support;
import com.g27.oms_be.entities.User;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.util.List;

public class SupportSpecifications {

    public static Specification<Support> hasStatus(List<String> status) {
        return (root, query, cb) -> {
            if (status == null || status.isEmpty()) {
                return cb.conjunction();
            }
            return root.get("supportStatus").in(status);
        };
    }

    public static Specification<Support> hasUsername(String username, String roleName) {
        return (root, query, cb) -> {
            if (username == null || username.isEmpty()) {
                return cb.conjunction();
            }
            Join<Support, User> userJoin;
            if (roleName.equals("ROLE_MENTEE")) {
                userJoin = root.join("menteeId", JoinType.INNER);
            } else {
                userJoin = root.join("mentorId", JoinType.INNER);
            }
            return cb.equal(cb.lower(userJoin.get("userName")), username.toLowerCase());
        };
    }

    public static Specification<Support> createdBetween(LocalDate startDate, LocalDate endDate) {
        return (root, query, cb) -> {
            if (startDate == null && endDate == null) {
                return cb.conjunction();
            } else if (startDate == null) {
                return cb.lessThanOrEqualTo(root.get("supportCreatedDate"), endDate);
            } else if (endDate == null) {
                return cb.greaterThanOrEqualTo(root.get("supportCreatedDate"), startDate);
            } else {
                return cb.between(root.get("supportCreatedDate"), startDate, endDate);
            }
        };
    }
}
