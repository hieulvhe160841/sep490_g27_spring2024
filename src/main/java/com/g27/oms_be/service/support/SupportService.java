package com.g27.oms_be.service.support;

import com.g27.oms_be.dto.support.*;
import com.g27.oms_be.dto.user.common.BookMentorDTO;
import com.g27.oms_be.dto.user.mentor.ContactMenteeDTO;
import com.g27.oms_be.dto.user.mentor.MentorDetailDTO;
import com.g27.oms_be.dto.user.mentor.MentorSupportDTO;

import java.time.LocalDate;
import java.util.List;

public interface SupportService {
    List<ContactMenteeDTO> getMenteesByMentor(String userName);

    MentorSupportDTO mentorDetail(Long id);

    MentorCreateSupportByPostIdDTO mentorSupportMenteeByPostId(Long postId);

    List<ViewSupportListForEachPostDTO> viewAllSupportListByPostId(Long postId);

    ViewSupportDetailDTO viewSupportDetail(Long supportId);

    List<SupportListDTO> viewSupportList();

    List<SupportOfEachMentor> listOfSupportForEachMentor();

    ViewSupportDetailDTO viewSupportDetailForMentor(Long supportId);

    MentorshipRequestDTO createSupportByMentee(MentorshipRequestDTO mentorshipRequestDTO);

    AcceptOrRejectSupportDTO acceptOrRejectSupport(AcceptOrRejectSupportDTO acceptOrRejectSupportDTO, Long supportId);

    ConfirmStartSupportDTO menteeConfirmStart(Long supportId);

    ConfirmStartSupportDTO mentorConfirmStart(Long supportId);

    boolean checkWhenASupportInProgress(String mentorName, String menteeName);

    ConfirmEndSupportDTO menteeConfirmEnd(Long supportId);

    boolean ratingForSupportBySupportId(Long supportId, Double rating, String feedback);

    List<SupportOfEachMentee> viewMentorShipRequest(String username);

    List<BookMentorDTO> getListBookedMentor(String username);

    List<SupportListDTO> filterSupport(List<String> status, LocalDate startDate, LocalDate endDate);

    List<MentorDetailDTO> suggestMentor(String username);

    List<SupportOfEachMentee> filterMentorshipRequestMentee(String username, List<String> status, LocalDate startDate, LocalDate endDate);

    List<SupportOfEachMentor> filterMentorshipRequestMentor(String username, List<String> status, LocalDate startDate, LocalDate endDate);

    boolean check_Balance_When_Mentee_Start_Session(Long supportId);
}
