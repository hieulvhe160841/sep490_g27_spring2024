package com.g27.oms_be.service.support;

import com.g27.oms_be.common.SupportStatus;
import com.g27.oms_be.common.TransactionType;
import com.g27.oms_be.controller.WebSocketController;
import com.g27.oms_be.dto.socket.SocketMessageDTO;
import com.g27.oms_be.dto.support.*;
import com.g27.oms_be.dto.user.common.BookMentorDTO;
import com.g27.oms_be.dto.user.common.UserDetailDTO;
import com.g27.oms_be.dto.user.mentor.ContactMenteeDTO;
import com.g27.oms_be.dto.user.mentor.MentorDetailDTO;
import com.g27.oms_be.dto.user.mentor.MentorSupportDTO;
import com.g27.oms_be.entities.*;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.*;
import com.g27.oms_be.service.post.PostService;
import com.g27.oms_be.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SupportServiceImpl implements SupportService {
    private UserService userService;
    private SupportRepository supportRepository;
    private PostService postService;
    private UserRepository userRepository;
    private PostRepository postRepository;
    private TransactionRepository transactionRepository;
    private SupportTransactionRepository supportTransactionRepository;
    private UserWalletRepository userWalletRepository;
    private WebSocketController webSocketController;
    private UserSkillRepository userSkillRepository;

    @Autowired
    public SupportServiceImpl(UserService userService, SupportRepository supportRepository, PostService postService, UserRepository userRepository, PostRepository postRepository, TransactionRepository transactionRepository, SupportTransactionRepository supportTransactionRepository, UserWalletRepository userWalletRepository, WebSocketController webSocketController, UserSkillRepository userSkillRepository) {
        this.userService = userService;
        this.supportRepository = supportRepository;
        this.postService = postService;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.transactionRepository = transactionRepository;
        this.supportTransactionRepository = supportTransactionRepository;
        this.userWalletRepository = userWalletRepository;
        this.webSocketController = webSocketController;
        this.userSkillRepository = userSkillRepository;
    }

    @Override
    public List<ContactMenteeDTO> getMenteesByMentor(String mentorName) {
        List<String> mentees = supportRepository.findDistinctMentee(mentorName);
        if (mentees.size() > 0) {
            List<ContactMenteeDTO> contactMenteeDTOS = new ArrayList<>();
            for (String menteeName : mentees) {
                UserDetailDTO menteeDetail = userService.getUserDetailByUserName(menteeName);
                ContactMenteeDTO contactMenteeDTO = new ContactMenteeDTO();
                contactMenteeDTO.setMentee(menteeDetail);
                Support support = supportRepository.findTopByCreatedDate(mentorName, menteeName);
                contactMenteeDTO.setLatestSupportDay(support.getSupportCreatedDate());
                contactMenteeDTOS.add(contactMenteeDTO);
            }
            return contactMenteeDTOS;
        }
        return null;
    }

    @Override
    public MentorSupportDTO mentorDetail(Long id) {
        User user = userRepository.findByUserId(id);

        List<Support> contactMenteeDTOS = supportRepository.findByMentorId(user.getUserId());
        MentorSupportDTO mentorSupportDTO = Mapper.mapEntityToDto(user, MentorSupportDTO.class);
        List<SupportDTO> supportDTOS = new ArrayList<>();
        Double totalRating = 0D;
        Long numberOfRatingUser = 0L;
        for (int i = 0; i < contactMenteeDTOS.size(); i++) {
            SupportDTO supportDTO = Mapper.mapEntityToDto(contactMenteeDTOS.get(i), SupportDTO.class);
            supportDTOS.add(supportDTO);
            if (contactMenteeDTOS.get(i).getRating() != null) {
                numberOfRatingUser++;
                totalRating = totalRating + contactMenteeDTOS.get(i).getRating();
            }
        }
        mentorSupportDTO.setSupportDTOs(supportDTOS);
        if (numberOfRatingUser > 0) {
            user.setRating(totalRating / (numberOfRatingUser * 1.0));
            userRepository.save(user);
        }
        UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(user.getUserName());
        MentorDetailDTO mentorDetailDTO = new MentorDetailDTO();
        mentorDetailDTO.setUserDetail(userDetailDTO);
        mentorSupportDTO.setMentorDetail(mentorDetailDTO);
        return mentorSupportDTO;
    }

    @Override
    public MentorCreateSupportByPostIdDTO mentorSupportMenteeByPostId(Long postId) {
        Support support = new Support();
        support.setMentorId(userRepository.findUserByUserName(SecurityContextHolder.getContext().getAuthentication().getName()));
        support.setMenteeId(userRepository.findUserByUserName(postService.getPostByID(postId).getPostCreatedUser()));
        support.setPostId(postRepository.findById(postId).get());
        support.setSupportStatus(SupportStatus.WAITING.name());
        support.setSupportCreatedDate(LocalDate.now(ZoneId.of("Asia/Ho_Chi_Minh")));
        supportRepository.save(support);
        MentorCreateSupportByPostIdDTO mentorCreateSupportByPostIdDTO = MentorCreateSupportByPostIdDTO.builder().supportId(support.getSupportId()).mentorUserName(support.getMentorId().getUserName()).menteeUserName(support.getMenteeId().getUserName()).supportCreatedDate(support.getSupportCreatedDate()).supportStatus(support.getSupportStatus()).build();
        if (supportRepository.existsById(support.getSupportId())) {
            postService.updatePostForAdminByPostId(postId, false);
            return mentorCreateSupportByPostIdDTO;
        }
        return null;
    }

    @Override
    public List<ViewSupportListForEachPostDTO> viewAllSupportListByPostId(Long postId) {
        List<Support> supportList = supportRepository.findAllByPostIdPostId(postId);
        List<ViewSupportListForEachPostDTO> viewAllSupportListForEachPost = supportList.stream().map(support -> {
            ViewSupportListForEachPostDTO viewSupportListForEachPostDTO = Mapper.mapEntityToDto(support, ViewSupportListForEachPostDTO.class);
            viewSupportListForEachPostDTO.setMentorName(support.getMentorId().getUserName());
            return viewSupportListForEachPostDTO;
        }).collect(Collectors.toList());
        return viewAllSupportListForEachPost;
    }

    @Override
    public ViewSupportDetailDTO viewSupportDetail(Long supportId) {
        Optional<Support> support = supportRepository.findById(supportId);
        if (support.isPresent()) {
            ViewSupportDetailDTO viewSupportDetailDTO = Mapper.mapEntityToDto(support.get(), ViewSupportDetailDTO.class);
            viewSupportDetailDTO.setMentorAvatar(support.get().getMentorId().getAvatar());
            viewSupportDetailDTO.setMenteeAvatar(support.get().getMenteeId().getAvatar());
            viewSupportDetailDTO.setEducationLevel(support.get().getMentorId().getEducationLevel());
            viewSupportDetailDTO.setGoal(support.get().getMenteeId().getGoal());
            viewSupportDetailDTO.setInterest(support.get().getMenteeId().getInterest());
            viewSupportDetailDTO.setMenteePhone(support.get().getMenteeId().getPhone());
            viewSupportDetailDTO.setMentorPhone(support.get().getMentorId().getPhone());
            viewSupportDetailDTO.setCostPerHour(support.get().getMentorId().getCostPerHour());
            viewSupportDetailDTO.setDuration(support.get().getDuration());
            return viewSupportDetailDTO;
        }
        return null;
    }

    @Override
    public List<SupportListDTO> viewSupportList() {
        List<Support> supportList = supportRepository.findAll();
        if (supportList.size() != 0) {
            List<SupportListDTO> supportDTOList = supportList.stream().map(support -> {
                SupportListDTO supportListDTO = Mapper.mapEntityToDto(support, SupportListDTO.class);
                supportListDTO.setMentorName(support.getMentorId().getName());
                supportListDTO.setMenteeName(support.getMenteeId().getName());
                return supportListDTO;
            }).collect(Collectors.toList());
            return supportDTOList;
        }
        return null;
    }

    @Override
    public List<SupportOfEachMentor> listOfSupportForEachMentor() {
        List<Support> listSupport = supportRepository.findAllByMentorIdUserName(SecurityContextHolder.getContext().getAuthentication().getName());
        if (listSupport != null) {
            List<SupportOfEachMentor> listSupportOfEachMentor = listSupport.stream().map(support -> {
                SupportOfEachMentor supportOfEachMentor = new SupportOfEachMentor();
                supportOfEachMentor.setSupportId(support.getSupportId());
                supportOfEachMentor.setSupportStatus(support.getSupportStatus());
                supportOfEachMentor.setSupportCreatedDate(support.getSupportCreatedDate());
                supportOfEachMentor.setMenteeAvatar(support.getMenteeId().getAvatar());
                if (support.getPostId() != null) {
                    supportOfEachMentor.setPostId(support.getPostId().getPostId());
                }
                supportOfEachMentor.setMenteeUsername(userRepository.findByUserId(support.getMenteeId().getUserId()).getUserName());
                return supportOfEachMentor;
            }).collect(Collectors.toList());
            return listSupportOfEachMentor;
        }
        return null;
    }

    @Override
    public ViewSupportDetailDTO viewSupportDetailForMentor(Long supportId) {
        Optional<Support> optionalSupport = supportRepository.findByMentorIdUserNameAndSupportId(SecurityContextHolder.getContext().getAuthentication().getName(), supportId);
        if (optionalSupport.isPresent()) {
            Support support = optionalSupport.get();
            ViewSupportDetailDTO viewSupportDetailDTO = Mapper.mapEntityToDto(support, ViewSupportDetailDTO.class);
            return viewSupportDetailDTO;
        }
        return null;
    }

    @Override
    public MentorshipRequestDTO createSupportByMentee(MentorshipRequestDTO mentorshipRequestDTO) {
        User mentee = userRepository.findUserByUserName(mentorshipRequestDTO.getMentee());
        User mentor = userRepository.findUserByUserName(mentorshipRequestDTO.getMentor());
        Support support = new Support();
        support.setMenteeId(mentee);
        support.setMentorId(mentor);
        support.setSupportStatus(SupportStatus.WAITING.name());
        support.setSupportCreatedDate(LocalDate.now(ZoneId.of("Asia/Ho_Chi_Minh")));
        support.setPrice(mentor.getCostPerHour());
        support.setMentorConfirmStart(false);
        support.setMentorConfirmStart(false);
        support.setMenteeConfirmEnd(false);
        support.setDuration(mentorshipRequestDTO.getDuration());
        supportRepository.save(support);
        mentorshipRequestDTO.setSupportCreatedDate(support.getSupportCreatedDate());
        mentorshipRequestDTO.setSupportStatus(support.getSupportStatus());
        mentorshipRequestDTO.setSupportId(support.getSupportId());
        return mentorshipRequestDTO;
    }

    @Override
    public AcceptOrRejectSupportDTO acceptOrRejectSupport(AcceptOrRejectSupportDTO acceptOrRejectSupportDTO, Long supportId) {
        Optional<Support> optionalSupport = supportRepository.findById(supportId);
        if (optionalSupport.isPresent()) {
            Support support = optionalSupport.get();
            if (acceptOrRejectSupportDTO.getStatus().equalsIgnoreCase("Accept")) {
                support.setSupportStatus(SupportStatus.ACCEPTED.name());
                if (support.getPostId() != null) {
                    Post post = postRepository.getReferenceById(support.getPostId().getPostId());
                    post.setPostStatus(true);
                    postRepository.save(post);
                }
            }
            if (acceptOrRejectSupportDTO.getStatus().equalsIgnoreCase("Reject")) {
                support.setSupportStatus(SupportStatus.REJECTED.name());
            }
            supportRepository.save(support);
            return acceptOrRejectSupportDTO;
        }
        return null;
    }

    @Override
    public ConfirmStartSupportDTO menteeConfirmStart(Long supportId) {
        Optional<Support> optionalSupport = supportRepository.findById(supportId);
        if (optionalSupport.isPresent()) {
            Support support = optionalSupport.get();
            support.setMenteeConfirmStart(true);
            if (support.isMentorConfirmStart()) {
                support.setTimeStart(LocalDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh")));
                support.setSupportStatus(SupportStatus.IN_PROGRESS.name());
                Optional<UserWallet> userWallet = userWalletRepository.findByUserUserName(support.getMenteeId().getUserName());
                if (userWallet.isPresent()) {
                    UserWallet userWallet1 = userWallet.get();
                    if (support.getPostId() != null) {
                        userWallet1.setBalance(calculateForSupportByPostId(support, userWallet1));
                    } else {
                        userWallet1.setBalance(calculateForSupportWithoutPostId(support, userWallet1));
                    }
                    userWalletRepository.save(userWallet1);
                }
                webSocketController.sendStartSessionMessage(SocketMessageDTO.builder().supportId(support.getSupportId()).message("Start Session").build(), userRepository.findByUserId(support.getMentorId().getUserId()).getUserName());
            }
            webSocketController.sendNotifySessionIsStarted(SocketMessageDTO.builder().supportId(support.getSupportId()).username(support.getMentorId().getUserName()).message("Your session has been started").build());
            supportRepository.save(support);
            ConfirmStartSupportDTO confirmStartSupportDTO = Mapper.mapEntityToDto(support, ConfirmStartSupportDTO.class);
            return confirmStartSupportDTO;
        }
        return null;
    }

    @Override
    public ConfirmStartSupportDTO mentorConfirmStart(Long supportId) {
        Optional<Support> optionalSupport = supportRepository.findById(supportId);
        if (optionalSupport.isPresent()) {
            Support support = optionalSupport.get();
            support.setMentorConfirmStart(true);
            if (support.isMenteeConfirmStart()) {
                support.setTimeStart(LocalDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh")));
                support.setSupportStatus(SupportStatus.IN_PROGRESS.name());
                Optional<UserWallet> userWallet = userWalletRepository.findByUserUserName(support.getMenteeId().getUserName());
                if (userWallet.isPresent()) {
                    UserWallet userWallet1 = userWallet.get();
                    if (support.getPostId() != null) {
                        userWallet1.setBalance(calculateForSupportByPostId(support, userWallet1));
                    } else {
                        userWallet1.setBalance(calculateForSupportWithoutPostId(support, userWallet1));
                    }
                    userWalletRepository.save(userWallet1);
                }
                webSocketController.sendStartSessionMessage(SocketMessageDTO.builder().supportId(support.getSupportId()).message("Start Session").build(), userRepository.findByUserId(support.getMenteeId().getUserId()).getUserName());
            }
            webSocketController.sendNotifySessionIsStarted(SocketMessageDTO.builder().supportId(support.getSupportId()).username(support.getMenteeId().getUserName()).message("Your session has been started").build());
            supportRepository.save(support);
            ConfirmStartSupportDTO confirmStartSupportDTO = Mapper.mapEntityToDto(support, ConfirmStartSupportDTO.class);
            return confirmStartSupportDTO;
        }
        return null;
    }

    @Override
    public boolean checkWhenASupportInProgress(String mentorName, String menteeName) {
        List<Support> inProgressSupport = supportRepository.findSupportBySupportStatusInProgress(mentorName, menteeName);
        if (inProgressSupport.size() != 0) {
            return true;
        }
        return false;
    }


    @Override
    public ConfirmEndSupportDTO menteeConfirmEnd(Long supportId) {
        Optional<Support> optionalSupport = supportRepository.findById(supportId);
        if (optionalSupport.isPresent()) {
            Support support = optionalSupport.get();
            support.setMenteeConfirmEnd(true);
            support.setTimeEnd(LocalDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh")));
            long minutesDiff = support.getTimeStart().until(support.getTimeEnd(), ChronoUnit.MINUTES); // Chênh lệch theo phút
            support.setSupportStatus(SupportStatus.DONE.name());
            Optional<UserWallet> optionalMenteeWallet = userWalletRepository.findByUserUserId(support.getMenteeId().getUserId());
            Optional<UserWallet> optionalMentorWallet = userWalletRepository.findByUserUserId(support.getMentorId().getUserId());
            UserWallet menteeWallet = optionalMenteeWallet.get();
            UserWallet mentorWallet = optionalMentorWallet.get();
            if (support.getPostId() == null) {
                Double realAmount = support.getMentorId().getCostPerHour() * (double) minutesDiff / 60.0;
                support.setPrice(realAmount);
                if (optionalMenteeWallet.isPresent() && optionalMentorWallet.isPresent()) {
                    mentorWallet.setBalance(mentorWallet.getBalance() + realAmount * 0.9);
                    mentorWallet.setLastUpdateDate(LocalDate.now(ZoneId.of("Asia/Ho_Chi_Minh")));
                    menteeWallet.setBalance(menteeWallet.getBalance() + ((support.getDuration() * support.getMentorId().getCostPerHour()) - realAmount));
                    menteeWallet.setLastUpdateDate(LocalDate.now(ZoneId.of("Asia/Ho_Chi_Minh")));
                    addTransactionAndSupportTransaction(realAmount, support);
                }
            } else {
                support.setPrice(support.getPostId().getPostPrice());

//                menteeWallet.setBalance(menteeWallet.getBalance() - support.getPostId().getPostPrice());
                mentorWallet.setBalance(mentorWallet.getBalance() + support.getPostId().getPostPrice());
                addTransactionAndSupportTransaction(support.getPostId().getPostPrice(), support);

                Optional<Post> optionalPost = postRepository.findById(support.getPostId().getPostId());
                Post post = optionalPost.get();
                post.setPostStatus(false);
                postRepository.save(post);
            }
            userWalletRepository.save(menteeWallet);
            userWalletRepository.save(mentorWallet);
            supportRepository.save(support);
            webSocketController.sendEndSessionMessage(SocketMessageDTO.builder().supportId(support.getSupportId()).message("End Session").build(), userRepository.findByUserId(support.getMentorId().getUserId()).getUserName());
            ConfirmEndSupportDTO confirmEndSupportDTO = Mapper.mapEntityToDto(support, ConfirmEndSupportDTO.class);
            return confirmEndSupportDTO;
        }
        return null;
    }

    @Override
    public boolean ratingForSupportBySupportId(Long supportId, Double rating, String feedback) {
        Optional<Support> optionalSupport = supportRepository.findBySupportIdAndMenteeIdUserIdAndSupportStatus(supportId, userRepository.findUserByUserName(SecurityContextHolder.getContext().getAuthentication().getName()).getUserId(), "DONE");
        if (optionalSupport.isPresent()) {
            Support support = optionalSupport.get();
            support.setRating(rating);
            support.setSupportFeedback(feedback);
            supportRepository.saveAndFlush(support);

            Optional<User> optionalUser = userRepository.findById(support.getMentorId().getUserId());
            if (optionalUser.isPresent()) {
                User user = optionalUser.get();

                Double averageRatingForMentor = supportRepository.findAverageRatingMentor(support.getMentorId().getUserId());
                user.setRating(averageRatingForMentor);

                userRepository.save(user);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<SupportOfEachMentee> viewMentorShipRequest(String username) {
        List<Support> supportList = supportRepository.findAllByMenteeIdUserName(username);
        List<SupportOfEachMentee> supportOfEachMenteeList = new ArrayList<>();
        for (Support support : supportList) {
            SupportOfEachMentee supportOfEachMentee = new SupportOfEachMentee();
            supportOfEachMentee.setSupportId(support.getSupportId());
            supportOfEachMentee.setSupportStatus(support.getSupportStatus());
            supportOfEachMentee.setMentorAvatar(support.getMentorId().getAvatar());
            supportOfEachMentee.setMentorUsername(support.getMentorId().getUserName());
            supportOfEachMentee.setSupportCreatedDate(support.getSupportCreatedDate());
            supportOfEachMentee.setRating(support.getRating());
            if (support.getPostId() != null) {
                supportOfEachMentee.setPostId(support.getPostId().getPostId());
            }
            supportOfEachMenteeList.add(supportOfEachMentee);
        }
        return supportOfEachMenteeList;
    }

    @Override
    public List<BookMentorDTO> getListBookedMentor(String menteeName) {
        List<String> mentors = supportRepository.findDistinctMentor(menteeName);
        if (mentors.size() > 0) {
            List<BookMentorDTO> bookedMentors = new ArrayList<>();
            for (String mentorName : mentors) {
                User user = userRepository.findUserByUserName(mentorName);
                UserDetailDTO mentorDetail = userService.getUserDetailByUserName(mentorName);
                BookMentorDTO bookMentor = new BookMentorDTO();
                MentorDetailDTO mentorDetailDTO = new MentorDetailDTO();
                mentorDetailDTO.setUserDetail(mentorDetail);
                mentorDetailDTO.setCostPerHour(user.getCostPerHour());
                mentorDetailDTO.setRating(user.getRating());
                mentorDetailDTO.setNumberOfDoneSupport((long) supportRepository.findByMentorId(user.getUserId()).size());
                Support support = supportRepository.findTopByCreatedDate(mentorName, menteeName);
                bookMentor.setLatestSupportDay(support.getSupportCreatedDate());
                bookMentor.setMentorDetail(mentorDetailDTO);
                bookedMentors.add(bookMentor);
            }
            return bookedMentors;
        }
        return null;
    }

    @Override
    public List<SupportListDTO> filterSupport(List<String> status, LocalDate startDate, LocalDate endDate) {

        List<Support> supports = supportRepository.findAll(
                Specification.where(SupportSpecifications.hasStatus(status))
                        .and(SupportSpecifications.createdBetween(startDate, endDate))
        );
        List<SupportListDTO> supportListDTOList = new ArrayList<>();

        for (Support support : supports) {
            SupportListDTO supportListDTO = new SupportListDTO();
            supportListDTO.setMenteeName(support.getMenteeId().getUserName());
            supportListDTO.setMentorName(support.getMentorId().getUserName());
            supportListDTO.setSupportCreatedDate(support.getSupportCreatedDate());
            supportListDTO.setSupportStatus(support.getSupportStatus());
            supportListDTO.setSupportId(support.getSupportId());
            supportListDTOList.add(supportListDTO);
        }
        return supportListDTOList;
    }

    @Override
    public List<MentorDetailDTO> suggestMentor(String username) {
        List<BookMentorDTO> bookMentorDTOS = getListBookedMentor(username);
        System.out.println(bookMentorDTOS);
        List<MentorDetailDTO> suggestMentors = new ArrayList<>();
        if (bookMentorDTOS != null) {
            String latestSupportMentorName = bookMentorDTOS.get(0).getMentorDetail().getUserDetail().getUserName();
            List<String> skills = new ArrayList<>();
            for (UserSkill userSkill : userRepository.findUserByUserName(latestSupportMentorName).getUserSkillList()) {
                skills.add(userSkill.getSkill().getSkillName());
            }
            List<User> mentors = userRepository.findALlMentor();
            Set<MentorDetailDTO> distinctMentors = new HashSet<>();
            for (User user : mentors) {
                for (UserSkill userSkill : user.getUserSkillList()) {
                    for (String skill : skills) {
                        if (userSkill.getSkill().getSkillName().equalsIgnoreCase(skill)) {
                            MentorDetailDTO mentorDetailDTO = new MentorDetailDTO();
                            UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(user.getUserName());
                            mentorDetailDTO.setRating(user.getRating());
                            mentorDetailDTO.setCostPerHour(user.getCostPerHour());
                            mentorDetailDTO.setUserDetail(userDetailDTO);
                            mentorDetailDTO.setNumberOfDoneSupport((long) supportRepository.findByMentorId(user.getUserId()).size());
                            distinctMentors.add(mentorDetailDTO);
                        }
                    }
                }
            }
            suggestMentors.addAll(distinctMentors);
            return suggestMentors;
        } else {
            List<User> supportmentors = userRepository.findTopByRatingAndCostPerHour();
            for (User user : supportmentors) {
                MentorDetailDTO mentorDetailDTO = new MentorDetailDTO();
                UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(user.getUserName());
                mentorDetailDTO.setRating(user.getRating());
                mentorDetailDTO.setCostPerHour(user.getCostPerHour());
                mentorDetailDTO.setUserDetail(userDetailDTO);
                mentorDetailDTO.setNumberOfDoneSupport((long) supportRepository.findByMentorId(user.getUserId()).size());
                suggestMentors.add(mentorDetailDTO);
            }
            return suggestMentors;
        }
    }

    @Override
    public List<SupportOfEachMentee> filterMentorshipRequestMentee(String username, List<String> status, LocalDate startDate, LocalDate endDate) {
        User user = userRepository.findUserByUserName(username);
        List<Support> supportList = supportRepository.findAll(SupportSpecifications.hasUsername(username, user.getRole().getName())
                .and(SupportSpecifications.hasStatus(status))
                .and(SupportSpecifications.createdBetween(startDate, endDate)));
        List<SupportOfEachMentee> supportOfEachMenteeList = new ArrayList<>();
        for (Support support : supportList) {
            SupportOfEachMentee supportOfEachMentee = new SupportOfEachMentee();
            supportOfEachMentee.setSupportId(support.getSupportId());
            supportOfEachMentee.setSupportStatus(support.getSupportStatus());
            supportOfEachMentee.setMentorAvatar(support.getMentorId().getAvatar());
            supportOfEachMentee.setMentorUsername(support.getMentorId().getUserName());
            supportOfEachMentee.setSupportCreatedDate(support.getSupportCreatedDate());
            supportOfEachMentee.setRating(support.getRating());
            supportOfEachMentee.setMentorName(support.getMentorId().getName());
            if (support.getPostId() != null) {
                supportOfEachMentee.setPostId(support.getPostId().getPostId());
            }
            supportOfEachMenteeList.add(supportOfEachMentee);
        }
        return supportOfEachMenteeList;
    }

    @Override
    public List<SupportOfEachMentor> filterMentorshipRequestMentor(String username, List<String> status, LocalDate startDate, LocalDate endDate) {
        User user = userRepository.findUserByUserName(username);
        List<Support> supportList = supportRepository.findAll(SupportSpecifications.hasUsername(username, user.getRole().getName())
                .and(SupportSpecifications.hasStatus(status))
                .and(SupportSpecifications.createdBetween(startDate, endDate)));
        List<SupportOfEachMentor> supportOfEachMentorList = new ArrayList<>();
        for (Support support : supportList) {
            SupportOfEachMentor supportOfEachMentor = new SupportOfEachMentor();
            supportOfEachMentor.setSupportId(support.getSupportId());
            supportOfEachMentor.setSupportStatus(support.getSupportStatus());
            supportOfEachMentor.setMenteeAvatar(support.getMenteeId().getAvatar());
            supportOfEachMentor.setMenteeUsername(support.getMenteeId().getUserName());
            supportOfEachMentor.setSupportCreatedDate(support.getSupportCreatedDate());
            supportOfEachMentor.setMenteeName(support.getMenteeId().getName());
            if (support.getPostId() != null) {
                supportOfEachMentor.setPostId(support.getPostId().getPostId());
            }
            supportOfEachMentorList.add(supportOfEachMentor);
        }
        return supportOfEachMentorList;
    }

    @Override
    public boolean check_Balance_When_Mentee_Start_Session(Long supportId) {
        User user = userRepository.findUserByUserName(SecurityContextHolder.getContext().getAuthentication().getName());
        Support support = supportRepository.findBySupportIdAndMenteeIdUserId(supportId, user.getUserId()).get();
        UserWallet userWallet = userWalletRepository.findByUserUserId(user.getUserId()).get();
        if (support.getPostId() == null) {
            Double moneyForSupportingWithoutPost = userRepository.findByUserId(support.getMentorId().getUserId()).getCostPerHour() * support.getDuration();
            if (userWallet.getBalance() >= moneyForSupportingWithoutPost) {
                return true;
            }
        }
        if (support.getPostId() != null) {
            Double moneyForSupportingWithPost = support.getPostId().getPostPrice();
            if (userWallet.getBalance() >= moneyForSupportingWithPost) {
                return true;
            }
        }
        return false;
    }

    Double calculateForSupportByPostId(Support support, UserWallet userWallet) {
        return userWallet.getBalance() - (support.getPostId().getPostPrice());
    }

    Double calculateForSupportWithoutPostId(Support support, UserWallet userWallet) {
        return userWallet.getBalance() - (support.getDuration() * support.getMentorId().getCostPerHour());
    }

    public void addTransactionAndSupportTransaction(Double amount, Support support) {
        Transaction transaction = new Transaction();
        transaction.setTimeStamp(LocalDate.now());
        transaction.setAmount(amount);
        transaction.setTransactionType(TransactionType.SUPPORT.name());
        transaction.setTransactionStatus("done");
        transactionRepository.saveAndFlush(transaction);

        SupportTransaction supportTransaction = new SupportTransaction();
        supportTransaction.setTransaction(transaction);
        supportTransaction.setSupport(support);
        supportTransactionRepository.saveAndFlush(supportTransaction);
    }
}
