package com.g27.oms_be.service.denomination;

import com.g27.oms_be.dto.denomination.DenominationDTO;

import java.util.List;

public interface DenominationService {
    List<DenominationDTO> getAlDenominationList();
}
