package com.g27.oms_be.service.denomination;

import com.g27.oms_be.dto.denomination.DenominationDTO;
import com.g27.oms_be.entities.Denomination;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.DenominationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DenominationServiceImpl implements DenominationService {
    private DenominationRepository denominationRepository;

    @Autowired
    public DenominationServiceImpl(DenominationRepository denominationRepository) {
        this.denominationRepository = denominationRepository;
    }

    @Override
    public List<DenominationDTO> getAlDenominationList() {
        List<Denomination> denominationList = denominationRepository.findAll();
        if (denominationList != null) {
            List<DenominationDTO> denominationDTOList = denominationList.stream().map(denomination -> {
                DenominationDTO denominationDTO = Mapper.mapEntityToDto(denomination, DenominationDTO.class);
                return denominationDTO;
            }).collect(Collectors.toList());
            return denominationDTOList;
        }
        return null;
    }
}
