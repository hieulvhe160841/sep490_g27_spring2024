package com.g27.oms_be.service.video;

import com.g27.oms_be.dto.video.VideoDTO;
import com.g27.oms_be.entities.Course;
import com.g27.oms_be.entities.Video;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.CourseRepository;
import com.g27.oms_be.repository.VideoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VideoServiceImpl implements VideoService {
    private VideoRepository videoRepository;
    private CourseRepository courseRepository;

    public VideoServiceImpl(VideoRepository videoRepository, CourseRepository courseRepository) {
        this.videoRepository = videoRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<VideoDTO> getAllByCourse(Long courseId) {
        List<Video> list = videoRepository.findByCourseAndOrderByOrderInCourse(courseId);
        List<VideoDTO> videoDTOS = new ArrayList<>();
        for (Video video : list) {
            videoDTOS.add(Mapper.mapEntityToDto(video, VideoDTO.class));
        }
        return videoDTOS;
    }

    @Override
    public boolean deleteVideoInCourse(Long videoId, Long courseId) {
        Optional<Video> optionalVideo = videoRepository.existsByCourse(courseId, videoId);
        if (optionalVideo.isPresent()) {
            Course course = courseRepository.findByCourseId(courseId);
            Video video = videoRepository.getReferenceById(videoId);
            videoRepository.delete(video);
            course.setNumbersOfLesson(course.getNumbersOfLesson() - 1);
            courseRepository.saveAndFlush(course);
            return true;
        }
        return false;
    }
}
