package com.g27.oms_be.service.video;

import com.g27.oms_be.dto.video.VideoDTO;
import com.g27.oms_be.entities.Video;

import java.util.List;

public interface VideoService {
    List<VideoDTO> getAllByCourse(Long courseId);

    boolean deleteVideoInCourse(Long videoId, Long courseId);
}
