package com.g27.oms_be.service.userSkill;

import com.g27.oms_be.dto.user.mentor.MentorSkillDTO;
import com.g27.oms_be.dto.userSkill.UserSkillDetailDTO;
import com.g27.oms_be.entities.Skill;
import com.g27.oms_be.entities.User;
import com.g27.oms_be.entities.UserSkill;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.SkillRepository;
import com.g27.oms_be.repository.UserRepository;
import com.g27.oms_be.repository.UserSkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserSkillServiceImpl implements UserSkillService {
    private UserSkillRepository userSkillRepository;
    private UserRepository userRepository;
    private SkillRepository skillRepository;

    @Autowired
    public UserSkillServiceImpl(UserSkillRepository userSkillRepository, UserRepository userRepository, SkillRepository skillRepository) {
        this.userSkillRepository = userSkillRepository;
        this.userRepository = userRepository;
        this.skillRepository = skillRepository;
    }

    @Override
    public List<UserSkillDetailDTO> userSkillListByUserId(Long userID) {
        List<UserSkill> userSkillList = userSkillRepository.findByUserUserId(userID);
        if (userSkillList != null){
            List<UserSkillDetailDTO> userSkillDetailDTOList = userSkillList.stream().map(userSkill -> {
                UserSkillDetailDTO userSkillDetailDTO = new UserSkillDetailDTO();
                userSkillDetailDTO.setSkillName(userSkill.getSkill().getSkillName());
                return userSkillDetailDTO;
            }).collect(Collectors.toList());
            return userSkillDetailDTOList;
        }
        return null;
    }

    @Override
    public MentorSkillDTO registerAsMentor(MentorSkillDTO mentorSkillDTO) {
        User user = userRepository.findUserByUserName(mentorSkillDTO.getUserName());
        List<UserSkill> userSkillList = userSkillRepository.findByUserUserId(user.getUserId());
        if (!userSkillList.isEmpty()) {
            for (UserSkill userSkill : userSkillList) {
                userSkillRepository.delete(userSkill);
            }
        }
        user.setEducationLevel(mentorSkillDTO.getEducationLevel());
        user.setExperience(mentorSkillDTO.getExperience());
        user.setCostPerHour(mentorSkillDTO.getCostPerHour());
        for (String skillName : mentorSkillDTO.getSkills()) {
            Skill skill = skillRepository.findByskillName(skillName);

            UserSkill userSkill = new UserSkill();
            userSkill.setSkill(skill);
            userSkill.setUser(user);
            userSkillRepository.save(userSkill);

        }
        userRepository.save(user);

        return mentorSkillDTO;
    }

    @Override
    public UserSkillDetailDTO getByUserAndSkill(String userName, String skillName) {
        Optional<UserSkill> userSkill = userSkillRepository.findByUserNameAndSkill(userName, skillName);
        if (userSkill.isPresent()) {
            return Mapper.mapEntityToDto(userSkill.get(), UserSkillDetailDTO.class);
        }
        return null;
    }
}
