package com.g27.oms_be.service.userSkill;


import com.g27.oms_be.dto.user.mentor.MentorSkillDTO;
import com.g27.oms_be.dto.userSkill.UserSkillDetailDTO;

import java.util.List;
import java.util.Optional;

public interface UserSkillService {
    List<UserSkillDetailDTO> userSkillListByUserId(Long userID);
    MentorSkillDTO registerAsMentor(MentorSkillDTO mentorSkillDTO);
    UserSkillDetailDTO getByUserAndSkill(String userName,String skillName);
}
