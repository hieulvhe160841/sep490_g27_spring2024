package com.g27.oms_be.service.transaction;

import com.g27.oms_be.dto.transaction.TransactionDTO;

import java.util.List;

public interface TransactionService {
    Double totalAmountOfSupportAndCourseRegistrationTransaction();

    List<TransactionDTO> listAllTransaction();
}
