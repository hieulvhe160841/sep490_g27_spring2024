package com.g27.oms_be.service.transaction;

import com.g27.oms_be.dto.transaction.TransactionDTO;
import com.g27.oms_be.entities.Transaction;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {
    private TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Double totalAmountOfSupportAndCourseRegistrationTransaction() {
        Double result = transactionRepository.totalAmountOfCourseRegistrationAndSupportTransaction();
        return transactionRepository.totalAmountOfCourseRegistrationAndSupportTransaction();
    }

    @Override
    public List<TransactionDTO> listAllTransaction() {
        List<Transaction> transactionList = transactionRepository.findAll();
        if (transactionList != null) {
            List<TransactionDTO> listTransactionDTO = transactionList.stream().map(transaction -> {
                TransactionDTO transactionDTO = Mapper.mapEntityToDto(transaction, TransactionDTO.class);
                return transactionDTO;
            }).collect(Collectors.toList());
            return listTransactionDTO;
        }
        return null;
    }
}
