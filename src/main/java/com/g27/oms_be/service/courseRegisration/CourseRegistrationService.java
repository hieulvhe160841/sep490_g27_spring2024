package com.g27.oms_be.service.courseRegisration;

import com.g27.oms_be.dto.course.CourseDTO;

import java.util.List;

public interface CourseRegistrationService {
    String menteePurchaseCourse(Long courseId);

    List<CourseDTO> listOfCourseThatMenteePurchased();
}
