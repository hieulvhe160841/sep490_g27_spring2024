package com.g27.oms_be.service.courseRegisration;

import com.g27.oms_be.common.TransactionType;
import com.g27.oms_be.dto.course.CourseDTO;
import com.g27.oms_be.entities.*;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CourserRegistrationServiceImpl implements CourseRegistrationService {
    private CourseRepository courseRepository;
    private CourseRegistrationRepository courseRegistrationRepository;
    private CourseRegistrationTransactionRepository courseRegistrationTransactionRepository;
    private TransactionRepository transactionRepository;
    private UserRepository userRepository;
    private UserWalletRepository userWalletRepository;

    @Autowired
    public CourserRegistrationServiceImpl(CourseRepository courseRepository, CourseRegistrationRepository courseRegistrationRepository, CourseRegistrationTransactionRepository courseRegistrationTransactionRepository, TransactionRepository transactionRepository, UserRepository userRepository, UserWalletRepository userWalletRepository) {
        this.courseRepository = courseRepository;
        this.courseRegistrationRepository = courseRegistrationRepository;
        this.courseRegistrationTransactionRepository = courseRegistrationTransactionRepository;
        this.transactionRepository = transactionRepository;
        this.userRepository = userRepository;
        this.userWalletRepository = userWalletRepository;
    }

    @Override
    public String menteePurchaseCourse(Long courseId) {
        Optional<Course> optionalCourse = courseRepository.findById(courseId);
        if (optionalCourse.isPresent()) {
            Course course = optionalCourse.get();

            Optional<UserWallet> optionalMenteeWallet = userWalletRepository.findByUserUserName(SecurityContextHolder.getContext().getAuthentication().getName());
            UserWallet menteeWallet = optionalMenteeWallet.get();

            if (courseRegistrationRepository.findByCourseCourseIdAndUserUserName(courseId, SecurityContextHolder.getContext().getAuthentication().getName()) != null) {
                return "Bạn đã đăng ký khóa học này";
            }
            if (menteeWallet.getBalance() < course.getPrice()) {
                return "Số dư của bạn không đủ";
            } else {
                CourseRegistration courseRegistration = new CourseRegistration();
                courseRegistration.setCourse(course);
                courseRegistration.setUser(userRepository.findUserByUserName(SecurityContextHolder.getContext().getAuthentication().getName()));
                courseRegistration.setDateRegistered(LocalDate.now());
                courseRegistrationRepository.saveAndFlush(courseRegistration);

                CourseRegistrationTransaction courseRegistrationTransaction = new CourseRegistrationTransaction();
                courseRegistrationTransaction.setCourseRegistration(courseRegistration);
                courseRegistrationTransactionRepository.saveAndFlush(courseRegistrationTransaction);

                Transaction transaction = new Transaction();
                transaction.setTransactionType(TransactionType.COURSE_PURCHASE.name());
                transaction.setAmount(course.getPrice());
                transaction.setTimeStamp(LocalDate.now());
                transaction.setTransactionStatus("DONE");
                transaction.setCourseRegistrationTransaction(courseRegistrationTransaction);

                courseRegistrationTransaction.setTransaction(transaction);

                transactionRepository.saveAndFlush(transaction);

                menteeWallet.setBalance(menteeWallet.getBalance() - course.getPrice());
                menteeWallet.setLastUpdateDate(LocalDate.now());

                Optional<UserWallet> optionalMentorWallet = userWalletRepository.findByUserUserId(course.getUser().getUserId());
                UserWallet mentorWallet = optionalMentorWallet.get();
                mentorWallet.setBalance(mentorWallet.getBalance() + (course.getPrice() * 0.9));
                mentorWallet.setLastUpdateDate(LocalDate.now());

                userWalletRepository.save(menteeWallet);
                userWalletRepository.save(mentorWallet);

                return "Đăng ký khóa học thành công";
            }
        }
        return "Đăng ký khóa học không thành công, không tìm thấy khóa học";
    }

    @Override
    public List<CourseDTO> listOfCourseThatMenteePurchased() {
        List<CourseRegistration> courseRegistrationList = courseRegistrationRepository.findByUserName(SecurityContextHolder.getContext().getAuthentication().getName());
        if (courseRegistrationList != null) {
            List<CourseDTO> courseDTOList = courseRegistrationList.stream().map(courseRegistration -> {
                CourseDTO courseDTO = Mapper.mapEntityToDto(courseRegistration.getCourse(), CourseDTO.class);
                return courseDTO;
            }).collect(Collectors.toList());
            return courseDTOList;
        }
        return null;
    }
}
