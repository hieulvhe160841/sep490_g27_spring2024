package com.g27.oms_be.service.role;

import com.g27.oms_be.entities.Role;

public interface RoleService {
    Role findByName(String name);

}
