package com.g27.oms_be.service.denominationTransaction;

import com.g27.oms_be.dto.denominationTransaction.DenominationTransactionDTO;
import com.g27.oms_be.dto.denominationTransaction.MenteeViewPersonalDenominationTransactionDTO;

import java.util.List;

public interface DenominationTransactionService {
    DenominationTransactionDTO getDenominationTransactionByOrderId(String orderId);

    List<MenteeViewPersonalDenominationTransactionDTO> getAllListOfDenominationTransactionHaveMade();
}
