package com.g27.oms_be.service.denominationTransaction;

import com.g27.oms_be.dto.denominationTransaction.DenominationTransactionDTO;
import com.g27.oms_be.dto.denominationTransaction.MenteeViewPersonalDenominationTransactionDTO;
import com.g27.oms_be.entities.DenominationTransaction;
import com.g27.oms_be.repository.DenominationTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DenominationTransactionServiceImpl implements DenominationTransactionService {
    private DenominationTransactionRepository denominationTransactionRepository;

    @Autowired
    public DenominationTransactionServiceImpl(DenominationTransactionRepository denominationTransactionRepository) {
        this.denominationTransactionRepository = denominationTransactionRepository;
    }

    @Override
    public DenominationTransactionDTO getDenominationTransactionByOrderId(String orderId) {
        DenominationTransaction denominationTransaction = denominationTransactionRepository.findByOrderId(orderId);
        if (denominationTransaction != null) {
            DenominationTransactionDTO denominationTransactionDTO = new DenominationTransactionDTO();
            denominationTransactionDTO.setDenominationId(denominationTransaction.getDenomination().getDenominationId());
            denominationTransactionDTO.setDenominationTransactionId(denominationTransaction.getDenominationTransactionId());
            denominationTransactionDTO.setUserId(denominationTransaction.getUser().getUserId());
            denominationTransactionDTO.setOrderId(denominationTransaction.getOrderId());
            denominationTransactionDTO.setTransactionId(denominationTransaction.getTransaction().getTransactionId());
            return denominationTransactionDTO;
        }
        return null;
    }

    @Override
    public List<MenteeViewPersonalDenominationTransactionDTO> getAllListOfDenominationTransactionHaveMade() {
        List<DenominationTransaction> denominationTransactionList = denominationTransactionRepository.findAllByUserUserName(SecurityContextHolder.getContext().getAuthentication().getName());
        if (denominationTransactionList != null) {
            List<MenteeViewPersonalDenominationTransactionDTO> denominationTransactionDTOList = denominationTransactionList.stream().map(denominationTransaction -> {
                MenteeViewPersonalDenominationTransactionDTO menteeViewPersonalDenominationTransactionDTO = new MenteeViewPersonalDenominationTransactionDTO();
                menteeViewPersonalDenominationTransactionDTO.setDenominationTransactionId(denominationTransaction.getDenominationTransactionId());
                menteeViewPersonalDenominationTransactionDTO.setValue(denominationTransaction.getDenomination().getValue());
                menteeViewPersonalDenominationTransactionDTO.setOrderId(denominationTransaction.getOrderId());
                menteeViewPersonalDenominationTransactionDTO.setCreatedDate(denominationTransaction.getTransaction().getTimeStamp());
                return menteeViewPersonalDenominationTransactionDTO;
            }).collect(Collectors.toList());
            return denominationTransactionDTOList;
        }
        return null;
    }
}
