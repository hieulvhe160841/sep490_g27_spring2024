package com.g27.oms_be.service.course;

import com.g27.oms_be.dto.course.*;
import com.g27.oms_be.entities.Course;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface CourseService {
    List<CourseForAdminDashBoardDTO> getTop5RecentCourseForAdminDashBoard();

    List<NumberOfCoursePerMonthAndYearDTO> getNumberOfCourseCreatedPerMonth();

    void map(List<CourseDTO> courseDTOS, List<Course> courseList);

    CourseDetailDTO getCourseDetail(Long id);

    RegisterCourseDTO regisCourse(RegisterCourseDTO registerCourseDTO, List<String> title, List<MultipartFile> file, MultipartFile courseAvatar) throws IOException;

    List<CourseDTO> getCoursesOfMentor(String username);

    List<CourseDTO> suggestCourse(String username);

    boolean checkExistCourse(Long id);

    List<CourseDTO> latestCourses();

    EditCourseDTO editCourse(EditCourseDTO editCourseDTO);

    List<CourseDTO> filterCourse(String mentorName, String courseName, Double fromPrice, Double toPrice);

    boolean checkUserCreateCourse(String userName, Long courseId);
}
