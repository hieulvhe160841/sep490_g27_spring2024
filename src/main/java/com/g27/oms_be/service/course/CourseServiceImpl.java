package com.g27.oms_be.service.course;

import com.g27.oms_be.dto.course.*;
import com.g27.oms_be.dto.user.common.BookMentorDTO;
import com.g27.oms_be.dto.user.common.UserDetailDTO;
import com.g27.oms_be.dto.user.mentor.MentorSupportDTO;
import com.g27.oms_be.dto.video.VideoDTO;
import com.g27.oms_be.entities.Course;
import com.g27.oms_be.entities.CourseRegistration;
import com.g27.oms_be.entities.User;
import com.g27.oms_be.entities.Video;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.*;
import com.g27.oms_be.service.firebase.FirebaseService;
import com.g27.oms_be.service.support.SupportService;
import com.g27.oms_be.service.user.UserService;
import com.g27.oms_be.service.video.VideoService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service

public class CourseServiceImpl implements CourseService {
    private CourseRepository courseRepository;
    private UserService userService;
    private VideoService videoService;
    private SupportService supportService;
    private UserRepository userRepository;
    private VideoRepository videoRepository;
    private FirebaseService firebaseService;
    private SkillRepository skillRepository;

    private CourseRegistrationRepository courseRegistrationRepository;

    public CourseServiceImpl(CourseRepository courseRepository, UserService userService, VideoService videoService, SupportService supportService, UserRepository userRepository, VideoRepository videoRepository, FirebaseService firebaseService, SkillRepository skillRepository, CourseRegistrationRepository courseRegistrationRepository) {
        this.courseRepository = courseRepository;
        this.userService = userService;
        this.videoService = videoService;
        this.supportService = supportService;
        this.userRepository = userRepository;
        this.videoRepository = videoRepository;
        this.firebaseService = firebaseService;
        this.skillRepository = skillRepository;
        this.courseRegistrationRepository = courseRegistrationRepository;
    }

    @Override
    public List<CourseForAdminDashBoardDTO> getTop5RecentCourseForAdminDashBoard() {
        List<Course> courseList = courseRepository.findTop5ByOrderByCourseCreatedDateDesc();
        List<CourseForAdminDashBoardDTO> courseForAdminDashBoardDTOList = courseList.stream().
                map(course -> {
                    CourseForAdminDashBoardDTO courseForAdminDashBoardDTO = Mapper.mapEntityToDto(course, CourseForAdminDashBoardDTO.class);
                    ViewCourseOwnerDTO viewCourseOwnerDTO = Mapper.mapEntityToDto(course.getUser(), ViewCourseOwnerDTO.class);
                    courseForAdminDashBoardDTO.setViewCourseOwnerDTO(viewCourseOwnerDTO);
                    return courseForAdminDashBoardDTO;
                }).collect(Collectors.toList());

        return courseForAdminDashBoardDTOList;
    }

    @Override
    public List<NumberOfCoursePerMonthAndYearDTO> getNumberOfCourseCreatedPerMonth() {
        List<NumberOfCoursePerMonthAndYearDTO> numberOfCoursePerMonthAndYearDTOList = courseRepository.countCourseCreatedPerMonth(LocalDate.now().getYear());
        return numberOfCoursePerMonthAndYearDTOList;
    }

    @Override
    public void map(List<CourseDTO> courseDTOS, List<Course> courseList) {
        for (Course course :
                courseList) {
            CourseDTO courseDTO = new CourseDTO();
            courseDTO = Mapper.mapEntityToDto(course, CourseDTO.class);
            courseDTO.setMentorName(course.getUser().getName());
            courseDTOS.add(courseDTO);
        }
    }

    @Override
    public CourseDetailDTO getCourseDetail(Long id) {
        Course course = courseRepository.findByCourseId(id);
        if (course != null) {
            CourseDetailDTO courseDetailDTO = Mapper.mapEntityToDto(course, CourseDetailDTO.class);
            List<VideoDTO> list = videoService.getAllByCourse(id);
            UserDetailDTO userDetailDTO = userService.getByUserId(course.getUser().getUserId());
            courseDetailDTO.setVideoDTOs(list);
            courseDetailDTO.setMentorName(userDetailDTO.getName());
            courseDetailDTO.setMentorUsername(userDetailDTO.getUserName());
            return courseDetailDTO;
        }
        return null;
    }

    @Override
    public RegisterCourseDTO regisCourse(RegisterCourseDTO registerCourseDTO, List<String> title, List<MultipartFile> file, MultipartFile courseAvatar) throws IOException {
        Course course = courseRepository.findByCourseId(registerCourseDTO.getCourseId());
        User user = userRepository.findUserByUserName(registerCourseDTO.getMentorName());
        List<VideoDTO> videoDTOS = new ArrayList<>();
        //video save...
        for (int i = 0; i < file.size(); i++) {
            byte[] fileContent = file.get(i).getBytes();
            String videoUrl = firebaseService.uploadFile(file.get(i).getOriginalFilename(), fileContent);
            Video video = new Video();
            video.setVideoUrl(videoUrl);
            video.setVideoTitle(title.get(i));
            video.setCourse(course);
            video.setOrderInCourse(i + 1);
            videoRepository.save(video);
            VideoDTO videoDTO = Mapper.mapEntityToDto(video, VideoDTO.class);
            videoDTOS.add(videoDTO);
        }
        course.setCourseName(registerCourseDTO.getCourseName());
        course.setCourseModifiedDate(LocalDate.now());
        course.setPrice(registerCourseDTO.getPrice());
        course.setDescription(registerCourseDTO.getDescription());
        course.setUser(user);
        course.setCourseRating(0D);
        course.setNumbersOfLesson(videoRepository.countVideosByCourseId(registerCourseDTO.getCourseId()));
        if (!courseAvatar.isEmpty()) {
            String avatar = firebaseService.uploadFile(courseAvatar.getOriginalFilename(), courseAvatar.getBytes());
            course.setAvatar(avatar);
        }
        courseRepository.save(course);
        registerCourseDTO.setLectureVideo(videoDTOS);
        registerCourseDTO.setAvatar(course.getAvatar());
        registerCourseDTO.setNumberOfLesson(course.getNumbersOfLesson());
        return registerCourseDTO;
    }

    @Override
    public List<CourseDTO> getCoursesOfMentor(String username) {
        List<Course> courseList = courseRepository.findByUserName(username);
        if (!courseList.isEmpty()) {
            List<CourseDTO> courseDTOS = new ArrayList<>();
            for (Course course : courseList) {
                CourseDTO courseDTO = Mapper.mapEntityToDto(course, CourseDTO.class);
                courseDTO.setMentorName(course.getUser().getName());
                courseDTOS.add(courseDTO);
            }
            return courseDTOS;
        }
        return null;
    }

    @Override
    public List<CourseDTO> suggestCourse(String username) {
        List<CourseRegistration> courseRegistrations = courseRegistrationRepository.findByUserName(username);
        if (courseRegistrations.size() == 0) {
            List<BookMentorDTO> bookMentorDTOS = supportService.getListBookedMentor(username);
            System.out.println(bookMentorDTOS);
            List<CourseDTO> suggestCourseDTO = new ArrayList<>();
            if (bookMentorDTOS == null) {
//                System.out.println("th1");
                List<Course> suggestCourse = courseRepository.findTopCourses();
                for (Course course : suggestCourse) {
                    CourseDTO courseDTO = Mapper.mapEntityToDto(course, CourseDTO.class);
                    courseDTO.setMentorName(course.getUser().getName());
                    suggestCourseDTO.add(courseDTO);
                }
                return suggestCourseDTO;
            } else {
//                System.out.println("th2");
                List<CourseDTO> suggestcourseDTOS = new ArrayList<>();
                for (BookMentorDTO bookMentor : bookMentorDTOS) {
                    List<Course> mentorCourses = courseRepository.findByUserName(bookMentor.getMentorDetail().getUserDetail().getUserName());
                    for (Course course : mentorCourses) {
                        CourseDTO courseDTO = Mapper.mapEntityToDto(course, CourseDTO.class);
                        courseDTO.setMentorName(course.getUser().getName());
                        suggestcourseDTOS.add(courseDTO);
                    }
                }
                return suggestcourseDTOS;
            }
        } else {
//            System.out.println("th3");
            Set<String> mentorNames = new HashSet<>();
            List<CourseDTO> suggestCourseDTO2s = new ArrayList<>();
            for (CourseRegistration courseRegistration : courseRegistrations) {
                String mentorName = courseRegistration.getCourse().getUser().getUserName();
                mentorNames.add(mentorName);
            }
            for (String mentorname : mentorNames) {
                List<Course> courseList = courseRepository.findByUserName(mentorname);
                for (Course course : courseList) {
                    CourseDTO courseDTO = Mapper.mapEntityToDto(course, CourseDTO.class);
                    courseDTO.setMentorName(course.getUser().getName());
                    suggestCourseDTO2s.add(courseDTO);
                }
            }
            return suggestCourseDTO2s;
        }
    }

    @Override
    public boolean checkExistCourse(Long id) {
        Optional<Course> optionalCourse = courseRepository.findById(id);
        if (optionalCourse.isPresent()) {
            return true;
        }
        return false;
    }

    @Override
    public List<CourseDTO> latestCourses() {
        List<Course> latestCourses = courseRepository.findTopFiveCourses();
        if (latestCourses != null) {
            List<CourseDTO> latestCourseDTOS = new ArrayList<>();

            for (Course course : latestCourses) {
                CourseDTO courseDTO = Mapper.mapEntityToDto(course, CourseDTO.class);
                latestCourseDTOS.add(courseDTO);
            }
            return latestCourseDTOS;
        }
        return null;
    }

    @Override
    public EditCourseDTO editCourse(EditCourseDTO editCourseDTO) {
        Course course = courseRepository.findByCourseId(editCourseDTO.getCourseId());
        if (course == null) {
            return null;
        }
        course.setPrice(editCourseDTO.getPrice());
        course.setCourseModifiedDate(LocalDate.now());
        course.setDescription(editCourseDTO.getDescription());
        courseRepository.save(course);
        editCourseDTO.setDateModified(LocalDate.now());
        return editCourseDTO;
    }

    @Override
    public List<CourseDTO> filterCourse(String mentorName, String courseName, Double fromPrice, Double toPrice) {
        List<Course> courseList = courseRepository.findAll(
                Specification.where(CourseSpecifications.courseNameContains(courseName))
                        .and(CourseSpecifications.hasUserWithName(mentorName))
                        .and(CourseSpecifications.hasPriceBetween(fromPrice, toPrice))
        );
        List<CourseDTO> courseDTOList = new ArrayList<>();
        map(courseDTOList,courseList);
        return courseDTOList;
    }

    @Override
    public boolean checkUserCreateCourse(String userName, Long courseId) {
        Optional<Course> optionalCourseByUser = courseRepository.existsCourseByUser(userName, courseId);
        if (optionalCourseByUser.isPresent()) {
            return true;
        }
        return false;
    }
}
