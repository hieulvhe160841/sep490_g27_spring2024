package com.g27.oms_be.service.course;

import com.g27.oms_be.entities.Course;
import com.g27.oms_be.entities.User;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public class CourseSpecifications {
    public static Specification<Course> courseNameContains(String courseName) {
        return (root, query, cb) -> {
            if (courseName == null || courseName.isEmpty()) {
                return cb.conjunction(); // Trường hợp không có tham số hoặc tham số trống.
            }
            return cb.like(cb.lower(root.get("courseName")), "%" + courseName.toLowerCase() + "%");
        };
    }
    public static Specification<Course> createdBetween(LocalDate startDate, LocalDate endDate) {
        return (root, query, cb) -> {
            if (startDate == null && endDate == null) {
                return cb.conjunction();
            } else if (startDate == null) {
                return cb.lessThanOrEqualTo(root.get("createdDate"), endDate);
            } else if (endDate == null) {
                return cb.greaterThanOrEqualTo(root.get("createdDate"), startDate);
            } else {
                return cb.between(root.get("createdDate"), startDate, endDate);
            }
        };
    }
    public static Specification<Course> hasUserWithName(String name) {
        return (root, query, cb) -> {
            if (name == null || name.isEmpty()) {
                return cb.conjunction();
            }
            Join<Course, User> userJoinCourse = root.join("user", JoinType.INNER);
            return cb.like(cb.lower(userJoinCourse.get("name")), "%" + name.toLowerCase() + "%");
        };
    }
    public static Specification<Course> hasPriceBetween(Double fromPrice, Double toPrice){
        return (root, query, cb) -> {
            Predicate predicate = cb.conjunction(); // Tạo điều kiện mặc định, không lọc.

            if (fromPrice != null) {
                predicate = cb.and(predicate, cb.greaterThanOrEqualTo(root.get("price"), fromPrice));
            }
            if (toPrice != null) {
                predicate = cb.and(predicate, cb.lessThanOrEqualTo(root.get("price"), toPrice));
            }

            return predicate;
        };

    }
}
