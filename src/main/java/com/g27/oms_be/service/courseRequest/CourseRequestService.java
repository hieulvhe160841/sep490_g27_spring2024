package com.g27.oms_be.service.courseRequest;

import com.g27.oms_be.dto.courseRequest.CourseRequestDTO;
import com.g27.oms_be.dto.courseRequest.CourseRequestDetailDTO;
import com.g27.oms_be.dto.courseRequest.ListCourseRequestForEducationStaffDTO;
import com.g27.oms_be.dto.mentorRequest.AcceptOrDenyRequestDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface CourseRequestService {
    CourseRequestDTO createCourseRequest(CourseRequestDTO courseRequestDTO, MultipartFile imageFile, MultipartFile videoFile) throws IOException;

    List<ListCourseRequestForEducationStaffDTO> listCourseForEducationStaff();

    CourseRequestDetailDTO viewCourseRequestDetailForEducationStaff(Long courseRequestId) throws IOException;

    AcceptOrDenyRequestDTO acceptOrDenyCourseRequest(AcceptOrDenyRequestDTO acceptOrDenyRequestDTO, Long requestId);

    CourseRequestDetailDTO viewCourseRequestDetailForMentor(Long courseRequestId);

    List<CourseRequestDTO> mentorCourseRequest(String username);
}
