package com.g27.oms_be.service.courseRequest;

import com.g27.oms_be.dto.courseRequest.CourseRequestDTO;
import com.g27.oms_be.dto.courseRequest.CourseRequestDetailDTO;
import com.g27.oms_be.dto.courseRequest.ListCourseRequestForEducationStaffDTO;
import com.g27.oms_be.dto.mentorRequest.AcceptOrDenyRequestDTO;
import com.g27.oms_be.entities.Course;
import com.g27.oms_be.entities.CourseRequest;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.CourseRepository;
import com.g27.oms_be.repository.CourseRequestRepository;
import com.g27.oms_be.repository.UserRepository;
import com.g27.oms_be.service.firebase.FirebaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CourseRequestServiceImpl implements CourseRequestService {
    private CourseRequestRepository courseRequestRepository;
    private UserRepository userRepository;
    private FirebaseService firebaseService;
    private CourseRepository courseRepository;

    @Autowired
    public CourseRequestServiceImpl(CourseRequestRepository courseRequestRepository, UserRepository userRepository, FirebaseService firebaseService, CourseRepository courseRepository) {
        this.courseRequestRepository = courseRequestRepository;
        this.userRepository = userRepository;
        this.firebaseService = firebaseService;
        this.courseRepository = courseRepository;
    }

    @Override
    public CourseRequestDTO createCourseRequest(CourseRequestDTO courseRequestDTO, MultipartFile imageFile, MultipartFile videoFile) throws IOException {
        String urlCourseAvatar = uploadFileToFireBase(imageFile, "image/");
        String urlVideoDemoSource = uploadFileToFireBase(videoFile, "video/");
        CourseRequest courseRequest = Mapper.mapDtoToEntity(courseRequestDTO, CourseRequest.class);
        courseRequest.setCourseRequestStatus("Pending");
        courseRequest.setCourseAvatar(urlCourseAvatar);
        courseRequest.setDemoSource(urlVideoDemoSource);
        courseRequest.setUser(userRepository.findUserByUserName(SecurityContextHolder.getContext().getAuthentication().getName()));
        courseRequestRepository.save(courseRequest);
        courseRequestDTO.setCourseRequestId(courseRequest.getCourseRequestId());
        courseRequestDTO.setCourseAvatar(urlCourseAvatar);
        courseRequestDTO.setDemoSource(urlVideoDemoSource);
        return courseRequestDTO;
    }

    @Override
    public List<ListCourseRequestForEducationStaffDTO> listCourseForEducationStaff() {
        List<CourseRequest> courseRequestList = courseRequestRepository.findByCourseRequestStatusEqualsIgnoreCase("pending");
        List<ListCourseRequestForEducationStaffDTO> listCourseRequestForEducationStaffDTO = courseRequestList.stream().map(courseRequest -> {
            ListCourseRequestForEducationStaffDTO listCourseRequestForEducationStaff = Mapper.mapEntityToDto(courseRequest, ListCourseRequestForEducationStaffDTO.class);
            listCourseRequestForEducationStaff.setMentorUserName(courseRequest.getUser().getUserName());
            return listCourseRequestForEducationStaff;
        }).collect(Collectors.toList());
        return listCourseRequestForEducationStaffDTO;
    }

    @Override
    public CourseRequestDetailDTO viewCourseRequestDetailForEducationStaff(Long courseRequestId) throws IOException {
        Optional<CourseRequest> optionalCourseRequest = courseRequestRepository.findById(courseRequestId);
        if (optionalCourseRequest.isPresent()) {
            CourseRequest courseRequest = optionalCourseRequest.get();
            CourseRequestDetailDTO courseRequestDetailDTO = Mapper.mapEntityToDto(courseRequest, CourseRequestDetailDTO.class);
            courseRequestDetailDTO.setMentorUserName(userRepository.findByUserId(courseRequest.getUser().getUserId()).getUserName());
            return courseRequestDetailDTO;
        }
        return null;
    }

    @Override
    public AcceptOrDenyRequestDTO acceptOrDenyCourseRequest(AcceptOrDenyRequestDTO acceptOrDenyRequestDTO, Long requestId) {
        Optional<CourseRequest> courseRequest = courseRequestRepository.findById(requestId);
        if (courseRequest.isPresent()) {
            CourseRequest updateCourseRequest = courseRequest.get();
            if (acceptOrDenyRequestDTO.getFeedback() != null && !acceptOrDenyRequestDTO.getFeedback().trim().isEmpty()) {
                updateCourseRequest.setCourseRequestFeedback(acceptOrDenyRequestDTO.getFeedback());
            }
            if (acceptOrDenyRequestDTO.getStatus() != null && !acceptOrDenyRequestDTO.getStatus().trim().isEmpty()) {
                updateCourseRequest.setCourseRequestStatus(acceptOrDenyRequestDTO.getStatus());
            }
            courseRequestRepository.save(updateCourseRequest);
            if (acceptOrDenyRequestDTO.getStatus().equalsIgnoreCase("accepted")) {
                Course course = new Course();
                course.setCourseName(updateCourseRequest.getCourseName());
                course.setUser(updateCourseRequest.getUser());
                course.setPrice(updateCourseRequest.getPrice());
                course.setDescription(updateCourseRequest.getDescription());
                course.setCourseRating(0.0);
                course.setCourseCreatedDate(LocalDate.now());
                course.setCourseModifiedDate(LocalDate.now());
                course.setCourseStatus(true);
                course.setAvatar(updateCourseRequest.getCourseAvatar());
                course.setNumbersOfLesson(0);
                courseRepository.save(course);
            }
            return acceptOrDenyRequestDTO;
        } else {
            return null;
        }
    }

    @Override
    public CourseRequestDetailDTO viewCourseRequestDetailForMentor(Long courseRequestId) {
        Optional<CourseRequest> optionalCourseRequestDetailDTO = courseRequestRepository.findByUserUserNameAndCourseRequestId(SecurityContextHolder.getContext().getAuthentication().getName(), courseRequestId);
        if (optionalCourseRequestDetailDTO.isPresent()) {
            CourseRequest courseRequest = optionalCourseRequestDetailDTO.get();
            CourseRequestDetailDTO courseRequestDetailDTO = Mapper.mapEntityToDto(courseRequest, CourseRequestDetailDTO.class);
            courseRequestDetailDTO.setMentorUserName(SecurityContextHolder.getContext().getAuthentication().getName());
            return courseRequestDetailDTO;
        }
        return null;
    }

    @Override
    public List<CourseRequestDTO> mentorCourseRequest(String username) {
        List<CourseRequest> mentorCourseRequest = courseRequestRepository.findByMentor(username);
        if (mentorCourseRequest.isEmpty()) {
            return null;
        }
        List<CourseRequestDTO> courseRequestDTOS = new ArrayList<>();
        for (CourseRequest courseRequest :
                mentorCourseRequest) {
            CourseRequestDTO courseRequestDTO = Mapper.mapEntityToDto(courseRequest, CourseRequestDTO.class);
            courseRequestDTOS.add(courseRequestDTO);
        }
        return courseRequestDTOS;
    }

    private String uploadFileToFireBase(MultipartFile file, String folderPath) throws IOException {
        return firebaseService.uploadFile(file.getOriginalFilename(), file.getBytes());
    }
}
