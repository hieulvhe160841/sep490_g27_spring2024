package com.g27.oms_be.service.mentorRequest;

import com.g27.oms_be.common.MentorRequestStatus;
import com.g27.oms_be.dto.mentorRequest.*;
import com.g27.oms_be.dto.userCertificate.UserCertificateDetailDTO;
import com.g27.oms_be.dto.userSkill.UserSkillDetailDTO;
import com.g27.oms_be.entities.MentorRequest;
import com.g27.oms_be.entities.Role;
import com.g27.oms_be.entities.User;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.MentorRequestRepository;
import com.g27.oms_be.repository.RoleRepository;
import com.g27.oms_be.repository.UserRepository;
import com.g27.oms_be.service.userCertificate.UserCertificateService;
import com.g27.oms_be.service.userSkill.UserSkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MentorRequestServiceImpl implements MentorRequestService {
    private MentorRequestRepository mentorRequestRepository;
    private RoleRepository roleRepository;
    private UserRepository userRepository;
    private UserCertificateService userCertificateService;
    private UserSkillService userSkillService;

    @Autowired
    public MentorRequestServiceImpl(MentorRequestRepository mentorRequestRepository, RoleRepository roleRepository, UserRepository userRepository, UserCertificateService userCertificateService, UserSkillService userSkillService) {
        this.mentorRequestRepository = mentorRequestRepository;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.userCertificateService = userCertificateService;
        this.userSkillService = userSkillService;
    }

    @Override
    public List<MentorRequestDTO> listMentorRequest() {
        List<MentorRequest> mentorRequestList = mentorRequestRepository.findAll();
        if (mentorRequestList != null) {
            List<MentorRequestDTO> mentorRequestDTOList = mentorRequestList.stream().map(mentorRequest -> {
                MentorRequestDTO mentorRequestDTO = Mapper.mapEntityToDto(mentorRequest, MentorRequestDTO.class);
                UserCreatedMentorRequestDTO userCreatedMentorRequestDTO = Mapper.mapEntityToDto(mentorRequest.getUser(), UserCreatedMentorRequestDTO.class);
                mentorRequestDTO.setUserCreatedMentorRequestDTO(userCreatedMentorRequestDTO);
                return mentorRequestDTO;
            }).collect(Collectors.toList());
            return mentorRequestDTOList;
        }
        return null;
    }

    @Override
    public MentorRequestDTO mentorRequestDetail(Long requestId) {
        Optional<MentorRequest> mentorRequestOptional = mentorRequestRepository.findById(requestId);
        if (mentorRequestOptional.isPresent()) {
            MentorRequest mentorRequest = mentorRequestOptional.get();
            MentorRequestDTO mentorRequestDTO = new MentorRequestDTO();
            UserCreatedMentorRequestDTO userCreatedMentorRequestDTO = Mapper.mapEntityToDto(mentorRequest.getUser(), UserCreatedMentorRequestDTO.class);
            mentorRequestDTO.setUserCreatedMentorRequestDTO(userCreatedMentorRequestDTO);
            mentorRequestDTO.setMentorRequestId(mentorRequest.getMentorRequestId());
            mentorRequestDTO.setMentorRequestStatus(mentorRequest.getMentorRequestStatus());
            mentorRequestDTO.setMentorRequestFeedback(mentorRequest.getMentorRequestFeedback());
            return mentorRequestDTO;
        }
        return null;
    }

    @Override
    public boolean checkExistedMentorRequestByUserIdAndRequestId(Long userId, Long requestId) {
        Optional<MentorRequest> mentorRequest = mentorRequestRepository.findByMentorRequestIdAndUserUserId(requestId, userId);
        return mentorRequest.isPresent();
    }

    @Override
    public AcceptOrDenyRequestDTO acceptOrDenyMentorRequest(AcceptOrDenyRequestDTO acceptOrDenyRequestDTO, Long requestId) {
        Optional<MentorRequest> mentorRequest = mentorRequestRepository.findById(requestId);
        if (mentorRequest.isPresent()) {
            MentorRequest updateMentorRequest = mentorRequest.get();
            if (acceptOrDenyRequestDTO.getFeedback() != null && !acceptOrDenyRequestDTO.getFeedback().trim().isEmpty()) {
                updateMentorRequest.setMentorRequestFeedback(acceptOrDenyRequestDTO.getFeedback());
            }
            if (acceptOrDenyRequestDTO.getStatus() != null && !acceptOrDenyRequestDTO.getStatus().trim().isEmpty()) {
                updateMentorRequest.setMentorRequestStatus(acceptOrDenyRequestDTO.getStatus().toUpperCase());
            }
            mentorRequestRepository.save(updateMentorRequest);
            if (acceptOrDenyRequestDTO.getStatus().equalsIgnoreCase("accepted")) {
//                User user = userRepository.findByUserId(mentorRequest.get().getUser().getUserId());
//                Role role = roleRepository.findByName("ROLE_MENTOR");
//                user.setRole(role);
//                userRepository.save(user);
                updateMentorRequest.setMentorRequestStatus(MentorRequestStatus.ACCEPTED.name());
                mentorRequestRepository.save(updateMentorRequest);

            }
            return acceptOrDenyRequestDTO;
        }
        return null;
    }

    @Override
    public MentorRequestDetailForMenteeDTO viewMentorRequestDetailForMentee(Long requestId) {
        Optional<MentorRequest> optionalMentorRequest = mentorRequestRepository.findByMentorRequestIdAndUserUserName(requestId, SecurityContextHolder.getContext().getAuthentication().getName());
        if (optionalMentorRequest.isPresent()) {
            MentorRequest mentorRequest = optionalMentorRequest.get();
            MentorRequestDetailForMenteeDTO mentorRequestDetailForMenteeDTO = Mapper.mapEntityToDto(mentorRequest, MentorRequestDetailForMenteeDTO.class);
            mentorRequestDetailForMenteeDTO.setUserCreatedMentorRequestDTO(SecurityContextHolder.getContext().getAuthentication().getName());

            List<UserCertificateDetailDTO> userCertificateDetailDTOList = userCertificateService.userCertificateListByUserId(mentorRequest.getUser().getUserId());
            List<UserSkillDetailDTO> userSkillDetailDTOList = userSkillService.userSkillListByUserId(mentorRequest.getUser().getUserId());

            mentorRequestDetailForMenteeDTO.setUserCertificateDetailDTOList(userCertificateDetailDTOList);
            mentorRequestDetailForMenteeDTO.setUserSkillDetailDTOList(userSkillDetailDTOList);

            mentorRequestDetailForMenteeDTO.setEducationLevel(userRepository.findByUserId(mentorRequest.getUser().getUserId()).getEducationLevel());
            mentorRequestDetailForMenteeDTO.setExperience(userRepository.findByUserId(mentorRequest.getUser().getUserId()).getExperience());
            return mentorRequestDetailForMenteeDTO;
        }
        return null;
    }

    @Override
    public List<MenteeViewOwnRegisAsMentorDTO> getMentorRequestOfMentee(String username) {
        List<MentorRequest> mentorRequests = mentorRequestRepository.findRegisAsMentorRequestByMentee(username);
        if (mentorRequests.size() != 0) {
            List<MenteeViewOwnRegisAsMentorDTO> menteeViewOwnRegisAsMentorDTOS = new ArrayList<>();
            for (MentorRequest mentorRequest : mentorRequests
            ) {
                MenteeViewOwnRegisAsMentorDTO menteeViewOwnRegisAsMentorDTO = new MenteeViewOwnRegisAsMentorDTO();
                menteeViewOwnRegisAsMentorDTO.setMentorRequestId(mentorRequest.getMentorRequestId());
                menteeViewOwnRegisAsMentorDTO.setMentorRequestFeedback(mentorRequest.getMentorRequestFeedback());
                menteeViewOwnRegisAsMentorDTO.setMentorRequestStatus(mentorRequest.getMentorRequestStatus());
                menteeViewOwnRegisAsMentorDTOS.add(menteeViewOwnRegisAsMentorDTO);
            }
            return menteeViewOwnRegisAsMentorDTOS;
        }
        return null;
    }

    @Override
    public boolean checkExistWaitingMentorRequest(String username) {
        Optional<MentorRequest> mentorRequests = mentorRequestRepository.findWaitingMentorRequestByMentee(username);
        if (mentorRequests.isPresent()) {
            return true;
        }
        return false;
    }

    @Override
    public String updateRole(String username) {
        Role role_mentor = roleRepository.findByName("ROLE_MENTOR");
        User mentee = userRepository.findUserByUserName(username);
        mentee.setRole(role_mentor);
        userRepository.save(mentee);
        return "User role is changed!";
    }
}
