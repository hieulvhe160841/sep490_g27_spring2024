package com.g27.oms_be.service.mentorRequest;

import com.g27.oms_be.dto.mentorRequest.AcceptOrDenyRequestDTO;
import com.g27.oms_be.dto.mentorRequest.MenteeViewOwnRegisAsMentorDTO;
import com.g27.oms_be.dto.mentorRequest.MentorRequestDTO;
import com.g27.oms_be.dto.mentorRequest.MentorRequestDetailForMenteeDTO;

import java.util.List;

public interface MentorRequestService {
    List<MentorRequestDTO> listMentorRequest();

    MentorRequestDTO mentorRequestDetail(Long requestId);

    boolean checkExistedMentorRequestByUserIdAndRequestId(Long userId, Long requestId);

    AcceptOrDenyRequestDTO acceptOrDenyMentorRequest(AcceptOrDenyRequestDTO acceptOrDenyRequestDTO, Long requestId);

    MentorRequestDetailForMenteeDTO viewMentorRequestDetailForMentee(Long requestId);

    List<MenteeViewOwnRegisAsMentorDTO> getMentorRequestOfMentee(String username);

    boolean checkExistWaitingMentorRequest(String username);

    String updateRole(String username);
}

