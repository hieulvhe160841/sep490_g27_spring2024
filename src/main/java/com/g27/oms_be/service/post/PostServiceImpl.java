package com.g27.oms_be.service.post;

import com.g27.oms_be.dto.post.*;
import com.g27.oms_be.entities.Post;
import com.g27.oms_be.entities.User;
import com.g27.oms_be.mapper.Mapper;
import com.g27.oms_be.repository.PostRepository;
import com.g27.oms_be.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {
    private PostRepository postRepository;
    private UserRepository userRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Long totalNumberOfPost() {
        return postRepository.count();
    }

    @Override
    public PostDTO addPost(PostDTO postDTO) {
        Post post = Mapper.mapDtoToEntity(postDTO, Post.class);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        User user = userRepository.findUserByUserName(userName);
        post.setUser(user);
        post.setPostStatus(true);
        post.setPostCreatedTime(LocalDateTime.now());
        post.setPostModifiedTime(LocalDateTime.now());
        post.setPostTitle(postDTO.getPostTitle());
        postRepository.saveAndFlush(post);
        postDTO.setPostId(post.getPostId());
        postDTO.setPostStatus(post.isPostStatus());
        postDTO.setPostCreatedUser(userName);
        return postDTO;
    }

    @Override
    public PostDTO getPostByID(Long id) {
        Optional<Post> optionalPost = postRepository.findById(id);
        if (optionalPost.isPresent()) {
            Post post = optionalPost.get();
            PostDTO postDTO = Mapper.mapEntityToDto(post, PostDTO.class);
            postDTO.setPostCreatedUser(post.getUser().getUserName());
            return postDTO;
        }
        return null;
    }

    @Override
    public List<PostDTO> getAllPostByMenteeUserName() {
        List<Post> postList = postRepository.findByUserUserName(SecurityContextHolder.getContext().getAuthentication().getName());
        List<PostDTO> postDTOList = postList.stream().map(post -> {
            PostDTO postDTO = Mapper.mapEntityToDto(post, PostDTO.class);
            postDTO.setPostCreatedUser(SecurityContextHolder.getContext().getAuthentication().getName());
            return postDTO;
        }).collect(Collectors.toList());
        return postDTOList;
    }

    @Override
    public boolean deletePostByPostId(Long postId) {
        postRepository.deleteById(postId);
        boolean postDeleted = postRepository.existsById(postId);
        return postDeleted;
    }

    @Override
    public boolean checkPostExisted(Long id) {
        return postRepository.existsById(id);
    }

    @Override
    public boolean checkUserCreatedPostOrNot(Long postId) {
        Optional<Post> post = postRepository.findByPostIdAndUserUserName(postId, SecurityContextHolder.getContext().getAuthentication().getName());
        return post.isPresent();
    }

    @Override
    public UpdatePostDTO updatePostForMenteeByPostId(Long postId, UpdatePostDTO updatePostDTO) {
        Optional<Post> postExisted = postRepository.findById(postId);
        if (postExisted.isPresent()) {
            Post post = postExisted.get();
            if (updatePostDTO.getPostContent() != null && !updatePostDTO.getPostContent().trim().equals("")) {
                post.setPostContent(updatePostDTO.getPostContent());
            }
            if (updatePostDTO.getPostPrice() != null && !updatePostDTO.getPostPrice().trim().equals("")) {
                post.setPostPrice(Double.parseDouble(updatePostDTO.getPostPrice()));
            }
            if (updatePostDTO.getPostTitle() != null && !updatePostDTO.getPostTitle().trim().equals("")) {
                post.setPostTitle(updatePostDTO.getPostTitle());
            }
            post.setPostModifiedTime(LocalDateTime.now());
            postRepository.saveAndFlush(post);
        }
        return updatePostDTO;
    }

    @Override
    public PostDTO updatePostForAdminByPostId(Long postId, Boolean status) {
        Optional<Post> postExisted = postRepository.findById(postId);
        if (postExisted.isPresent()) {
            Post post = postExisted.get();
            if (post.isPostStatus() == true) {
                post.setPostStatus(false);
                post.setPostModifiedTime(LocalDateTime.now());
            } else {
                post.setPostStatus(true);
                post.setPostModifiedTime(LocalDateTime.now());
            }
            postRepository.save(post);
            return getPostByID(postId);
        } else {
            return null;
        }
    }

    @Override
    public ViewPostDetailForMentorDTO viewPostDetailForMentorByPostId(Long postId) {
        Optional<Post> optionalPost = postRepository.findById(postId);
        if (optionalPost.isPresent()) {
            ViewPostDetailForMentorDTO viewPostDetailForMentorDTO = Mapper.mapEntityToDto(optionalPost.get(), ViewPostDetailForMentorDTO.class);
            viewPostDetailForMentorDTO.setPostCreatedUser(optionalPost.get().getUser().getUserName());
            viewPostDetailForMentorDTO.setMenteeName(optionalPost.get().getUser().getName());
            return viewPostDetailForMentorDTO;
        }
        return null;
    }

    @Override
    public List<ViewAllAvailablePostForMentor> viewAllAvailablePostForMentor() {
        List<Post> postList = postRepository.findByPostStatus(true);
        if (postList != null) {
            List<ViewAllAvailablePostForMentor> listOfAvailablePost = postList.stream().map(post -> {
                ViewAllAvailablePostForMentor viewAllAvailablePostForMentor = Mapper.mapEntityToDto(post, ViewAllAvailablePostForMentor.class);
                viewAllAvailablePostForMentor.setPostCreatedUser(post.getUser().getUserName());
                viewAllAvailablePostForMentor.setMenteeName(post.getUser().getName());
                viewAllAvailablePostForMentor.setPrice(post.getPostPrice());
                return viewAllAvailablePostForMentor;
            }).collect(Collectors.toList());
            return listOfAvailablePost;
        }
        return null;
    }

    @Override
    public ViewPostDetailForMenteeDTO viewPostDetailForMenteeDTO(Long postId) {
        Post post = postRepository.findByUserUserNameAndPostId(SecurityContextHolder.getContext().getAuthentication().getName(), postId);
        if (post != null) {
            ViewPostDetailForMenteeDTO viewPostDetailForMenteeDTO = Mapper.mapEntityToDto(post, ViewPostDetailForMenteeDTO.class);
            viewPostDetailForMenteeDTO.setPostCreatedUser(post.getUser().getUserName());
            return viewPostDetailForMenteeDTO;
        }
        return null;
    }

    @Override
    public List<ViewAllPostForAdminDTO> postListForAdmin() {
        List<Post> postList = postRepository.findAll();
        if (postList != null) {
            List<ViewAllPostForAdminDTO> viewAllPostForAdminDTOList = postList.stream().map(post -> {
                ViewAllPostForAdminDTO viewAllPostForAdminDTO = Mapper.mapEntityToDto(post, ViewAllPostForAdminDTO.class);
                viewAllPostForAdminDTO.setPostCreatedUser(post.getUser().getUserName());
                return viewAllPostForAdminDTO;
            }).collect(Collectors.toList());
            return viewAllPostForAdminDTOList;
        }
        return null;
    }
}
