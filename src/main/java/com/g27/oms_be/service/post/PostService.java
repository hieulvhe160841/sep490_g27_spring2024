package com.g27.oms_be.service.post;

import com.g27.oms_be.dto.post.*;

import java.util.List;

public interface PostService {
    Long totalNumberOfPost();

    PostDTO addPost(PostDTO postDTO);

    PostDTO getPostByID(Long id);

    List<PostDTO> getAllPostByMenteeUserName();

    boolean deletePostByPostId(Long postId);

    boolean checkPostExisted(Long id);

    boolean checkUserCreatedPostOrNot(Long postId);

    UpdatePostDTO updatePostForMenteeByPostId(Long postId, UpdatePostDTO updatePostDTO);

    PostDTO updatePostForAdminByPostId(Long postId, Boolean status);

    ViewPostDetailForMentorDTO viewPostDetailForMentorByPostId(Long postId);

    List<ViewAllAvailablePostForMentor> viewAllAvailablePostForMentor();

    ViewPostDetailForMenteeDTO viewPostDetailForMenteeDTO(Long postId);

    List<ViewAllPostForAdminDTO> postListForAdmin();
}
