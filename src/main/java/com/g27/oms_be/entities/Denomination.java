package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "Denomination")
@Data
public class Denomination {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "denominationId")
    private Long denominationId;

    @Column(name = "value")
    private Double value;

    @Column(name = "amount")
    private Double amount;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE, mappedBy = "denomination")
    private List<DenominationTransaction> denominationTransactions;

}
