package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "Skill")
@Data
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "skillId")
    private Long skillId;

    @Column(name = "skillName", nullable = false)
    private String skillName;

    @Column(name = "skillDescription", nullable = true)
    private String skillDescription;

    @Column(name = "skillStatus", nullable = false)
    private Boolean skillStatus;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE, mappedBy = "skill")
    private List<UserSkill> userSkills;
}
