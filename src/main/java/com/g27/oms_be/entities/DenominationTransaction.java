package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "denomination_transaction")
@Data
public class DenominationTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "denominationTransactionId")
    private Long denominationTransactionId;

    @Column(name = "orderId")
    private String orderId;

    @OneToOne
    @JoinColumn(name = "transactionId")
    private Transaction transaction;

    @ManyToOne
    @JoinColumn(name = "denominationId")
    private Denomination denomination;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

}
