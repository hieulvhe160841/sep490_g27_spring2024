package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "Support")
@Data
public class Support {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "supportId")
    private Long supportId;

    @ManyToOne
    @JoinColumn(name = "mentorId", nullable = true)
    private User mentorId;

    @ManyToOne
    @JoinColumn(name = "menteeId", nullable = true)
    private User menteeId;

    @ManyToOne
    @JoinColumn(name = "postId", nullable = true)
    private Post postId;

    @Column(name = "supportStatus")
    private String supportStatus;

    @Column(name = "supportCreatedDate")
    private LocalDate supportCreatedDate;

    @Column(name = "price")
    private Double price;

    @Column(name = "supportFeedback")
    private String supportFeedback;

    @Column(name = "rating")
    private Double rating;

    @Column(name = "timeStart")
    private LocalDateTime timeStart;

    @Column(name = "timeEnd")
    private LocalDateTime timeEnd;

    @Column(name = "mentorConfirmStart", nullable = true)
    private boolean mentorConfirmStart;

    @Column(name = "menteeConfirmStart", nullable = true)
    private boolean menteeConfirmStart;

    @Column(name = "menteeConfirmEnd", nullable = true)
    private boolean menteeConfirmEnd;

    @Column
    private Double duration;

    @OneToOne(mappedBy = "support")
    private SupportTransaction supportTransaction;
}
