package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "Course_Request")
@Data
public class CourseRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "courseRequestId")
    private Long courseRequestId;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    @Column(name = "courseName", nullable = false)
    private String courseName;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "courseRequestStatus", nullable = false)
    private String courseRequestStatus;

    @Column(name = "feedback")
    private String courseRequestFeedback;

    @Column(name = "demoSource", nullable = false)
    private String demoSource;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "courseAvatar")
    private String courseAvatar;
}
