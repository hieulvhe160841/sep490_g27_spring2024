package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "Course")
@Data
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "courseId")
    private Long courseId;

    @Column(name = "courseName", nullable = false)
    private String courseName;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "courseStatus")
    private boolean courseStatus;

    @Column(name = "createdDate", nullable = false)
    private LocalDate courseCreatedDate;

    @Column(name = "modifiedDate", nullable = false)
    private LocalDate courseModifiedDate;

    @Column(name = "rating", nullable = true)
    private Double courseRating;

    @Column(name = "numbersOfLesson", nullable = true)
    private Integer numbersOfLesson;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE, mappedBy = "course")
    private List<Video> videos;

    @Column(name ="avatar" )
    private String avatar;
}
