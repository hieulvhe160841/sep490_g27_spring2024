package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "Post")
@Data
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "postId")
    private Long postId;

    @Column(name = "postTitle")
    private String postTitle;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @Column(name = "postContent", nullable = false)
    private String postContent;

    @Column(name = "postStatus", nullable = false)
    private boolean postStatus;

    @Column(name = "postPrice")
    private Double postPrice;

    @Column(name = "postCreatedTime")
    private LocalDateTime postCreatedTime;

    @Column(name = "postModifiedTime")
    private LocalDateTime postModifiedTime;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "postId")
    private List<Support> supportList;

}
