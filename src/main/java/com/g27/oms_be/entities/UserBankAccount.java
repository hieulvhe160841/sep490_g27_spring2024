package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "User_Bank_Account")
@Data
public class UserBankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userBankAccountId")
    private Long userBankAccountId;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @Column(name = "bankName", nullable = false)
    private String bankName;

    @Column(name = "accountNumber", nullable = false)
    private String accountNumber;

    @Column(name = "QRCodeUrl", nullable = false)
    private String QRCodeUrl;

}
