package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "User_Skill")
@Data
public class UserSkill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userSkillId")
    private Long mentorSkillId;

    @ManyToOne
    @JoinColumn(name = "skillId", nullable = false)
    private Skill skill;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;
}
