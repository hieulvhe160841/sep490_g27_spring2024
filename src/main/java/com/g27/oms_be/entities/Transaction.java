package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Table(name = "Transaction")
@Data
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transactionId")
    private Long transactionId;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "transactionType")
    private String transactionType;

    @Column(name = "timeStamp")
    private LocalDate timeStamp;

    @OneToOne(mappedBy = "transaction")
    private DenominationTransaction denominationTransaction;

    @OneToOne(mappedBy = "transaction")
    private SupportTransaction supportTransaction;

    @OneToOne(mappedBy = "transaction")
    private CourseRegistrationTransaction courseRegistrationTransaction;

    @OneToOne(mappedBy = "transaction")
    private WithdrawalRequest withdrawalRequest;

    @Column(name = "status")
    private String transactionStatus;
}
