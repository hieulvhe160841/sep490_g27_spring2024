package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "Withdrawal_Request")
@Data
public class WithdrawalRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "withdrawalRequestId")
    private Long withdrawalRequestId;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @OneToOne
    @JoinColumn(name = "transactionId")
    private Transaction transaction;

    @Column(name = "withdrawalRequestStatus")
    private String withdrawalRequestStatus;

    @Column(name = "withdrawalFeedback")
    private String withdrawalFeedback;
}
