package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Table(name = "User_Wallet")
@Data
public class UserWallet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userWalletId")
    private Long userWalletId;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @Column(name = "balance", nullable = false)
    private Double balance;

    @Column(name = "lastUpdateDate", nullable = false)
    private LocalDate lastUpdateDate;
}
