package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "Course_Registration_Transaction")
@Data
public class CourseRegistrationTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_registration_transaction_id")
    private Long courseRegistrationTransactionId;

    @OneToOne
    @JoinColumn(name = "transaction_id")
    private Transaction transaction;

    @OneToOne
    @JoinColumn(name = "course_registration_id")
    private CourseRegistration courseRegistration;
}
