package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "Support_Transaction")
@Data
public class SupportTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "supportTransactionId")
    private Long supportTransactionId;

    @OneToOne
    @JoinColumn(name = "transactionId")
    private Transaction transaction;

    @OneToOne
    @JoinColumn(name = "supportId")
    private Support support;

}
