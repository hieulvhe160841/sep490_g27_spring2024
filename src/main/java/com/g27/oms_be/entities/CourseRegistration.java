package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Table(name = "Course_Registration")
@Data
public class CourseRegistration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "courseRegistrationId")
    private Long courseRegistrationId;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @ManyToOne
    @JoinColumn(name = "courseId")
    private Course course;

    @Column(name = "dateRegistered")
    private LocalDate dateRegistered;

    @Column(name = "feedback")
    private String courseRegistrationFeedback;

    @OneToOne(mappedBy = "courseRegistration")
    private CourseRegistrationTransaction courseRegistrationTransaction;
}
