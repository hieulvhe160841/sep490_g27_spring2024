package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "Mentor_Request")
@Data
public class MentorRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mentorRequestId")
    private Long mentorRequestId;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @Column(name = "mentorRequestStatus")
    private String mentorRequestStatus;

    @Column(name = "mentorRequestFeedback")
    private String mentorRequestFeedback;

}
