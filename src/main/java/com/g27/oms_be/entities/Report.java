package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Table(name = "Report")
@Data
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reportId")
    private Long reportId;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @Column(name = "content")
    private String content;

    @Column(name = "reportCreatedDate")
    private LocalDate reportCreatedDate;

    @Column(name = "reportLastModified")
    private LocalDate reportLastModified;

}
