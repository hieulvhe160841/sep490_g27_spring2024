package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "Call_History")
@Data
public class CallHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "call_history_id")
    private Long callHistoryId;

    @ManyToOne
    @JoinColumn(name = "call_session_id")
    private CallSession callSession;

    @Column(name = "rating")
    private Double rating;
}
