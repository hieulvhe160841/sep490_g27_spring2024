package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "Certificate")
@Data
public class Certificate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "certificateId")
    private Long certificateId;

    @Column(name = "certificateName", nullable = false)
    private String certificateName;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE, mappedBy = "certificate")
    private List<UserCertificate> userCertificates;
}
