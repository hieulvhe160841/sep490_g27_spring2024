package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "Video")
@Data
public class Video {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "videoId")
    private Long videoId;

    @Column(name = "videoTitle")
    private String videoTitle;

    @Column(name = "videoUrl")
    private String videoUrl;

    @Column(name = "orderInCourse")
    private Integer orderInCourse;

    @ManyToOne
    @JoinColumn(name = "courseId")
    private Course course;
}
