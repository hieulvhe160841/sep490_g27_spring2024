package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "Call_Session")
@Data
public class CallSession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "callSessionId")
    private Long callSessionId;

    @ManyToOne
    @JoinColumn(name = "user1Id")
    private User user1;

    @ManyToOne
    @JoinColumn(name = "user2Id")
    private User user2;

    @Column(name = "startTime")
    private LocalDate startTime;

    @Column(name = "endTime")
    private LocalDate endTime;

    @Column(name = "status")
    private boolean status;

    @OneToMany(mappedBy = "callSession", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private List<CallHistory> callHistoryList;
}
