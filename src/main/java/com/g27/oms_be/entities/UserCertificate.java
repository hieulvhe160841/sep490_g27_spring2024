package com.g27.oms_be.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "User_Certificate")
@Data
public class UserCertificate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userCertificateId")
    private Long mentorCertificateId;

    @ManyToOne
    @JoinColumn(name = "certificateId", nullable = false)
    private Certificate certificate;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;

    @Column(name = "certificateUrl")
    private String certificateUrl;
}
