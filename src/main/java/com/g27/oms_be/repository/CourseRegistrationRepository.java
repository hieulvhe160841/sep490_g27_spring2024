package com.g27.oms_be.repository;

import com.g27.oms_be.entities.CourseRegistration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRegistrationRepository extends JpaRepository<CourseRegistration, Long> {
    @Query("select cr from CourseRegistration cr where cr.user.userName = :userName")
    List<CourseRegistration> findByUserName(@Param("userName") String username);

    CourseRegistration findByCourseCourseIdAndUserUserName(Long courseId, String username);
}
