package com.g27.oms_be.repository;

import com.g27.oms_be.entities.Support;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface SupportRepository extends JpaRepository<Support, Long>, JpaSpecificationExecutor<Support> {
    @Query("select s from Support s where s.menteeId.userId= :menteeId and s.supportStatus  like 'DONE' ")
    List<Support> findByMenteeId(@Param("menteeId") Long menteeId);

    @Query("select s from Support s where s.mentorId.userId= :mentorId and s.supportStatus  like 'DONE' ")
    List<Support> findByMentorId(@Param("mentorId") Long mentorId);

    List<Support> findAll();

    List<Support> findAllByPostIdPostId(Long postId);

    List<Support> findAllByMentorIdUserName(String userName);

    List<Support> findAllByMenteeIdUserName(String userName);

    Optional<Support> findByMentorIdUserNameAndSupportId(String userName, Long supportId);

    @Query("SELECT AVG(COALESCE(s.rating, 0)) FROM Support s WHERE s.mentorId.userId = :mentorId and s.rating is not null")
    Double findAverageRatingMentor(@Param("mentorId") Long mentorId);

    @Query("select s from Support s where (s.supportStatus  like 'WAITING' or s.supportStatus like 'ACCEPTED' or s.supportStatus like 'IN_PROGRESS') and s.mentorId.userName like :mentorName and s.menteeId.userName like :menteeName ")
    List<Support> findSupportBySupportStatusInProgress(@Param("mentorName") String mentorName, @Param("menteeName") String menteeName);

    Optional<Support> findBySupportIdAndMenteeIdUserId(Long supportId, Long menteeId);

    Optional<Support> findBySupportIdAndMenteeIdUserIdAndSupportStatus(Long supportId, Long menteeId, String supportStatus);

    @Query("select distinct s.mentorId.userName from Support s where s.menteeId.userName like :userName and s.supportStatus like 'DONE'")
    List<String> findDistinctMentor(@Param("userName") String menteename);

    @Query("select distinct s.menteeId.userName from Support s where s.mentorId.userName like :userName and s.supportStatus like 'DONE'")
    List<String> findDistinctMentee(@Param("userName") String mentorname);

    @Query("select s from Support s where s.menteeId.userName like :menteename and s.mentorId.userName like :mentorname and s.supportStatus like 'DONE' order by s.supportCreatedDate limit 1")
    Support findTopByCreatedDate(@Param("mentorname") String mentorname, @Param("menteename") String menteename);

}
