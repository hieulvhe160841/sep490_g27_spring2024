package com.g27.oms_be.repository;

import com.g27.oms_be.dto.course.NumberOfCoursePerMonthAndYearDTO;
import com.g27.oms_be.entities.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long>, JpaSpecificationExecutor<Course> {
    List<Course> findTop5ByOrderByCourseCreatedDateDesc();

    @Query("SELECT NEW com.g27.oms_be.dto.course.NumberOfCoursePerMonthAndYearDTO(" +
            "EXTRACT(MONTH FROM c.courseCreatedDate), " +
            "EXTRACT(YEAR FROM c.courseCreatedDate), " +
            "COUNT(c)) " +
            "FROM Course c WHERE EXTRACT(YEAR FROM c.courseCreatedDate) = :year " +
            "GROUP BY EXTRACT(MONTH FROM c.courseCreatedDate), EXTRACT(YEAR FROM c.courseCreatedDate)")
    List<NumberOfCoursePerMonthAndYearDTO> countCourseCreatedPerMonth(@Param("year") int year);

    @Query("select  c from Course c where c.courseStatus= true order by c.courseCreatedDate desc LIMIT 5")
    List<Course> findTopFiveCourses();

    @Query("select  c from Course c where c.courseStatus= true order by function('RAND') LIMIT 5")
    List<Course> findRandomCourses();

    @Query("SELECT c FROM Course c WHERE  c.courseStatus= true " +
            "and (LOWER(c.description) LIKE LOWER(concat('%', :goal, '%')) " +
            "OR LOWER(c.courseName) LIKE LOWER(concat('%', :goal, '%')) " +
            "OR LOWER(c.description) LIKE LOWER(concat('%', :interest, '%')) " +
            "OR LOWER(c.courseName) LIKE LOWER(concat('%', :interest, '%')))")
    List<Course> findByCourseNameAndDescription(@Param("goal") String goal, @Param("interest") String interest);

    Course findByCourseId(Long id);

    @Query("Select c from Course c where c.user.userName like :username and c.courseStatus = true")
    List<Course> findByUserName(@Param("username") String username);

    @Query("select c from Course c where c.courseStatus = true order by c.courseRating desc, c.courseCreatedDate desc, c.price asc ")
    List<Course> findTopCourses();

    @Query("Select c from Course c where c.user.userName like :userName and c.courseId = :courseId")
    Optional<Course> existsCourseByUser(@Param("userName") String userName, @Param("courseId") Long courseId);

}
