package com.g27.oms_be.repository;

import com.g27.oms_be.entities.UserCertificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserCertificateRepository extends JpaRepository<UserCertificate, Long> {
    List<UserCertificate> findByUserUserId(Long userId);
    @Query("select uc from UserCertificate uc where uc.certificate.certificateName = :certificateName and uc.user.userName = :userName")
    Optional<UserCertificate>findByUserNameAndCertificate(@Param("userName")String userName,@Param("certificateName") String certificateName);
}
