package com.g27.oms_be.repository;

import com.g27.oms_be.dto.certificate.CertificateDTO;
import com.g27.oms_be.entities.Certificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface CertificateRepository extends JpaRepository<Certificate,Long> {
    Certificate findByCertificateId(Long id);
    Certificate findByCertificateName(String name);
    @Query("select new com.g27.oms_be.dto.certificate.CertificateDTO(c.certificateId,c.certificateName,uc.certificateUrl) from  UserCertificate uc join uc.certificate c" +
            " where uc.user.userId= :userId")
    Set<CertificateDTO> findCertificateByUserId(@Param("userId")Long id);
    List<Certificate> findAll();
}
