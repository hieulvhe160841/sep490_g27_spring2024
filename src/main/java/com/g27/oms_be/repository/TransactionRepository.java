package com.g27.oms_be.repository;

import com.g27.oms_be.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    @Query("select coalesce(sum(t.amount), 0) from Transaction t where t.transactionType in ('SUPPORT','COURSE_PURCHASE')")
    Double totalAmountOfCourseRegistrationAndSupportTransaction();

}
