package com.g27.oms_be.repository;

import com.g27.oms_be.entities.CourseRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRequestRepository extends JpaRepository<CourseRequest, Long> {
    Optional<CourseRequest> findByUserUserNameAndCourseRequestId(String userName, Long courseRequestId);

    List<CourseRequest> findByCourseRequestStatusEqualsIgnoreCase(String courseRequestStatus);
    @Query("SELECT cr FROM CourseRequest cr where cr.user.userName = :mentorName")
    List<CourseRequest> findByMentor(@Param("mentorName") String mentorName);
}
