package com.g27.oms_be.repository;

import com.g27.oms_be.entities.UserSkill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserSkillRepository extends JpaRepository<UserSkill, Long> {
    List<UserSkill> findByUserUserId(Long userId);
    @Query("SELECT us from UserSkill us where us.user.userName =:userName and us.skill.skillName =:skillName")
    Optional<UserSkill> findByUserNameAndSkill(@Param("userName")String userName, @Param("skillName")String skillName);
}
