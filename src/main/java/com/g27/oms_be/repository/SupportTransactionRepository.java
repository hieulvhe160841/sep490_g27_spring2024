package com.g27.oms_be.repository;

import com.g27.oms_be.entities.SupportTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupportTransactionRepository extends JpaRepository<SupportTransaction, Long> {
}
