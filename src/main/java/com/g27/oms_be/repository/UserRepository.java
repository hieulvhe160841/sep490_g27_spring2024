package com.g27.oms_be.repository;

import com.g27.oms_be.dto.user.admin.NumberOfUserAndCoursePerMonthDTO;
import com.g27.oms_be.dto.user.admin.NumberOfUserPerMonthAndYearDTO;
import com.g27.oms_be.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserByUserName(String userName);

    User findByEmail(String email);

    User findByPhone(String phone);

    boolean existsByEmail(String email);

    boolean existsByPhone(String phone);

    boolean existsByUserName(String userName);

    User findByUserId(Long userId);

    @Query("SELECT u FROM User u WHERE u.role.name = 'ROLE_MENTOR' and u.status = true order by u.rating desc LIMIT 10")
    List<User> findTop10ByRating();

    Long countByRoleName(String roleName);

    @Query("SELECT u from User u WHERE u.role.name = 'ROLE_MENTOR' ")
    List<User> findALlMentor();

    @Query("SELECT NEW com.g27.oms_be.dto.user.admin.NumberOfUserPerMonthAndYearDTO(" +
            "EXTRACT(MONTH FROM u.dateCreated), " +
            "EXTRACT(YEAR FROM u.dateCreated), " +
            "COUNT(u)) " +
            "FROM User u WHERE EXTRACT(YEAR FROM u.dateCreated) = :year " +
            "GROUP BY EXTRACT(MONTH FROM u.dateCreated), EXTRACT(YEAR FROM u.dateCreated)")
    List<NumberOfUserPerMonthAndYearDTO> countUsersByDateCreatedPerMonth(@Param("year") int year);

    @Query("select  u from User u where u.role.name ='ROLE_MENTOR' order by function('RAND') LIMIT 10")
    List<User> findRandomMentor();

    @Query("select u from User u where u.role.name='ROLE_MENTOR'" +
            " and (LOWER(u.experience) LIKE LOWER(concat('%', :goal, '%')) " +
            "   OR LOWER(u.experience) LIKE LOWER(concat('%', :interest, '%')) " +
            "   OR LOWER(u.educationLevel) LIKE LOWER(concat('%', :interest, '%')) " +
            "   OR LOWER(u.educationLevel) LIKE LOWER(concat('%', :goal, '%'))) order by u.userName limit 10")
    List<User> findByExperience(@Param("interest") String interest, @Param("goal") String goal);
    @Query("Select u from User u where u.role.name like 'ROLE_MENTOR' order by u.rating desc , u.costPerHour asc ")
    List<User> findTopByRatingAndCostPerHour();
}
