package com.g27.oms_be.repository;

import com.g27.oms_be.entities.MentorRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MentorRequestRepository extends JpaRepository<MentorRequest, Long> {
    Optional<MentorRequest> findByMentorRequestIdAndUserUserName(Long mentorRequestId, String userName);

    Optional<MentorRequest> findByMentorRequestIdAndUserUserId(Long mentorRequestId, Long userId);

    @Query("select m from MentorRequest  m where m.user.userName like :username order by m.mentorRequestId desc")
    List<MentorRequest> findRegisAsMentorRequestByMentee(@Param("username") String username);

    @Query("select m from MentorRequest  m where m.user.userName like :username and m.mentorRequestStatus not like 'REJECTED'")
    Optional<MentorRequest> findWaitingMentorRequestByMentee(@Param("username") String username);
}
