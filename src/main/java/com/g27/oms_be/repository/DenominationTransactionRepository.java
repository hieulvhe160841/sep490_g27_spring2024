package com.g27.oms_be.repository;

import com.g27.oms_be.entities.DenominationTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DenominationTransactionRepository extends JpaRepository<DenominationTransaction, Long> {
    DenominationTransaction findByOrderId(String orderId);

    List<DenominationTransaction> findAllByUserUserName(String username);
}
