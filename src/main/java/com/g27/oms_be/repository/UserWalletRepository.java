package com.g27.oms_be.repository;

import com.g27.oms_be.entities.UserWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserWalletRepository extends JpaRepository<UserWallet, Long> {
//    UserWallet findByUserUserId(Long userId);

    Optional<UserWallet> findByUserUserName(String username);

    Optional<UserWallet> findByUserUserId(Long userId);
}
