package com.g27.oms_be.repository;

import com.g27.oms_be.entities.CourseRegistrationTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRegistrationTransactionRepository extends JpaRepository<CourseRegistrationTransaction, Long> {
}
