package com.g27.oms_be.repository;

import com.g27.oms_be.entities.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findByUserUserName(String userName);

    List<Post> findByPostStatus(boolean postStatus);

    Optional<Post> findByPostIdAndUserUserName(Long postId, String userName);

    Post findByUserUserNameAndPostId(String userName, Long postId);
}
