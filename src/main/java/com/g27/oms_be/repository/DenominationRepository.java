package com.g27.oms_be.repository;

import com.g27.oms_be.entities.Denomination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DenominationRepository extends JpaRepository<Denomination, Long> {
}
