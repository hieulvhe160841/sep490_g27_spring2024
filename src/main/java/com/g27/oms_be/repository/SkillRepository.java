package com.g27.oms_be.repository;

import com.g27.oms_be.dto.skill.SkillDTO;
import com.g27.oms_be.entities.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {
    Skill findByskillName(String skillName);

    Skill findByskillNameAndSkillIdNot(String skillName, Long skillId);

    Skill findBySkillId(Long skillId);

    @Query("SELECT new com.g27.oms_be.dto.skill.SkillDTO(s.skillId, s.skillName, s.skillDescription, s.skillStatus)" +
            " FROM UserSkill us JOIN us.skill s WHERE us.user.userId = :userId")
    Set<SkillDTO> findSkillsByUserId(@Param("userId") Long userId);

    @Query("select s from Skill s where s.skillStatus =true ")
    List<Skill> findAvailableSkill();
}
