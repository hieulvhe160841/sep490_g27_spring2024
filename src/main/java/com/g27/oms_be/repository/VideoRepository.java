package com.g27.oms_be.repository;

import com.g27.oms_be.entities.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VideoRepository extends JpaRepository<Video, Long> {
    @Query("Select v from Video v where v.course.courseId =:courseId order by v.orderInCourse asc")
    List<Video> findByCourseAndOrderByOrderInCourse(@Param("courseId") Long courseId);

    @Query("Select v from Video v where  v.course.courseId  = :courseId and v.videoId = :videoId")
    Optional<Video> existsByCourse(@Param("courseId") Long courseId,@Param("videoId")Long videoiD);

    @Query("Select count(v)  from Video v where v.course.courseId =:courseId")
    Integer countVideosByCourseId(@Param("courseId") Long courseId);
}
