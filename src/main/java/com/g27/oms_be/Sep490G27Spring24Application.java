package com.g27.oms_be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sep490G27Spring24Application {

    public static void main(String[] args) {
        SpringApplication.run(Sep490G27Spring24Application.class, args);
    }

}
