package com.g27.oms_be.payment;

import com.g27.oms_be.common.TransactionStatus;
import com.g27.oms_be.common.TransactionType;
import com.g27.oms_be.dto.denomination.DenominationDTO;
import com.g27.oms_be.entities.DenominationTransaction;
import com.g27.oms_be.entities.Transaction;
import com.g27.oms_be.entities.User;
import com.g27.oms_be.repository.DenominationRepository;
import com.g27.oms_be.repository.DenominationTransactionRepository;
import com.g27.oms_be.repository.TransactionRepository;
import com.g27.oms_be.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.*;

@Component
@Service
@Slf4j
public class Payment {
    @Value("${paypal.clientId}")
    private String clientId;
    @Value("${paypal.clientSecret}")
    private String clientSecret;
    @Value("${paypal.mode}")
    private String mode;
    @Value("${return_url}")
    private String return_url;
    @Value("${cancel_url}")
    private String cancel_url;
    private RestTemplate restTemplate;
    private UserRepository userRepository;
    private TransactionRepository transactionRepository;
    private DenominationTransactionRepository denominationTransactionRepository;
    private DenominationRepository denominationRepository;

    @Autowired
    public Payment(RestTemplate restTemplate, UserRepository userRepository, TransactionRepository transactionRepository, DenominationTransactionRepository denominationTransactionRepository, DenominationRepository denominationRepository) {
        this.restTemplate = restTemplate;
        this.userRepository = userRepository;
        this.transactionRepository = transactionRepository;
        this.denominationTransactionRepository = denominationTransactionRepository;
        this.denominationRepository = denominationRepository;
    }

    public String generateAccessToken() {
        HttpHeaders headers = new org.springframework.http.HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth(clientId, clientSecret);

        MultiValueMap<String, String> properties = new LinkedMultiValueMap<String, String>();
        properties.add("grant_type", "client_credentials");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(properties, headers);

        ResponseEntity<Map> response = restTemplate.exchange("https://api-m.sandbox.paypal.com/v1/oauth2/token", HttpMethod.POST, request, Map.class);
        return (String) response.getBody().get("access_token");
    }

    public Object createPayment(DenominationDTO denominationDTO) {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("intent", "CAPTURE");

        List<Map<String, Object>> purchaseUnits = new ArrayList<>();

        Map<String, Object> purchaseUnit = new HashMap<>();
        purchaseUnit.put("reference_id", denominationDTO.getDenominationId().toString());

        Map<String, Object> amount = new HashMap<>();
        amount.put("currency_code", "USD");

        Double convertedAmount = denominationDTO.getAmount() / 25000;
        amount.put("value", convertedAmount.toString());
        purchaseUnit.put("amount", amount);
        purchaseUnits.add(purchaseUnit);

        requestBody.put("purchase_units", purchaseUnits);

        Map<String, Object> experienceContext = new HashMap<>();
        experienceContext.put("payment_method_preference", "IMMEDIATE_PAYMENT_REQUIRED");
        experienceContext.put("brand_name", "OMS");
        experienceContext.put("locale", "en-US");
        experienceContext.put("landing_page", "LOGIN");
        experienceContext.put("user_action", "PAY_NOW");
        experienceContext.put("return_url", return_url);
        experienceContext.put("cancel_url", cancel_url);

        Map<String, Object> paypal = new HashMap<>();
        paypal.put("experience_context", experienceContext);

        Map<String, Object> paymentSource = new HashMap<>();
        paymentSource.put("paypal", paypal);

        requestBody.put("payment_source", paymentSource);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String accessToken = this.generateAccessToken();
        httpHeaders.set("Authorization", "Bearer " + accessToken);

        HttpEntity<Map<String, Object>> httpEntity = new HttpEntity<>(requestBody, httpHeaders);

        ResponseEntity<Map> responseEntity = restTemplate.exchange("https://api-m.sandbox.paypal.com/v2/checkout/orders", HttpMethod.POST, httpEntity, Map.class);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            DenominationTransaction denominationTransaction = new DenominationTransaction();
            denominationTransaction.setOrderId((String) responseEntity.getBody().get("id"));

            denominationTransaction.setDenomination(denominationRepository.getReferenceById(denominationDTO.getDenominationId()));
            User user = userRepository.findUserByUserName(SecurityContextHolder.getContext().getAuthentication().getName());
            denominationTransaction.setUser(user);
            denominationTransactionRepository.saveAndFlush(denominationTransaction);

            Transaction transaction = new Transaction();
            transaction.setDenominationTransaction(denominationTransaction);
            transaction.setAmount(denominationDTO.getAmount());
            transaction.setTransactionType(TransactionType.DENOMINATION.name());
            transaction.setTimeStamp(LocalDate.now());
            transaction.setTransactionStatus(TransactionStatus.PENDING.name());

            denominationTransaction.setTransaction(transaction);

            transactionRepository.saveAndFlush(transaction);

            return responseEntity.getBody();
        }
        return null;
    }
}
