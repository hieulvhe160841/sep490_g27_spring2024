package com.g27.oms_be.common;

public enum SupportStatus {
    ACCEPTED, REJECTED, WAITING, IN_PROGRESS, DONE;
}
