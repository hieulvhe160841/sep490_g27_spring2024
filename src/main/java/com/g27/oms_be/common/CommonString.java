package com.g27.oms_be.common;

public class CommonString {
    public static final String NOT_FOUND_MESSAGES = "NOT FOUND";
    public static final String CREATE_SUCCESSFULLY = "SUCCESSFULLY";
    public static final String CREATE_FAIL = "FAILED";
    public static final String EMAIL_SEND = "EMAIL WAS SEND SUCCESSFULLY";
    public static final String USER_NOT_AUTHORIZED = "USER NOT AUTHORIZED";
    public static final String REGEX_CHECK_VALIDATE_PRICE_FORMAT = "^\\d+(\\.\\d{1,2})?$";
    public static final String ACCEPTED_STRING = "accepted";
    public static final String REJECTED_STRING = "rejected";
    public static final String OK_MESSAGES = "OK";
    public static final String INVALID_INPUT = "INVALID INPUT";
}
