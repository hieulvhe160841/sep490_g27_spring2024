package com.g27.oms_be.common;

public enum TransactionStatus {
    DONE, CANCELED,PENDING;
}
