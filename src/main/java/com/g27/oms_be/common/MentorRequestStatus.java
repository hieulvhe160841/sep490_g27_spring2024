package com.g27.oms_be.common;

public enum MentorRequestStatus {
    ACCEPTED,REJECTED, WAITING;
}
