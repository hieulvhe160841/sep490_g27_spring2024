package com.g27.oms_be.config;

import com.g27.oms_be.security.CustomUserDetailsService;
import com.g27.oms_be.security.JwtAuthenticationFilter;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;


@Configuration
@EnableWebSecurity
@EnableMethodSecurity

public class WebSecurityConfig {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }

    @Autowired
    private LogoutHandler logoutHandler;

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    //configuring security
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        http
                .cors()
                .and()
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeRequests()
                .requestMatchers("/auth/**", "/top-rate-mentor", "/list-mentor/**", "/uploadAvatarAndView/{folderPath}", "/view/{fileName}", "/available-skill", "/certificate", "/payment/return_url/**", "/payment/cancel_url/**", "/course/list/{userName}", "/course/{id}", "/mentee/suggest-course", "/oms/**", "/app/**", "/filter-courses", "/queue/**", "/courses").permitAll()
                .requestMatchers("/user-wallet", "/mentee/support/{id}/confirm-end").hasAnyRole("MENTEE", "MENTOR")
                .requestMatchers("/educationStaff/**", "/admin/enableOrDisablePost/postId/{postId}", "/admin/viewPostDetail/{urlPostId}","/admin/postList").hasAnyRole("EDUCATION_STAFF", "ADMIN")
                .requestMatchers("/admin/**").hasAnyRole("ADMIN")
                .requestMatchers("/admin/supportDetail/**").hasAnyRole("MENTEE", "MENTOR", "ADMIN")
                .requestMatchers("/mentee/**", "/payment/create_payment").hasRole("MENTEE")
                .requestMatchers("/mentor/**").hasRole("MENTOR")
                .requestMatchers("/change-password", "/profile/{username}").hasAnyRole("TRANSACTION_STAFF", "EDUCATION_STAFF", "ADMIN", "MENTOR", "MENTEE")
                .requestMatchers("/transaction-staff/..").hasAnyRole("ADMIN", "TRANSACTION_STAFF")
                .anyRequest()
                .authenticated()
                .and()
                .authenticationProvider(authenticationProvider())
                .addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .logout(logout -> logout.logoutUrl("/api/auth/logout").addLogoutHandler(logoutHandler).
                        logoutSuccessHandler((request, response, authentication) -> {
                            // Xử lý khi logout thành công
                            response.setStatus(HttpServletResponse.SC_OK);
                            response.getWriter().write("Logout successful");
                            SecurityContextHolder.clearContext();
                            System.out.println("Security context cleared: " + SecurityContextHolder.getContext().getAuthentication());

                        }));
        return http.build();
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(customUserDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

}

