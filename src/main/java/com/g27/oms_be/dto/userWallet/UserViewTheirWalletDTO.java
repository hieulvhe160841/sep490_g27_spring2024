package com.g27.oms_be.dto.userWallet;

import lombok.Data;

@Data
public class UserViewTheirWalletDTO {
    private Long userWalletId;
    private Double balance;
}
