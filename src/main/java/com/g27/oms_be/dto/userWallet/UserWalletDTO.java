package com.g27.oms_be.dto.userWallet;

import lombok.Data;
import java.time.LocalDate;

@Data
public class UserWalletDTO {
    private Long userWalletId;
    private LocalDate lastUpdateDate;
    private Double balance;
    private String username;
}
