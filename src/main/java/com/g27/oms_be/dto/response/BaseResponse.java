package com.g27.oms_be.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseResponse<T> {
    private String messages;
    private int statusCode;
    private T data;

    public BaseResponse(String messages, int httpStatus) {
        this.messages = messages;
        this.statusCode = httpStatus;
    }

    public BaseResponse(String messages, int httpStatus, T data) {
        this.messages = messages;
        this.statusCode = httpStatus;
        this.data = data;
    }
}
