package com.g27.oms_be.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VerifyOtpResponse extends BaseResponse {
    private boolean checkResult;

    public VerifyOtpResponse(boolean b, String messages, int httpStatus) {
        super(messages, httpStatus);
        this.checkResult = b;
    }
}
