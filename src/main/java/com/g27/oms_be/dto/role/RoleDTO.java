package com.g27.oms_be.dto.role;

import lombok.*;

@Data
public class RoleDTO {
    private Long roleId;
    private String roleName;
}
