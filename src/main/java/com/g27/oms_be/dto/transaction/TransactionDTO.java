package com.g27.oms_be.dto.transaction;

import lombok.Data;

import java.time.LocalDate;

@Data
public class TransactionDTO {
    private Long transactionId;
    private Double amount;
    private String transactionType;
    private LocalDate timeStamp;
    private String status;
}
