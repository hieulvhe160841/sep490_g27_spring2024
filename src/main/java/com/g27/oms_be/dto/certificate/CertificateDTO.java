package com.g27.oms_be.dto.certificate;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CertificateDTO {
    private Long certificateId;
    private String certificateName;
    private String certificateUrl;
}
