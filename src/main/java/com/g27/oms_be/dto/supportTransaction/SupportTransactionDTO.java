package com.g27.oms_be.dto.supportTransaction;

import lombok.Data;

@Data
public class SupportTransactionDTO {
    private Long supportTransactionId;
    private Long supportId;
    private Double amount;
    private String transaction_type;
}
