package com.g27.oms_be.dto.userSkill;

import lombok.Data;

@Data
public class UserSkillDetailDTO {
    private String skillName;
}
