package com.g27.oms_be.dto.socket;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SocketMessageDTO {
    private Long supportId;
    private String message;
    private String username;
}
