package com.g27.oms_be.dto.support;

import lombok.Data;

import java.time.LocalDate;

@Data
public class SupportOfEachMentor {
    private Long supportId;
    private Long postId;
    private LocalDate supportCreatedDate;
    private String supportStatus;
    private String menteeUsername;
    private String menteeAvatar;
    private String menteeName;
}
