package com.g27.oms_be.dto.support;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConfirmEndSupportDTO {
    private Long supportId;
//    private boolean mentorConfirmEnd;
    private boolean menteeConfirmEnd;
    private LocalDateTime timeEnd;
}
