package com.g27.oms_be.dto.support;

import lombok.Data;

import java.time.LocalDate;

@Data
public class SupportOfEachMentee {
    private Long supportId;
    private Long postId;
    private LocalDate supportCreatedDate;
    private String supportStatus;
    private String mentorUsername;
    private String mentorAvatar;
    private Double rating;
    private String mentorName;
}
