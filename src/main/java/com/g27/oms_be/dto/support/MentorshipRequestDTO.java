package com.g27.oms_be.dto.support;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MentorshipRequestDTO {
    private Long supportId;
    private String mentor;
    private String mentee;
    private String supportStatus;
//    private String supportFeedback;
    private Double duration;
    private LocalDate supportCreatedDate;
//    private Double costPerHour;
}
