package com.g27.oms_be.dto.support;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class ViewSupportDetailDTO {
    private Long supportId;
    private String mentorUserName;
    private String menteeUserName;
    private String mentorAvatar;
    private String menteeAvatar;
    private String supportStatus;
    private LocalDate supportCreatedDate;
    private Double rating;
    private LocalDateTime timeStart;
    private LocalDateTime timeEnd;
    private boolean mentorConfirmStart;
    private boolean menteeConfirmStart;
    private boolean mentorConfirmEnd;
    private boolean menteeConfirmEnd;
    private String postContent;
    private String postPrice;
    private String educationLevel;
    private String goal;
    private String interest;
    private String menteePhone;
    private String mentorPhone;
    private Double costPerHour;
    private Double duration;
}
