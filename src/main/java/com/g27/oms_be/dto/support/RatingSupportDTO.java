package com.g27.oms_be.dto.support;

import lombok.Data;

@Data
public class RatingSupportDTO {
    private Double rating;
    private String feedback;
}
