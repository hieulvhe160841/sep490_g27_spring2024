package com.g27.oms_be.dto.support;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Data
@NoArgsConstructor
public class ViewSupportListForEachPostDTO {
    private Long supportId;
    private String mentorName;
    private String supportStatus;
    private LocalDate supportCreatedDate;
}
