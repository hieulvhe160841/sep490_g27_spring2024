package com.g27.oms_be.dto.support;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class MentorCreateSupportByPostIdDTO {
    private String mentorUserName;
    private String menteeUserName;
    private Long supportId;
    private LocalDate supportCreatedDate;
    private String supportStatus;
}
