package com.g27.oms_be.dto.support;

import com.g27.oms_be.dto.post.PostDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SupportDTO {
    private String menteeName;
    private LocalDate supportCreatedDate;
    private Double rating;
    private String supportFeedback;
  }
