package com.g27.oms_be.dto.support;

import lombok.Data;

import java.time.LocalDate;

@Data
public class SupportListDTO {
    private Long supportId;
    private String mentorName;
    private String menteeName;
    private String supportStatus;
    private LocalDate supportCreatedDate;
}
