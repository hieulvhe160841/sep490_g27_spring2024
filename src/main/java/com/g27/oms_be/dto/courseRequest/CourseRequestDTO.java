package com.g27.oms_be.dto.courseRequest;

import lombok.Data;

@Data
public class CourseRequestDTO {
    private Long courseRequestId;

    private String courseName;

    private String description;

    private String courseRequestStatus;

    private String courseRequestFeedback;

    private String price;

    private String demoSource;

    private String courseAvatar;

}
