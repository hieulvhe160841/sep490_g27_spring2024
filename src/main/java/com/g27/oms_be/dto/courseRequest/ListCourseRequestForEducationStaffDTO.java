package com.g27.oms_be.dto.courseRequest;

import lombok.Data;

@Data
public class ListCourseRequestForEducationStaffDTO {
    private Long courseRequestId;

    private String mentorUserName;

    private String courseName;

    private String description;

    private String courseRequestStatus;

    private String price;

}
