package com.g27.oms_be.dto.denominationTransaction;

import lombok.Data;

import java.time.LocalDate;

@Data
public class MenteeViewPersonalDenominationTransactionDTO {
    private Long denominationTransactionId;
    private String orderId;
    private Double value;
    private LocalDate createdDate;
}
