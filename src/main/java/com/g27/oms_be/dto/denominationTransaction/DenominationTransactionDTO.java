package com.g27.oms_be.dto.denominationTransaction;

import lombok.Data;

@Data
public class DenominationTransactionDTO {
    private Long denominationTransactionId;
    private String orderId;
    private Long userId;
    private Long denominationId;
    private Long transactionId;
}
