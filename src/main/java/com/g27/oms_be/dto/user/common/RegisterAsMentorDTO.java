package com.g27.oms_be.dto.user.common;

import com.g27.oms_be.dto.certificate.CertificateDTO;
import com.g27.oms_be.dto.skill.SkillDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterAsMentorDTO {
    private String userName;
    private Set<CertificateDTO> certificateUrl;
    private String academicLevel;
    private Set<SkillDTO> skills;
    private String experience;
}
