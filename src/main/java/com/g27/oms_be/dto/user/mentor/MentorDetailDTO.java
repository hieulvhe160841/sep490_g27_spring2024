package com.g27.oms_be.dto.user.mentor;

import com.g27.oms_be.dto.user.common.UserDetailDTO;
import lombok.Data;

@Data
public class MentorDetailDTO {
    private UserDetailDTO userDetail;
    private Double rating;
    private Double costPerHour;
    private Long numberOfDoneSupport;
}
