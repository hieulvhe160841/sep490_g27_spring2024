package com.g27.oms_be.dto.user.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NumberOfUserAndCoursePerMonthDTO {
    private Integer month;
    private Long numberOfCourseCreatedInMonth;
    private Long numberOfPeopleRegisteredInMonth;
}
