package com.g27.oms_be.dto.user.common;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MenteeDetailDTO {
    private String userName;
    @NotBlank(message = "Name can not be blank!")
    private String name;
    @NotBlank
    @Pattern(regexp = "^(0|\\+84)[3|5|7|8|9]\\d{8}$", message = "Invalid vietnamese phone number!")
    private String phone;
    private String goal;
    private String address;
    private Integer gender;
    private String avatar;
    @NotBlank(message = "Email must not be blank")
    @Email
    private String email;
    private LocalDate dayOfBirth;
    private String interest;
}
