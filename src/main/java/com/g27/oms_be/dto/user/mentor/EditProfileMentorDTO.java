package com.g27.oms_be.dto.user.mentor;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EditProfileMentorDTO {
    @NotBlank(message = "Không được để trống !")
    @Length(max = 30, message = "Độ dài tối đa là 30! ")
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Tên chỉ được chứa các chữ cái, dấu cách, dấu chấm, dấu gạch nối và dấu.")
    private String name;
    @NotBlank(message = "Không được để trống !")
    @Length(max = 100, min = 5, message = "Độ dài tối thiểu là 5 và tối đa là 100!")
    private String address;
    @Past(message = "Ngày không hợp lệ!")
    @NotNull(message = "Không được để trống")
    private LocalDate dayOfBirth;
    @NotBlank(message = "Không được để trống !")
    @Pattern(regexp = "^(0|\\+84)[3|5|7|8|9]\\d{8}$", message = "Số điện thoại không hợp lệ!")
    private String phone;
    @NotBlank(message = "Không được để trống !")
    @Length(max = 255, message = "Độ dài tối đa là 255 !")
    private String experience;
    @NotBlank(message = "Không được để trống !")
    @Length(max = 255, message = "Độ dài tối đa là 255 !")
    private String educationLevel;
    @Positive(message = "Giá tiền phải lớn hơn 0")
    @DecimalMax(value = "10000000")
    private Double costPerHours;
}
