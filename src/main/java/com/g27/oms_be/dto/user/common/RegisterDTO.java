package com.g27.oms_be.dto.user.common;

import jakarta.validation.constraints.*;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RegisterDTO {
    @NotBlank(message = "Không được để trống !")
    @Length(max = 30, min = 5,message = " Độ dài tối thiểu là 5 và tối đa là 30!")
    @Pattern(regexp = "^[a-zA-Z0-9]+([._]?[a-zA-Z0-9]+)*$", message = "Tên người dùng chỉ được chứa các chữ cái, dấu cách, dấu chấm, dấu gạch nối")
    private String userName;
    @NotBlank(message = "Không được để trống !")
    @Length(min = 8, message = "Độ dài tối thiểu của mật khẩu là 8")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%^&+=]).+$",
            message = "Mật khẩu phải chứa ít nhất một chữ thường, một chữ hoa, một chữ số và một ký tự đặc biệt")
    private String password;
    //Abc#1234 is example
    @NotBlank(message = "Không được để trống !")
    @Length(max = 30, message = "Độ dài tối đa là 30! ")
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Tên chỉ được chứa các chữ cái, dấu cách, dấu chấm, dấu gạch nối và dấu.")
    private String name;
    @NotBlank(message = "Không được để trống !")
    @Pattern(regexp = "^(0|\\+84)[3|5|7|8|9]\\d{8}$", message = "Số điện thoại việt nam không hợp lệ!")
    private String phone;
    @NotBlank(message = "Không được để trống !")
    @Length(max = 100, min = 5, message = "Độ dài tối thiểu là 5 và tối đa là 100!" )
    private String address;
    @Past(message = "Ngày không hợp lệ!")
    @NotNull(message = "Không được để trống")
    private LocalDate dayOfBirth;
    @NotNull(message = "Không được để trống")
    private Integer gender;
    private String avatar;
    @NotBlank(message = "Không được để trống !")
    @Email(message = "Định dạng không hợp lệ")
    private String email;
    private String role;
    private LocalDateTime timeGenerateOtp;
}
