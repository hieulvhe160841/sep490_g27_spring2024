package com.g27.oms_be.dto.user.admin;

import com.g27.oms_be.dto.role.RoleDTO;
import lombok.Data;

@Data
public class UserListDTO {
    private Long userId;

    private String userName;

    private String name;

    private boolean status;

    private RoleDTO roleDTO;
}
