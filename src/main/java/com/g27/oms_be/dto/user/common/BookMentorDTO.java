package com.g27.oms_be.dto.user.common;

import com.g27.oms_be.dto.post.PostDTO;
import com.g27.oms_be.dto.user.mentor.MentorDetailDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookMentorDTO {
    private MentorDetailDTO mentorDetail;
    private LocalDate latestSupportDay;
}
