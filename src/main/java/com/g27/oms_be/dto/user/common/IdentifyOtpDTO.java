package com.g27.oms_be.dto.user.common;

import lombok.Data;

@Data
public class IdentifyOtpDTO {
    private String enterOtp;
}
