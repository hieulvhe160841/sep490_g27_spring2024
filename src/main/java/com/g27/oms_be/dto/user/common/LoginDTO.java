package com.g27.oms_be.dto.user.common;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class LoginDTO {
    @NotBlank(message = "Không được để trống !")
    private String userName;
    @NotBlank(message = "Không được để trống !")
    private String password;
}
