package com.g27.oms_be.dto.user.mentor;

import com.g27.oms_be.dto.post.PostDTO;
import com.g27.oms_be.dto.user.common.UserDetailDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactMenteeDTO {
//    private Long supportId;
    private UserDetailDTO mentee;
    private LocalDate latestSupportDay;
//    private String supportStatus;
//    private LocalDate completionDate;
//    private LocalDate supportCreatedDate;
//    private Double price;
//    private String supportFeedback;
//    private LocalDateTime duration;
//    private List<PostDTO> postDTOs;
}
