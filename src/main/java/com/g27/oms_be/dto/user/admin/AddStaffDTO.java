package com.g27.oms_be.dto.user.admin;

import com.g27.oms_be.dto.role.RoleDTO;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Data
public class AddStaffDTO {
    @Length(max = 30, min = 5)
    @Pattern(regexp = "^[a-zA-Z0-9]+([._]?[a-zA-Z0-9]+)*$", message = "Tên người dùng chỉ được chứa các chữ cái, dấu cách, dấu chấm, dấu gạch nối")
    private String userName;

    @Length(max = 16, min = 8)
    private String password;

    @NotBlank(message = "Không được để trống")
    @Length(max = 30)
    private String name;

    private boolean status;
    @NotBlank
    @Pattern(regexp = "^\\d{10,11}$", message = "Invalid phone number!")
    private String phone;

    @NotBlank
    @Length(max = 100, min = 5)
    private String address;

    @NotNull
    private Integer gender;

    @NotNull
    @Past
    private LocalDate dayOfBirth;

    private RoleDTO roleDTO;
}
