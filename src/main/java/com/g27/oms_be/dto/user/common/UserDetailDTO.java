package com.g27.oms_be.dto.user.common;

import com.g27.oms_be.dto.role.RoleDTO;
import lombok.Data;

import java.time.LocalDate;

@Data
public class UserDetailDTO {
    private Long userId;

    private String userName;

    private String name;

    private boolean status;

    private String phone;

    private String address;

    private Integer gender;

    private LocalDate dayOfBirth;

    private String avatar;

    private String email;

    private String educationLevel;

//    private Double rating;

    private String experience;

    private String goal;

    private String interest;

    private LocalDate dateCreated;

    private LocalDate dateModified;

    private RoleDTO roleDTO;
}
