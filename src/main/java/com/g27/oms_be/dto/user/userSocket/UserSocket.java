package com.g27.oms_be.dto.user.userSocket;

import javax.security.auth.Subject;
import java.security.Principal;

public class UserSocket implements Principal {
    private String name;

    public UserSocket(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public boolean implies(Subject subject) {
        return Principal.super.implies(subject);
    }
}
