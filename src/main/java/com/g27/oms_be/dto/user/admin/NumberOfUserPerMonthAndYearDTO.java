package com.g27.oms_be.dto.user.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NumberOfUserPerMonthAndYearDTO {
    private Integer month;
    private Integer year;
    private Long numberOfPeopleRegisteredInMonth;
}
