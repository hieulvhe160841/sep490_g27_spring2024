package com.g27.oms_be.dto.user.common;

import jakarta.validation.constraints.Pattern;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class ResetPasswordDTO {
    @Length( min = 8)
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%^&+=]).+$",
            message = "Password must contain at least one lowercase letter," +
                    " one uppercase letter, one digit, and one special character")
    private String newPassword;
}
