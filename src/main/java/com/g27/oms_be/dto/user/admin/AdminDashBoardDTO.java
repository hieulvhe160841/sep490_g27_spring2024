package com.g27.oms_be.dto.user.admin;

import com.g27.oms_be.dto.course.CourseForAdminDashBoardDTO;
import com.g27.oms_be.dto.user.mentor.MentorDetailDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AdminDashBoardDTO {
    private Long numberOfMentors;
    private Long numberOfMentees;
    private Long numberOfOthers;
    private Long totalUsers;
    private Long totalPost;
    private Long totalRequest;
    private List<CourseForAdminDashBoardDTO> recentCourseForAdminDashBoardList;
    private Double totalProfit;
    private List<NumberOfUserAndCoursePerMonthDTO> numberOfUserAndCoursePerMonthDTOList;
    private List<MentorDetailDTO> topRatedMentor;
}
