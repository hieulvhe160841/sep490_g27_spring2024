package com.g27.oms_be.dto.user.mentor;

import com.g27.oms_be.dto.certificate.CertificateDTO;
import com.g27.oms_be.dto.skill.SkillDTO;
import com.g27.oms_be.dto.support.SupportDTO;
import lombok.Data;

import java.util.Set;

@Data
public class MentorListDTO  {
    private MentorDetailDTO mentorDetail;
    private Set<SkillDTO> skills;
    private Set<CertificateDTO> certificates;
    private Set<SupportDTO> supports;
}
