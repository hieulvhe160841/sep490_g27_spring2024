package com.g27.oms_be.dto.user.common;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class ChangePasswordDTO {
    private String userName;
    @NotBlank(message = "Không được để trống !")
    private String oldPassword;
    @Length(min = 8, message = "Độ dàu tối thiểu của mật khẩu là 8")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%^&+=]).+$",
            message = "Mật khẩu có ít nhất 1 chữ hoa, 1 chữ thường, 1 chữ số và 1 kí tự đặc biệt.")
    private String newPassword;
    @NotBlank(message = "Không được để trống !")
    private String confirmNewPassword;
}
