package com.g27.oms_be.dto.user.mentor;

import com.g27.oms_be.dto.support.SupportDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MentorSupportDTO {
    private MentorDetailDTO mentorDetail;
    private List<SupportDTO> supportDTOs;
}
