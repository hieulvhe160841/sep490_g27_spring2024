package com.g27.oms_be.dto.user.mentor;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MentorCertificateDTO {
    private String userName;
    @NotNull(message = "Không được để trống!")
    private Map<String, String> certificates;
}
