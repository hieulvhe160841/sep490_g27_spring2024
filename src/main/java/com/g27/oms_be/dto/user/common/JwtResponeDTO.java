package com.g27.oms_be.dto.user.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponeDTO {
    private String token;
    private String refreshToken;
    private String username;
    private String secret;
    private String role;
    private Long expire;

}
