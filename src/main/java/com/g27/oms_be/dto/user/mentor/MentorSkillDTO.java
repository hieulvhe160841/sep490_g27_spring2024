package com.g27.oms_be.dto.user.mentor;

import com.g27.oms_be.dto.skill.SkillDTO;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.util.Set;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MentorSkillDTO {
    private String userName;
    @NotBlank(message = "Không được để trống !")
    @Length(max = 255, message = "Độ dài tối đa là 255 !")
    private String experience;
    @NotBlank(message = "Không được để trống !")
    @Length(max = 255, message = "Độ dài tối đa là 255 !")
    private String educationLevel;
    @NotEmpty(message = "Không được để trống !")
    private Set<String> skills;
    @Positive(message = "Nhập số dương!")
    @DecimalMax(value = "10000000", message = "Giá trị không vượt quá 10000000")
    private Double costPerHour;
}
