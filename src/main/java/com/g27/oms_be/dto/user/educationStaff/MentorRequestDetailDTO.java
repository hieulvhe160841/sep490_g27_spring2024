package com.g27.oms_be.dto.user.educationStaff;

import com.g27.oms_be.dto.mentorRequest.MentorRequestDTO;
import com.g27.oms_be.dto.userCertificate.UserCertificateDetailDTO;
import com.g27.oms_be.dto.userSkill.UserSkillDetailDTO;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class MentorRequestDetailDTO {
    private List<UserCertificateDetailDTO> userCertificateDetailDTOList;
    private List<UserSkillDetailDTO> userSkillDetailDTOList;
    private MentorRequestDTO mentorRequestDetail;
}
