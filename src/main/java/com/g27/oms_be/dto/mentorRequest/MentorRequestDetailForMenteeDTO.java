package com.g27.oms_be.dto.mentorRequest;

import com.g27.oms_be.dto.userCertificate.UserCertificateDetailDTO;
import com.g27.oms_be.dto.userSkill.UserSkillDetailDTO;
import lombok.Data;

import java.util.List;

@Data
public class MentorRequestDetailForMenteeDTO {
    private Long mentorRequestId;
    private String userCreatedMentorRequestDTO;
    private String mentorRequestStatus;
    private String mentorRequestFeedback;
    private String educationLevel;
    private String experience;
    private List<UserCertificateDetailDTO> userCertificateDetailDTOList;
    private List<UserSkillDetailDTO> userSkillDetailDTOList;
}
