package com.g27.oms_be.dto.mentorRequest;

import lombok.Data;

@Data
public class AcceptOrDenyRequestDTO {
    private String feedback;
    private String status;
}
