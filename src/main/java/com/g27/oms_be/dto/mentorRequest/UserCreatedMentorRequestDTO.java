package com.g27.oms_be.dto.mentorRequest;

import lombok.Data;

@Data
public class UserCreatedMentorRequestDTO {
    private Long userId;
    private String name;
    private String userName;
    private String avatar;
    private String experience;
    private Double costPerHour;
    private String educationLevel;
}
