package com.g27.oms_be.dto.mentorRequest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MenteeViewOwnRegisAsMentorDTO {
    private Long mentorRequestId;
    private String mentorRequestFeedback;
    private String mentorRequestStatus;
}
