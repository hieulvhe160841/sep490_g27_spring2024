package com.g27.oms_be.dto.mentorRequest;

import lombok.Data;

@Data
public class MentorRequestDTO {
    private Long mentorRequestId;
    private UserCreatedMentorRequestDTO userCreatedMentorRequestDTO;
    private String mentorRequestStatus;
    private String mentorRequestFeedback;
}
