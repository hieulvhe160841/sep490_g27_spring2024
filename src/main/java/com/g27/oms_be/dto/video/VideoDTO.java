package com.g27.oms_be.dto.video;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VideoDTO {
    private Integer orderInCourse;
    private String videoUrl;
    private String videoTitle;
}
