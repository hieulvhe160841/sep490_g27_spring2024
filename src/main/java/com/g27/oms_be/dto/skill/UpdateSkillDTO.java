package com.g27.oms_be.dto.skill;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateSkillDTO {
    @Pattern(regexp = "^[a-zA-Z ]*$", message = "Tên ngôn ngữ không thể chứa số hoặc kí tự đặc biệt")
    private String skillName;
    private String skillDescription;
    @NotNull(message = "Trạng thái ngôn ngữ không thể để trống")
    private boolean skillStatus;
}
