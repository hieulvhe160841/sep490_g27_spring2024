package com.g27.oms_be.dto.skill;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SkillDTO {
    private Long skillId;
    @NotBlank
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Name can not contain special syntax and number")
    private String skillName;
    @NotBlank
    private String skillDescription;

    private Boolean skillStatus;
}
