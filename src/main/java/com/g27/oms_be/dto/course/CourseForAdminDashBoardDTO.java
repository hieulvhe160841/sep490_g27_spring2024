package com.g27.oms_be.dto.course;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseForAdminDashBoardDTO {
    private Long courseId;
    private String courseName;
    private ViewCourseOwnerDTO viewCourseOwnerDTO;
    private Double courseRating;
    private Integer numbersOfLesson;
    private Double price;
    private LocalDate courseCreatedDate;
}
