package com.g27.oms_be.dto.course;

import lombok.Data;

@Data
public class MenteeRegisCourseDTO {
    private String menteeName;
    private Long courseId;
}
