package com.g27.oms_be.dto.course;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NumberOfCoursePerMonthAndYearDTO {
    private Integer month;
    private Integer year;
    private Long numberOfCourseCreatedInMonth;
}
