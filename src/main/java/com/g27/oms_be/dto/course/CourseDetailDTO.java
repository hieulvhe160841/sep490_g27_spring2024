package com.g27.oms_be.dto.course;

import com.g27.oms_be.dto.video.VideoDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CourseDetailDTO extends CourseDTO {
    private List<VideoDTO> videoDTOs;
//    private String mentorName;
    private String mentorUsername;
}
