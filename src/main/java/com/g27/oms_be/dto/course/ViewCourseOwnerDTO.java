package com.g27.oms_be.dto.course;

import lombok.Data;

@Data
public class ViewCourseOwnerDTO {
    private String name;
}
