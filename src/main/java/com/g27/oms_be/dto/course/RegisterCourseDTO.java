package com.g27.oms_be.dto.course;

import com.g27.oms_be.dto.user.mentor.MentorDetailDTO;
import com.g27.oms_be.dto.user.mentor.MentorListDTO;
import com.g27.oms_be.dto.video.VideoDTO;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterCourseDTO {
    private Long courseId;
    @NotBlank(message = "Tên khóa học không được để trống")
    @Length(max = 100)
    private String courseName;
    @NotBlank
    private String category;
    private List<VideoDTO> lectureVideo;
    private Integer numberOfLesson;
    @Positive(message = "Giá tiền phải lớn hơn 0")
    private Double price;
    @NotBlank(message = "Mô tả khóa học không được để trống")
    @Length(max = 255)
    private String description;
    private String avatar;
    private String mentorName;
}
