package com.g27.oms_be.dto.course;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Data
public class EditCourseDTO {
    private Long courseId;
    @Length(max = 255)
    @NotBlank
    private String description;
    @Positive
    private Double price;
    private LocalDate dateModified;
}
