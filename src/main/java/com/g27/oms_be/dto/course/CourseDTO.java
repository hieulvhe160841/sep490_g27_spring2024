package com.g27.oms_be.dto.course;

import com.g27.oms_be.dto.user.common.UserDetailDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CourseDTO {
    private Long courseId;
    private String courseName;
    private String description;
    private Double price;
    private String avatar;
    private String mentorName;

//    private UserDetailDTO createUser;
}
