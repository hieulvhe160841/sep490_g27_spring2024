package com.g27.oms_be.dto.userCertificate;

import lombok.Data;

@Data
public class UserCertificateDetailDTO {
    private String certificateName;
    private String certificateUrl;
}
