package com.g27.oms_be.dto.post;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ViewPostDetailForMentorDTO {
    private String postTitle;
    private Long postId;
    private String postCreatedUser;
    private String menteeName;
    private String postContent;
    private String postPrice;
    private LocalDateTime postCreatedTime;
}
