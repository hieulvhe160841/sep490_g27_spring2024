package com.g27.oms_be.dto.post;

import lombok.Data;

@Data
public class ViewPostDetailForMenteeDTO extends ViewPostDetailForMentorDTO {
    private boolean postStatus;
}
