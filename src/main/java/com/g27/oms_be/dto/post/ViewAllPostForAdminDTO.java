package com.g27.oms_be.dto.post;

import lombok.Data;

@Data
public class ViewAllPostForAdminDTO {
    private String postTitle;
    private Long postId;
    private String postCreatedUser;
    private Double postPrice;
}
