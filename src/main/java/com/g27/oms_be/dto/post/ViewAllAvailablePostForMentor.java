package com.g27.oms_be.dto.post;

import lombok.Data;

@Data
public class ViewAllAvailablePostForMentor {
    private String postTitle;
    private Long postId;
    private String postCreatedUser;
    private String menteeName;
    private boolean postStatus;
    private Double price;
}
