package com.g27.oms_be.dto.post;

import jakarta.validation.constraints.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UpdatePostDTO {
    private String postTitle;
    private String postContent;
    private boolean postStatus;
    @Positive(message = "Price must bigger than 0")
    @Pattern(regexp = "\\d+(\\.\\d+)?", message = "Price must be number")
    private String postPrice;
    private LocalDateTime postModifiedTime;
}
