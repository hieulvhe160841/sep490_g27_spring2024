package com.g27.oms_be.dto.post;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDTO {
    private Long postId;
    private String postCreatedUser;
    @NotNull
    @NotBlank
    @NotEmpty
    private String postContent;
    private boolean postStatus;
    @NotNull
    @Positive(message = "Price must bigger than 0")
    @Pattern(regexp = "\\d+(\\.\\d+)?", message = "Price must be number")
    private String postPrice;
    private LocalDateTime postCreatedTime;
    private LocalDateTime postModifiedTime;
    @NotNull
    @NotBlank
    @NotEmpty
    private String postTitle;
}
