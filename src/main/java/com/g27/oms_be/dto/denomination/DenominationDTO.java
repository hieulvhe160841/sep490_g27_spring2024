package com.g27.oms_be.dto.denomination;

import lombok.Data;

@Data
public class DenominationDTO {
    private Long denominationId;
    private Double value;
    private Double amount;

}
