package com.g27.oms_be.validate;

import com.g27.oms_be.common.CommonString;
import com.g27.oms_be.dto.course.RegisterCourseDTO;
import com.g27.oms_be.dto.courseRequest.CourseRequestDTO;
import com.g27.oms_be.dto.response.BaseResponse;
import com.g27.oms_be.dto.user.common.EditProfileDTO;
import com.g27.oms_be.dto.user.common.MenteeDetailDTO;
import com.g27.oms_be.dto.user.mentor.EditProfileMentorDTO;
import com.g27.oms_be.service.skill.SkillService;
import com.g27.oms_be.service.user.UserService;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.ValidatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class Validator {
    @Autowired
    SkillService skillService;
    @Autowired
    private UserService userService;

    public List<String> responseData(BindingResult bindingResult) {
        List<String> errors = bindingResult.getFieldErrors().stream().map(error -> error.getField() + ": " + error.getDefaultMessage()).collect(Collectors.toList());
        return errors;
    }

    public BaseResponse checkBindingResult(BindingResult bindingResult, List<String> errors) {
        BaseResponse<Object> response = new BaseResponse<>();
        for (FieldError error : bindingResult.getFieldErrors()) {
            errors.add(error.getField() + ":" + error.getDefaultMessage());
        }
        response.setMessages("Thông tin không hợp lệ");
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
        response.setData(errors);
        return response;
    }

    public Map<String, String> checkValidateCourseRequest(CourseRequestDTO courseRequestDTO) {
        Map<String, String> errors = new HashMap<>();
        if (courseRequestDTO == null) {
            errors.put("Yêu cầu khóa học", "Yêu cầu khóa học không được để trống");
            return errors;
        }
        if (courseRequestDTO.getCourseName() == null || courseRequestDTO.getCourseName().trim().isEmpty()) {
            errors.put("Tên khóa học:", "Tên khóa học không được để trống.");
        }
        if (courseRequestDTO.getDescription() == null || courseRequestDTO.getDescription().trim().isEmpty()) {
            errors.put("Mô tả khóa học:", "Mô tả kháo học không được để trống.");
        }
        if (courseRequestDTO.getPrice() == null || courseRequestDTO.getPrice().trim().isEmpty() || !isValidPrice(courseRequestDTO.getPrice())) {
            errors.put("Giá khóa học:", "Giá khóa học không được để trống");
        }
        return errors;
    }

//    public Map<String, String> registerCourse(RegisterCourseDTO registerCourseDTO) {
//        Map<String, String> errors = new HashMap<>();
//        if (registerCourseDTO == null) {
//            errors.put("RegisterCourse", "Must not be empty!");
//            return errors;
//
//        }
//        if (registerCourseDTO.getCourseName().trim().length() == 0) {
//            errors.put("CourseName:", "Course name must not be empty.");
//        }
//        if (registerCourseDTO.getDescription().trim().length() == 0) {
//            errors.put("CourseDescription:", "Course description must not be empty.");
//        }
//        if (registerCourseDTO.getNumberOfLesson() < 0) {
//            errors.put("NumberOfLesson:", "Number of lesson can not be 0.");
//        }
//        if (isValidDouble(String.valueOf(registerCourseDTO.getPrice())) &&
//                (registerCourseDTO.getPrice() < 10000D || registerCourseDTO.getPrice() > 5000000D)) {
//            errors.put("CoursePrice", "Must be in range 10000 to 500000");
//        }
//        return errors;
//    }

    public boolean isValidPrice(String price) {
        return price.matches(CommonString.REGEX_CHECK_VALIDATE_PRICE_FORMAT);
    }

    boolean isValidDouble(String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public List<String> editProfile(MenteeDetailDTO menteeDetail) {
        List<String> errors = new ArrayList<>();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        jakarta.validation.Validator validator = factory.getValidator();
        Set<ConstraintViolation<MenteeDetailDTO>> violations = validator.validate(menteeDetail);
        for (ConstraintViolation<MenteeDetailDTO> violation : violations) {
            errors.add(violation.getMessage());
        }
        return errors;
    }

    public List<String> checkDuplicate(String userName, String phone, String email) {
        List<String> error =new ArrayList<>();
        if (userName != null && userService.existsByUserName(userName)) {
            error.add("Tên người dùng đã tồn tại");
        }
        if (phone != null && userService.existsByPhone(phone)) {
            error.add("Số điện thoại đã tồn tại");
        }
        if (email != null && userService.existsByEmail(email)) {
            error.add("Email đã tồn tại");
        }

        return error;
    }

    public List<String> editProfileMentor(EditProfileMentorDTO editProfileMentor) {
        List<String> errors = new ArrayList<>();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        jakarta.validation.Validator validator = factory.getValidator();
        Set<ConstraintViolation<EditProfileMentorDTO>> violations = validator.validate(editProfileMentor);
        for (ConstraintViolation<EditProfileMentorDTO> violation : violations) {
            errors.add(violation.getPropertyPath().toString() + " : " + violation.getMessage());
        }
        return errors;
    }
    public List<String> editProfile(EditProfileDTO editProfileDTO) {
        List<String> errors = new ArrayList<>();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        jakarta.validation.Validator validator = factory.getValidator();
        Set<ConstraintViolation<EditProfileDTO>> violations = validator.validate(editProfileDTO);
        for (ConstraintViolation<EditProfileDTO> violation : violations) {
            errors.add(violation.getPropertyPath().toString() + " : " + violation.getMessage());
        }
        return errors;
    }
    public List<String> validateObject(Object object) {
        List<String> errors = new ArrayList<>();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        jakarta.validation.Validator validator = factory.getValidator();
        Set<ConstraintViolation<Object>> violations = validator.validate(object);
        for (ConstraintViolation<Object> violation : violations) {
            errors.add(violation.getPropertyPath().toString() + " : " + violation.getMessage());
        }
        return errors;
    }
}


