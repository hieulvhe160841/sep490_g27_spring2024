package com.g27.oms_be.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@Service
public class JwtTokenProvider {
    @Value("${ra.jwt.secret}")
    private String JWT_SECRET;

    @Value("${ra.jwt.expiration}")
    private Long JWT_EXPIRATION;

    @Value("${ra.jwt.refresh-secret}")
    private String JWT_REFRESH_SECRET;

    @Value("${ra.jwt.refresh-expiration}")
    private Long JWT_REFRESH_EXPIRATION;


    public String generateToken(CustomUserDetails customUserDetails) {
        Date now = new Date();
        Date dateExpire = new Date(now.getTime() + JWT_EXPIRATION);
        System.out.println(customUserDetails.getAuthorities());
        return Jwts.builder().
                setSubject(customUserDetails.getUsername())
                .setIssuedAt(now)
                .setExpiration(dateExpire)
                .signWith(SignatureAlgorithm.HS256, JWT_SECRET)
                .compact();
    }

    public String getUserNameFromJwt(String token) {
        Claims claim = Jwts.parserBuilder()
                .setSigningKey(JWT_SECRET)
                .build()
                .parseClaimsJws(token)
                .getBody();
        return claim.getSubject();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET)
                    .parseClaimsJws(token);
            return true;
        } catch (MalformedJwtException ex) {
            log.error("invalid jwt token!");
        } catch (ExpiredJwtException ex) {
            log.error("expired jwt token");
        } catch (UnsupportedJwtException ex) {
            log.error("unsupported jwt token. Cannot parse");
        } catch (IllegalArgumentException ex) {
            log.error("jwt claims string is empty");
        }
        return false;
    }

    public String generateRefreshToken(CustomUserDetails customUserDetails) {
        Date now = new Date();
        Date dateExpire = new Date(now.getTime() + JWT_REFRESH_EXPIRATION);
        System.out.println(customUserDetails.getAuthorities());
        return Jwts.builder().
                setSubject(customUserDetails.getUsername())
                .setIssuedAt(now)
                .setExpiration(dateExpire)
                .signWith(SignatureAlgorithm.HS256, JWT_REFRESH_SECRET)
                .compact();
    }

    public String getUserNameFromRefreshJwt(String token) {
        Claims claim = Jwts.parserBuilder()
                .setSigningKey(JWT_REFRESH_SECRET)
                .build()
                .parseClaimsJws(token)
                .getBody();
        return claim.getSubject();
    }

    public boolean validateRefreshToken(String token) {
        try {
            Jwts.parser().setSigningKey(JWT_REFRESH_SECRET)
                    .parseClaimsJws(token);
            return true;
        } catch (MalformedJwtException ex) {
            log.error("invalid jwt token!");
        } catch (ExpiredJwtException ex) {
            log.error("expired jwt token");
        } catch (UnsupportedJwtException ex) {
            log.error("unsupported jwt token. Cannot parse");
        } catch (IllegalArgumentException ex) {
            log.error("jwt claims string is empty");
        }
        return false;
    }

    private Key getSignKey() {
        byte[] keyBytes= Decoders.BASE64.decode(JWT_SECRET);
        return Keys.hmacShaKeyFor(keyBytes);
    }


}
