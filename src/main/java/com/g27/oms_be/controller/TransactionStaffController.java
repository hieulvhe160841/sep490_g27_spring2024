package com.g27.oms_be.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.g27.oms_be.common.CommonString;
import com.g27.oms_be.dto.response.BaseResponse;
import com.g27.oms_be.dto.transaction.TransactionDTO;
import com.g27.oms_be.dto.user.common.EditProfileDTO;
import com.g27.oms_be.dto.user.common.UserDetailDTO;
import com.g27.oms_be.security.CustomUserDetails;
import com.g27.oms_be.service.transaction.TransactionService;
import com.g27.oms_be.service.user.UserService;
import com.g27.oms_be.validate.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/transaction-staff")
@CrossOrigin(origins = "*")
public class TransactionStaffController {
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private Validator validator;

    @GetMapping("/transaction-list")
    public ResponseEntity<Object> getAllTransaction() {
        List<TransactionDTO> transactionDTOList = transactionService.listAllTransaction();
        if (transactionDTOList != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), transactionDTOList), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>(CommonString.NOT_FOUND_MESSAGES, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @PutMapping("/profile/edit")
    public ResponseEntity<Object> editProfile(@RequestParam("avatar") MultipartFile avatar,
                                              @RequestParam("profile") String profile) {
        try {
            EditProfileDTO editProfileDTO = objectMapper.readValue(profile, EditProfileDTO.class);
            List<String> errors = new ArrayList<>();
            if (validator.editProfile(editProfileDTO).size() != 0) {
                errors = validator.editProfile(editProfileDTO);
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }
            if (validator.checkDuplicate(null, editProfileDTO.getPhone(), null).size() != 0) {
                errors = validator.checkDuplicate(null, editProfileDTO.getPhone(), null);
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
            UserDetailDTO userDetailDTO = userService.editProfile(editProfileDTO, avatar, customUserDetails.getUsername());
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), userDetailDTO), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(new BaseResponse<>("Không thể cập nhật hồ sơ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
