package com.g27.oms_be.controller;

import com.g27.oms_be.common.CommonString;
import com.g27.oms_be.dto.course.CourseForAdminDashBoardDTO;
import com.g27.oms_be.dto.course.NumberOfCoursePerMonthAndYearDTO;
import com.g27.oms_be.dto.post.PostDTO;
import com.g27.oms_be.dto.post.ViewAllPostForAdminDTO;
import com.g27.oms_be.dto.skill.SkillDTO;
import com.g27.oms_be.dto.skill.UpdateSkillDTO;
import com.g27.oms_be.dto.support.SupportListDTO;
import com.g27.oms_be.dto.support.ViewSupportDetailDTO;
import com.g27.oms_be.dto.user.admin.*;
import com.g27.oms_be.dto.user.mentor.MentorDetailDTO;
import com.g27.oms_be.dto.user.common.UserDetailDTO;
import com.g27.oms_be.dto.response.BaseResponse;
import com.g27.oms_be.service.course.CourseService;
import com.g27.oms_be.service.post.PostService;
import com.g27.oms_be.service.skill.SkillService;
import com.g27.oms_be.service.support.SupportService;
import com.g27.oms_be.service.transaction.TransactionService;
import com.g27.oms_be.service.user.UserService;
import com.g27.oms_be.validate.Validator;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/admin")
@CrossOrigin(origins = "*")
public class AdminController {
    @Autowired
    private UserService userService;
    @Autowired
    private SkillService skillService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private PostService postService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private Validator validator;
    @Autowired
    private SupportService supportService;

    @GetMapping("/listUser")
    public ResponseEntity<Object> getAllUser() {
        List<UserListDTO> userListDTOS = userService.getAllUser();
        if (!userListDTOS.isEmpty()) {
            return new ResponseEntity<>(new BaseResponse<>("OK", 200, userListDTOS), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse("Không tìm thấy người dùng nào", 404), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addStaff")
    public ResponseEntity<Object> addStaff(@Valid @RequestBody AddStaffDTO addStaffDTO, BindingResult bindingResult) {
        BaseResponse<Object> response = new BaseResponse<>();
        List<String> errors = new ArrayList<>();

        if (bindingResult.hasErrors()) {
            response = validator.checkBindingResult(bindingResult, errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        if (!userService.existsByUserName(addStaffDTO.getUserName()) && !userService.existsByPhone(addStaffDTO.getPhone())) {
            AddStaffDTO addStaff = userService.addStaff(addStaffDTO);
            response.setMessages("Đăng ký nhân viên thành công");
            response.setStatusCode(HttpStatus.CREATED.value());
            response.setData(addStaff);
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } else {
            if (userService.existsByUserName(addStaffDTO.getUserName())) {
                errors.add("Tên đăng nhập đã tồn tại");
            }
            if (userService.existsByPhone(addStaffDTO.getPhone())) {
                errors.add("Số điện thoại đã tồn tại");
            }
            response.setMessages("Đăng ký nhân viên thất bại");
            response.setStatusCode(HttpStatus.BAD_REQUEST.value());
            response.setData(errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/userDetail/{username}")
    public ResponseEntity<Object> getUserDetailByUsername(@PathVariable String username) {
        UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(username);
        if (userDetailDTO != null) {
            return new ResponseEntity<>(new BaseResponse<>("OK", 200, userDetailDTO), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse("Người dùng không tồn tại", 404), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addSkill")
    public ResponseEntity<Object> addSkill(@Valid @RequestBody SkillDTO skillDTO, BindingResult bindingResult) {
        BaseResponse<Object> response = new BaseResponse<>();
        List<String> errors = new ArrayList<>();
        if (bindingResult.hasErrors()) {
            response = validator.checkBindingResult(bindingResult, errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        } else {
            if (skillService.checkDuplicateSkillName(skillDTO.getSkillName())) {
                skillDTO.setSkillStatus(true);
                SkillDTO addSkill = skillService.addSkill(skillDTO);
                response.setMessages("Thêm ngôn ngữ thành công");
                response.setStatusCode(HttpStatus.OK.value());
                response.setData(addSkill);
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else if (!skillService.checkDuplicateSkillName(skillDTO.getSkillName())) {
                errors.add("Tên ngôn ngữ đã tồn tại");
            }
            response.setMessages("Thêm ngôn ngữ thất bại");
            response.setStatusCode(HttpStatus.BAD_REQUEST.value());
            response.setData(errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/updateSkill/{urlSkillId}")
    public ResponseEntity<Object> updateSkill(@PathVariable String urlSkillId, @Valid @RequestBody UpdateSkillDTO updateSkillDTO, BindingResult bindingResult) {
        try {
            Long skillId = Long.parseLong(urlSkillId);
            if (skillService.checkSkillExisted(skillId)) {
                BaseResponse<Object> response = new BaseResponse<>();
                List<String> errors = new ArrayList<>();
                if (bindingResult.hasErrors()) {
                    response = validator.checkBindingResult(bindingResult, errors);
                    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                } else {
                    if (skillService.checkDupicateSkillNameExceptId(updateSkillDTO.getSkillName(), skillId)) {
                        UpdateSkillDTO updateSkill = skillService.updateSkill(updateSkillDTO, skillId);
                        response.setMessages("Cập nhật ngôn ngữ thành công");
                        response.setStatusCode(HttpStatus.OK.value());
                        response.setData(updateSkill);
                        return new ResponseEntity<>(response, HttpStatus.OK);
                    } else if (!skillService.checkDupicateSkillNameExceptId(updateSkillDTO.getSkillName(), skillId)) {
                        errors.add("Tên ngôn ngữ đã tồn tại");
                        response.setMessages("Cập nhật ngôn ngữ thất bại");
                        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
                        response.setData(errors);
                    }
                    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                }
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy ngôn ngữ", HttpStatus.NOT_FOUND.value()), HttpStatus.OK);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/dashBoard")
    public ResponseEntity<Object> adminDashBoard() {
        Long percentageOfMentors = userService.percentageOfMentors();

        Long percentageOfMentees = userService.percentageOfMentees();

        Long percentageOfOthers = userService.numberOfAllUsers() - (percentageOfMentees + percentageOfMentors);

        Long totalUser = userService.numberOfAllUsers();

        Long totalPost = postService.totalNumberOfPost();

        Double totalProfit = transactionService.totalAmountOfSupportAndCourseRegistrationTransaction() * 0.1;

        List<CourseForAdminDashBoardDTO> courseForAdminDashBoardList = courseService.getTop5RecentCourseForAdminDashBoard();

        List<NumberOfUserPerMonthAndYearDTO> numberOfUserRegisteredPerMonthAndYearList = userService.countUsersRegisteredInMonth();

        List<NumberOfCoursePerMonthAndYearDTO> numberOfCoursePerMonthAndYearList = courseService.getNumberOfCourseCreatedPerMonth();

        List<NumberOfUserAndCoursePerMonthDTO> numberOfUserAndCoursePerMonthList = userService.numberOfUserAndCoursePerMonthDtoList(numberOfUserRegisteredPerMonthAndYearList, numberOfCoursePerMonthAndYearList);
        List<MentorDetailDTO> topRatedMentorList = userService.getTopRateMentor();

        Long totalRequest = userService.totalNumberOfRequest();
        AdminDashBoardDTO adminDashBoardDTO = AdminDashBoardDTO.builder()
                .totalUsers(totalUser)
                .numberOfMentors(percentageOfMentors)
                .numberOfMentees(percentageOfMentees)
                .numberOfOthers(percentageOfOthers)
                .totalPost(totalPost).totalRequest(totalRequest)
                .recentCourseForAdminDashBoardList(courseForAdminDashBoardList)
                .totalRequest(totalRequest)
                .totalProfit(totalProfit)
                .recentCourseForAdminDashBoardList(courseForAdminDashBoardList)
                .numberOfUserAndCoursePerMonthDTOList(numberOfUserAndCoursePerMonthList)
                .topRatedMentor(topRatedMentorList).build();
        return new ResponseEntity<>(new BaseResponse<>("OK", HttpStatus.OK.value(), adminDashBoardDTO), HttpStatus.OK);
    }

    @PutMapping("/enableOrDisablePost/postId/{postId}")
    public ResponseEntity<Object> updatePostByPostId(@PathVariable String postId) {
        BaseResponse<Object> response = new BaseResponse<>();
        try {
            Long postIdValue = Long.valueOf(postId);
            PostDTO postDTO = postService.updatePostForAdminByPostId(postIdValue, null);
            if (postDTO != null) {
                response.setMessages("Cập nhật trạng thái thành công");
                response.setStatusCode(HttpStatus.OK.value());
                response.setData(postDTO);
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                response.setMessages("Không tìm thấy bài đăng, thay đổi trạng thái thất bại");
                response.setStatusCode(HttpStatus.BAD_REQUEST.value());
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (IllegalArgumentException e) {
            response.setMessages("Thông tin đầu vào không hợp lệ");
            response.setStatusCode(HttpStatus.BAD_REQUEST.value());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/viewPostDetail/{urlPostId}")
    public ResponseEntity<Object> viewPostDetailForMentee(@PathVariable String urlPostId) {
        try {
            Long postId = Long.parseLong(urlPostId);
            PostDTO postDTO = postService.getPostByID(postId);
            if (postDTO != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), postDTO), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy bài đăng", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/postList")
    public ResponseEntity<Object> postList() {
        List<ViewAllPostForAdminDTO> postForAdminDTOList = postService.postListForAdmin();
        if (postForAdminDTOList != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), postForAdminDTOList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy bài đăng", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/supportDetail/{urlSupportId}")
    public ResponseEntity<Object> supportDetail(@PathVariable String urlSupportId) {
        try {
            Long supportId = Long.parseLong(urlSupportId);
            ViewSupportDetailDTO viewSupportDetail = supportService.viewSupportDetail(supportId);
            if (viewSupportDetail != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), viewSupportDetail), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy phiên trợ giúp", HttpStatus.NOT_FOUND.value()), HttpStatus.OK);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/supportList")
    public ResponseEntity<Object> supportList() {
        List<SupportListDTO> supportListDTOList = supportService.viewSupportList();
        if (supportListDTOList != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), supportListDTOList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy phiên gia sư", HttpStatus.NOT_FOUND.value()), HttpStatus.OK);
        }
    }

    @GetMapping("/skillDetail/{urlSkillId}")
    public ResponseEntity<Object> skillDetail(@PathVariable String urlSkillId) {
        try {
            Long skillId = Long.parseLong(urlSkillId);
            SkillDTO skillDTO = skillService.skillDetail(skillId);
            if (skillDTO != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), skillDTO), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy ngôn ngữ", HttpStatus.NOT_FOUND.value()), HttpStatus.OK);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/listUser/{username}")
    public ResponseEntity<Object> updateStatus(@PathVariable("username") String username) {
        String notification = userService.updateStatus(username);
        if (notification != null) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), notification), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), "Cập nhật trạng thái người dùng thất bại"), HttpStatus.OK);
    }

}
