package com.g27.oms_be.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.g27.oms_be.common.CommonString;
import com.g27.oms_be.dto.response.BaseResponse;
import com.g27.oms_be.dto.response.VerifyOtpResponse;
import com.g27.oms_be.dto.user.common.*;
import com.g27.oms_be.security.CustomUserDetails;
import com.g27.oms_be.security.CustomUserDetailsService;
import com.g27.oms_be.security.JwtTokenProvider;
import com.g27.oms_be.service.user.UserService;
import com.g27.oms_be.validate.Validator;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jakarta.mail.MessagingException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@RestController
@RequestMapping("/auth")
@CrossOrigin("*")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider tokenProvider;
    @Autowired
    private UserService userService;
    @Autowired
    private Validator validator;
    @Autowired
    private CustomUserDetailsService customUserDetailsService;
    @Value("${ra.jwt.expiration}")
    private Long expire;
    @Value("${ra.jwt.refresh-expiration}")
    private Long refreshExpire;
    private static String CHAT_ENGINE_PROJECT_ID = "3115972e-266e-4915-b179-1d21a682a59d";
    private static String CHAT_ENGINE_PRIVATE_KEY = "8bfc3459-5db6-4163-8653-485aba38201d";
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("/login")
    public ResponseEntity<Object> loginUser(@Valid @RequestBody LoginDTO loginDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<String> errors = validator.responseData(bindingResult);
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
        }
        if (userService.findByUserName(loginDTO.getUserName()) == null && loginDTO.getPassword() != null) {
            return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy người dùng", HttpStatus.NOT_FOUND.value(), loginDTO), HttpStatus.NOT_FOUND);
        }
        if (userService.findByUserName(loginDTO.getUserName()) != null && userService.checkLogin(loginDTO) != null) {
            return new ResponseEntity<>(new BaseResponse(userService.checkLogin(loginDTO), HttpStatus.UNAUTHORIZED.value(), loginDTO), HttpStatus.UNAUTHORIZED);
        }
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUserName(), loginDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        String jwt = tokenProvider.generateToken(customUserDetails);
        String refreshJwt = tokenProvider.generateRefreshToken(customUserDetails);
        UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(loginDTO.getUserName());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Project-ID", CHAT_ENGINE_PROJECT_ID);
        headers.set("User-Name", customUserDetails.getUsername().toLowerCase());
        System.out.println(customUserDetails.getSecret());
        headers.set("User-Secret", customUserDetails.getSecret());


        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "https://api.chatengine.io/users/me",
                HttpMethod.GET,
                entity,
                String.class);
        HttpStatus statusCode = (HttpStatus) response.getStatusCode();
        if (statusCode == HttpStatus.OK) {
            String responseBody = response.getBody();
            Map<String, Object> responseObject = new Gson().fromJson(responseBody, new TypeToken<Map<String, Object>>() {
            }.getType());
        }
        return new ResponseEntity<>(new BaseResponse("OK", HttpStatus.OK.value(), new JwtResponeDTO(jwt, refreshJwt, customUserDetails.getUsername(), customUserDetails.getSecret(), userDetailDTO.getRoleDTO().getRoleName(), expire)), HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<Object> addUser(@Valid @RequestBody RegisterDTO registerDTO, BindingResult bindingResult) throws MessagingException {

        List<String> errors = new ArrayList<>();
        if (bindingResult.hasErrors()) {
            errors = validator.responseData(bindingResult);
            return new ResponseEntity<>(new BaseResponse(HttpStatus.BAD_REQUEST.toString(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
        }
        errors = validator.checkDuplicate(registerDTO.getUserName(), registerDTO.getPhone(), registerDTO.getEmail());
        if (!errors.isEmpty()) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.toString(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new BaseResponse<>("Mã otp đã được gửi đến email đăng kí !", HttpStatus.CREATED.value(), userService.sendEmail(registerDTO)), HttpStatus.CREATED);
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<Object> refresh(@RequestBody RefreshTokenDTO refreshToken) {
        boolean check = tokenProvider.validateRefreshToken(refreshToken.getRefreshToken());
        if (check) {
            JwtResponeDTO jwtResponeDTO = new JwtResponeDTO();
            String userName = tokenProvider.getUserNameFromRefreshJwt(refreshToken.getRefreshToken());
            UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(userName);
            if (userDetailDTO != null) {
                CustomUserDetails customUserDetails = new CustomUserDetails(userName, " ", userDetailDTO.getEmail(), userDetailDTO.getName(), userDetailDTO.isStatus(), userDetailDTO.getDateCreated(), "", new ArrayList<>());
                String jwt = tokenProvider.generateToken(customUserDetails);
                String refreshJwt = tokenProvider.generateRefreshToken(customUserDetails);
                jwtResponeDTO.setToken(jwt);
                jwtResponeDTO.setRefreshToken(refreshJwt);
                jwtResponeDTO.setExpire(refreshExpire);
                jwtResponeDTO.setUsername(userName);
                jwtResponeDTO.setRole(userDetailDTO.getRoleDTO().getRoleName());
            }
            return new ResponseEntity<>(new BaseResponse<>("Xác minh thành công", 201, jwtResponeDTO), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse<>("Xác minh thất bại", 401), HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/verify")
    public ResponseEntity<?> verify(@RequestParam String email, @RequestParam("otp") String otp) {
        if (otp.isEmpty()) {
            return new ResponseEntity<>(new BaseResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), "Nhập email!"), HttpStatus.BAD_REQUEST);
        }
        String message = userService.verifyEmail(email, otp);
        if (message.equals("Thời gian hết hạn, vui lòng tạo OTP mới")) {
            return new ResponseEntity<>(new BaseResponse(message, HttpStatus.REQUEST_TIMEOUT.value()), HttpStatus.REQUEST_TIMEOUT);
        }
        if (message.equals("OTP không hợp lệ,hãy nhập lại OTP")) {
            return new ResponseEntity<>(new BaseResponse(message, HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
        String username = message.substring(21);
        String url = "https://api.chatengine.io/users";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Private-Key", CHAT_ENGINE_PRIVATE_KEY);

        Map<String, String> body = new HashMap<String, String>();
        body.put("username", username.toLowerCase());
        body.put("secret", otp);

        HttpEntity<Map<String, String>> requestBody = new HttpEntity<>(body, headers);
        ResponseEntity<Map> response = restTemplate.exchange(url, HttpMethod.POST, requestBody, Map.class);
        return new ResponseEntity<>(new BaseResponse("Xác minh tài khoản thành công", HttpStatus.OK.value(), username), HttpStatus.OK);
    }

    @PutMapping("/regenerate-otp")
    public ResponseEntity<?> regenerateOtp(@RequestParam String email) {
        return new ResponseEntity<>(new BaseResponse(userService.regenerateOtp(email), HttpStatus.OK.value(), email), HttpStatus.OK);
    }

    @PutMapping("/forgot-password")
    public ResponseEntity<Object> forgotPassword(@RequestBody ForgotPasswordDTO forgotPasswordDTO) {
        if (forgotPasswordDTO.getEmail() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        String forgotPasswordMessage = userService.forgotPassword(forgotPasswordDTO.getEmail());
        if (forgotPasswordMessage == null) {
            return new ResponseEntity<>(new BaseResponse("Người dùng không tồn tại", 404), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new BaseResponse(forgotPasswordMessage, 200), HttpStatus.OK);
    }

    @PostMapping("/identify")
    public ResponseEntity<Object> identifyOtp(@RequestParam String email, @RequestBody IdentifyOtpDTO identifyOtpDTO) {
        if (identifyOtpDTO.getEnterOtp() == null) {
            return new ResponseEntity<>(new BaseResponse("Vui lòng nhập OTP", 400), HttpStatus.BAD_REQUEST);
        }
        if (userService.verifyOtp(email, identifyOtpDTO.getEnterOtp())) {
            return new ResponseEntity<>(new VerifyOtpResponse(true, "Xác minh OTP thành công", 200), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new VerifyOtpResponse(false, "OTP không hợp lệ, vui lòng nhập lại OTP", 401), HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/change-password")
    public ResponseEntity<Object> changePassword(@Valid @RequestBody ChangePasswordDTO changePasswordDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<String> error = validator.responseData(bindingResult);
            return new ResponseEntity<>(new BaseResponse("Thông tin không hợp lệ", HttpStatus.BAD_REQUEST.value(), error), HttpStatus.BAD_REQUEST);
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        changePasswordDTO.setUserName(userDetails.getUsername());
        String message = userService.changePassword(changePasswordDTO);
        if (message.equals("Mật khẩu đã được thay đổi!")) {

            return new ResponseEntity<>(new BaseResponse("Thay đổi mật khẩu thành công", HttpStatus.OK.value(), changePasswordDTO), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse(message, HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/reset-password")
    public ResponseEntity<Object> resetPassword(@RequestParam String email, @Valid @RequestBody ResetPasswordDTO resetPasswordDTO, BindingResult bindingResult) {
        if (email == null) {
            return new ResponseEntity<>(new BaseResponse("Vui lòng cung cấp email", 400), HttpStatus.BAD_REQUEST);
        }
        if (bindingResult.hasErrors()) {
            List<String> error = validator.responseData(bindingResult);
            return new ResponseEntity<>(new BaseResponse("Thông tin không hợp lệ", HttpStatus.BAD_REQUEST.value(), error), HttpStatus.BAD_REQUEST);
        } else {
            String resetPasswordResult = userService.resetPassword(resetPasswordDTO, email);
            if (resetPasswordResult == null) {
                return new ResponseEntity<>(new BaseResponse("Người dùng không tồn tại", 404), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(new BaseResponse(resetPasswordResult, 200), HttpStatus.OK);
        }
    }

    @PostMapping("/regenerate-otp-reset-password")
    public ResponseEntity<?> regenerateOtpForResetPassword(@RequestParam String email) {
        return new ResponseEntity<>(new BaseResponse(userService.forgotPassword(email), HttpStatus.OK.value(), email), HttpStatus.OK);
    }
}
