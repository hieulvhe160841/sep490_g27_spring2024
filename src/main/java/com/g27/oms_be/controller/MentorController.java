package com.g27.oms_be.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.g27.oms_be.common.CommonString;
import com.g27.oms_be.dto.course.EditCourseDTO;
import com.g27.oms_be.dto.course.RegisterCourseDTO;
import com.g27.oms_be.dto.courseRequest.CourseRequestDTO;
import com.g27.oms_be.dto.courseRequest.CourseRequestDetailDTO;
import com.g27.oms_be.dto.post.ViewAllAvailablePostForMentor;
import com.g27.oms_be.dto.post.ViewPostDetailForMentorDTO;
import com.g27.oms_be.dto.response.BaseResponse;
import com.g27.oms_be.dto.support.*;
import com.g27.oms_be.dto.user.common.UserDetailDTO;
import com.g27.oms_be.dto.user.mentor.ContactMenteeDTO;
import com.g27.oms_be.dto.user.mentor.EditProfileMentorDTO;
import com.g27.oms_be.dto.user.mentor.MentorListDTO;
import com.g27.oms_be.security.CustomUserDetails;
import com.g27.oms_be.service.course.CourseService;
import com.g27.oms_be.service.courseRequest.CourseRequestService;
import com.g27.oms_be.service.post.PostService;
import com.g27.oms_be.service.support.SupportService;
import com.g27.oms_be.service.user.UserService;
import com.g27.oms_be.service.video.VideoService;
import com.g27.oms_be.validate.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mentor")
@CrossOrigin(origins = "*")
public class MentorController {
    @Autowired
    private SupportService supportService;
    @Autowired
    private CourseRequestService courseRequestService;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    Validator validator;
    @Autowired
    PostService postService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private UserService userService;
    @Autowired
    private VideoService videoService;

    @GetMapping("/list-contact-mentee")
    public ResponseEntity<Object> listContactMentee() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<ContactMenteeDTO> contactMenteeDTOS = supportService.getMenteesByMentor(customUserDetails.getUsername());
        if (contactMenteeDTOS != null) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), contactMenteeDTOS), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.NOT_FOUND.getReasonPhrase(), HttpStatus.NOT_FOUND.value(), "Không có mentee nào trong danh sách!"), HttpStatus.NOT_FOUND);
    }

    @PostMapping("/createCourseRequest")
    public ResponseEntity<Object> createCourseRequest(@RequestParam("videoFile") MultipartFile videoFile, @RequestParam("imageFile") MultipartFile avatarFile, @RequestParam("courseRequest") String courseRequestDTO) throws JsonProcessingException {
        if (videoFile == null || avatarFile == null || videoFile.isEmpty() || avatarFile.isEmpty()) {
            Map<String, String> validateInputFile = new HashMap<>();
            if (videoFile == null || videoFile.isEmpty()) {
                validateInputFile.put("Video mô tả", "Video mô tả không được để trống.");
            }
            if (avatarFile == null || avatarFile.isEmpty()) {
                validateInputFile.put("Ảnh khóa học", "Ảnh khóa học không được để trống.");
            }
            return new ResponseEntity<>(new BaseResponse<>("MISSING FILES", HttpStatus.BAD_REQUEST.value(), validateInputFile), HttpStatus.BAD_REQUEST);
        }
        CourseRequestDTO parseCourseRequest = objectMapper.readValue(courseRequestDTO, CourseRequestDTO.class);
        try {
            Map<String, String> validateInputData = validator.checkValidateCourseRequest(parseCourseRequest);
            if (!validateInputData.isEmpty()) {
                return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value(), validateInputData), HttpStatus.CREATED);
            }
            CourseRequestDTO courseRequest = courseRequestService.createCourseRequest(parseCourseRequest, avatarFile, videoFile);
            return new ResponseEntity<>(new BaseResponse<>("Tạo yêu cầu duyệt khóa học thành công", HttpStatus.CREATED.value(), courseRequest), HttpStatus.CREATED);
        } catch (IOException e) {
            return new ResponseEntity<>(new BaseResponse<>("Không thể tạo yêu cầu duyệt khóa học", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/viewCourseRequestDetail/{urlCourseRequestId}")
    public ResponseEntity<Object> viewCourseRequestDetail(@PathVariable String urlCourseRequestId) {
        try {
            Long courseRequestId = Long.parseLong(urlCourseRequestId);
            CourseRequestDetailDTO courseRequestDetailDTO = courseRequestService.viewCourseRequestDetailForMentor(courseRequestId);
            if (courseRequestDetailDTO != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), courseRequestDetailDTO), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy yêu cầu duyệt khóa học phù hợp nào", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/viewPostDetail/{urlPostId}")
    public ResponseEntity<Object> viewPostDetailForMentor(@PathVariable String urlPostId) {
        try {
            Long postId = Long.parseLong(urlPostId);
            ViewPostDetailForMentorDTO viewPostDetailForMentorDTO = postService.viewPostDetailForMentorByPostId(postId);
            if (viewPostDetailForMentorDTO != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), viewPostDetailForMentorDTO), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy bài đăng phù hợp nào", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/viewPost")
    public ResponseEntity<Object> viewPostList() {
        List<ViewAllAvailablePostForMentor> allAvailablePostForMentor = postService.viewAllAvailablePostForMentor();
        if (allAvailablePostForMentor != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), allAvailablePostForMentor), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy bài đăng phù hợp nào", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @PostMapping("/createSupportByPostId/{urlPostId}")
    public ResponseEntity<Object> createSupportByPostId(@PathVariable String urlPostId) {
        try {
            Long postId = Long.parseLong(urlPostId);
            if (postService.getPostByID(postId).isPostStatus() == true) {
                MentorCreateSupportByPostIdDTO support = supportService.mentorSupportMenteeByPostId(postId);
                if (support != null) {
                    return new ResponseEntity<>(new BaseResponse<>("Tạo yêu cầu gia sư cho bài đăng thành công", HttpStatus.CREATED.value(), support), HttpStatus.CREATED);
                }
                return new ResponseEntity<>(new BaseResponse<>("Tạo yêu cầu gia sư cho bài đăng thất bại", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Hiện tại không thể tạo yêu cầu gia sư cho bài đăng này", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/listSupport")
    public ResponseEntity<Object> viewListSupportOfEachMentor() {
        if (supportService.listOfSupportForEachMentor() != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), supportService.listOfSupportForEachMentor()), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.NOT_FOUND_MESSAGES, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/supportDetail/{urlSupportId}")
    public ResponseEntity<Object> supportDetail(@PathVariable String urlSupportId) {
        try {
            Long supportId = Long.parseLong(urlSupportId);
            ViewSupportDetailDTO viewSupportDetail = supportService.viewSupportDetailForMentor(supportId);
            if (viewSupportDetail != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), viewSupportDetail), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy phiên gia sư phù hợp", HttpStatus.NOT_FOUND.value()), HttpStatus.OK);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/course-regis/{courseId}")
    public ResponseEntity<Object> regisCourse(@PathVariable String courseId, @RequestParam("registerCourse") String registerCourse, @RequestParam("files") List<MultipartFile> files, @RequestParam("titles") List<String> titles, @RequestParam("avatar") MultipartFile courseAvatar) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();

        if (files.isEmpty() || files.get(0).isEmpty()) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.NO_CONTENT.getReasonPhrase(), HttpStatus.NO_CONTENT.value(), "Chưa tải lên video"), HttpStatus.BAD_REQUEST);
        }
        if (files.size() != titles.size()) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), "Không có video cho tiêu đề đã chọn"), HttpStatus.BAD_REQUEST);
        }
        try {
            Long id = Long.parseLong(courseId);
            if (!courseService.checkExistCourse(id)) {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy khóa học ", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
            RegisterCourseDTO registerCourseDTO = objectMapper.readValue(registerCourse, RegisterCourseDTO.class);
            registerCourseDTO.setCourseId(id);
            List<String> errors = validator.validateObject(registerCourseDTO);
            if (!errors.isEmpty()) {
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }
            registerCourseDTO.setMentorName(customUserDetails.getUsername());
            RegisterCourseDTO registerCourseDTO1 = courseService.regisCourse(registerCourseDTO, titles, files, courseAvatar);
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.CREATED.getReasonPhrase(), HttpStatus.CREATED.value(), registerCourseDTO1), HttpStatus.CREATED);

        } catch (JsonParseException e) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.FORBIDDEN.getReasonPhrase(), HttpStatus.FORBIDDEN.value(), "Thông tin khóa học không hợp lệ"), HttpStatus.FORBIDDEN);

        }
    }

    @PutMapping("/listSupport/{id}")
    public ResponseEntity<Object> acceptOrRejectSupport(@PathVariable("id") String id, @RequestBody AcceptOrRejectSupportDTO acceptOrRejectSupportDTO) {
        try {
            Long supportId = Long.parseLong(id);
            AcceptOrRejectSupportDTO acceptOrRejectSupport = supportService.acceptOrRejectSupport(acceptOrRejectSupportDTO, supportId);
            if (acceptOrRejectSupport == null) {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy phiên gia sư phù hợp", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), acceptOrRejectSupport), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/course/{id}")
    public ResponseEntity<Object> editCourse(@PathVariable("id") String courseId, @RequestParam("course") String course) {
        try {
            Long id = Long.parseLong(courseId);
            EditCourseDTO editCourseDTO = objectMapper.readValue(course, EditCourseDTO.class);
            editCourseDTO.setCourseId(id);
            List<String> errors = validator.validateObject(editCourseDTO);
            if (!errors.isEmpty()) {
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }
            EditCourseDTO editCourseDTO1 = courseService.editCourse(editCourseDTO);
            if (editCourseDTO1 == null) {
                return new ResponseEntity<>(new BaseResponse<>("Khóa học không tồn tại", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);

            }
            return new ResponseEntity<>(new BaseResponse<>("Cập nhật khóa học thành công !", HttpStatus.OK.value(), editCourseDTO1), HttpStatus.OK);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Khóa học không đúng", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không đúng", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/support/{id}/confirm-start")
    public ResponseEntity<Object> confirmStart(@PathVariable("id") String id) {
        try {
            Long supportId = Long.parseLong(id);
            ConfirmStartSupportDTO confirmStartSupport = supportService.mentorConfirmStart(supportId);
            if (confirmStartSupport != null) {
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), confirmStartSupport), HttpStatus.OK);
            }
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.NOT_FOUND.getReasonPhrase(), HttpStatus.NOT_FOUND.value(), "Không tìm thấy phiên gia sư phù hợp"), HttpStatus.NOT_FOUND);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/profile")
    public ResponseEntity<Object> viewMentorProfile() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(customUserDetails.getUsername());
        MentorListDTO mentorListDTO = userService.getMentorDetailAndCertificatesByUserId(userDetailDTO.getUserId());
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), mentorListDTO), HttpStatus.OK);
    }

    @PutMapping("/profile/edit")
    public ResponseEntity<Object> editProfile(@RequestParam("avatar") MultipartFile avatar,
                                              @RequestParam("profile") String profile) {
        try {

            EditProfileMentorDTO editProfileMentor = objectMapper.readValue(profile, EditProfileMentorDTO.class);
            List<String> errors = new ArrayList<>();
            if (validator.editProfileMentor(editProfileMentor).size() != 0) {
                errors = validator.editProfileMentor(editProfileMentor);
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
            UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(customUserDetails.getUsername());
            if (!editProfileMentor.getPhone().equalsIgnoreCase(userDetailDTO.getPhone()) && validator.checkDuplicate(null, editProfileMentor.getPhone(), null).size() != 0) {
                errors = validator.checkDuplicate(null, editProfileMentor.getPhone(), null);
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }
            MentorListDTO mentorListDTO = userService.editProfileMentor(editProfileMentor, avatar, customUserDetails.getUsername());
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), mentorListDTO), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(new BaseResponse<>("Không thể cập nhật hồ sơ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @PutMapping("/profile/edit-certificate")
    public ResponseEntity<Object> editCertificate(@RequestParam("certificateFile") List<MultipartFile> certificateFile,
                                                  @RequestParam("certificateType") List<String> certificateType) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        MentorListDTO mentorListDTO = userService.editCertificate(certificateFile, certificateType, customUserDetails.getUsername());
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), mentorListDTO), HttpStatus.OK);
    }

    @GetMapping("/course-request/list")
    public ResponseEntity<Object> listCourseRequestByMentor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<CourseRequestDTO> list = courseRequestService.mentorCourseRequest(customUserDetails.getUsername());
        if (list != null) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), list), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>("Chưa có yêu cầu tạo khóa học nào!", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/filter-support")
    public ResponseEntity<Object> filterMentorShipRequest(@RequestParam(value = "status", required = false) List<String> status,
                                                          @RequestParam(value = "startDate", required = false) LocalDate startDate,
                                                          @RequestParam(value = "endDate", required = false) LocalDate endDate) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<SupportOfEachMentor> list = supportService.filterMentorshipRequestMentor(customUserDetails.getUsername(), status, startDate, endDate);
        if (!list.isEmpty()) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), list), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>("Không tim thấy yêu cầu hỗ trợ nào", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/course/{courseId}/video/{videoId}")
    public ResponseEntity<Object> deleteVideoCourse(@PathVariable("courseId") String course, @PathVariable("videoId") String video) {
        try {
            Long courseId = Long.parseLong(course);
            Long videoId = Long.parseLong(video);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
            boolean courseCreatedByUser = courseService.checkUserCreateCourse(customUserDetails.getUsername(), courseId);
            if (courseCreatedByUser) {
                boolean deleteExistedVideoCourse = videoService.deleteVideoInCourse(videoId, courseId);
                if (deleteExistedVideoCourse) {
                    return new ResponseEntity<>(new BaseResponse<>("Xóa video thành công!", HttpStatus.OK.value()), HttpStatus.OK);
                }
                return new ResponseEntity<>(new BaseResponse<>("Video không tồn tại!", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(new BaseResponse<>("Khóa học của gia sư không tồn tại!", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không đúng", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

}
