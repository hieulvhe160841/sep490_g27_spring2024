package com.g27.oms_be.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.g27.oms_be.common.CommonString;
import com.g27.oms_be.dto.courseRequest.CourseRequestDetailDTO;
import com.g27.oms_be.dto.courseRequest.ListCourseRequestForEducationStaffDTO;
import com.g27.oms_be.dto.mentorRequest.AcceptOrDenyRequestDTO;
import com.g27.oms_be.dto.mentorRequest.MentorRequestDTO;
import com.g27.oms_be.dto.response.BaseResponse;
import com.g27.oms_be.dto.support.SupportListDTO;
import com.g27.oms_be.dto.support.ViewSupportListForEachPostDTO;
import com.g27.oms_be.dto.user.common.EditProfileDTO;
import com.g27.oms_be.dto.user.common.UserDetailDTO;
import com.g27.oms_be.dto.user.educationStaff.MentorRequestDetailDTO;
import com.g27.oms_be.dto.userCertificate.UserCertificateDetailDTO;
import com.g27.oms_be.dto.userSkill.UserSkillDetailDTO;
import com.g27.oms_be.security.CustomUserDetails;
import com.g27.oms_be.service.course.CourseService;
import com.g27.oms_be.service.courseRequest.CourseRequestService;
import com.g27.oms_be.service.mentorRequest.MentorRequestService;
import com.g27.oms_be.service.support.SupportService;
import com.g27.oms_be.service.user.UserService;
import com.g27.oms_be.service.userCertificate.UserCertificateService;
import com.g27.oms_be.service.userSkill.UserSkillService;
import com.g27.oms_be.validate.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/educationStaff")
@CrossOrigin(origins = "*")
public class EducationStaffController {
    @Autowired
    private MentorRequestService mentorRequestService;
    @Autowired
    private UserCertificateService userCertificateService;
    @Autowired
    private UserSkillService userSkillService;
    @Autowired
    private CourseRequestService courseRequestService;
    @Autowired
    private Validator validator;
    @Autowired
    private SupportService supportService;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private CourseService courseService;

    @GetMapping("/mentorRequestList")
    public ResponseEntity<Object> getAllMentorRequest() {
        List<MentorRequestDTO> mentorRequestDTOList = mentorRequestService.listMentorRequest();
        if (mentorRequestDTOList != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), mentorRequestDTOList), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>(CommonString.NOT_FOUND_MESSAGES, HttpStatus.NOT_FOUND.value()), HttpStatus.OK);
    }

    @GetMapping("/mentorRequestDetail/userId/{userId}/requestId/{requestId}")
    public ResponseEntity<Object> getMentorRequestDetail(@PathVariable String userId, @PathVariable String requestId) {
        try {
            Long newUserId = Long.parseLong(userId);
            Long newRequestId = Long.parseLong(requestId);
            if (mentorRequestService.checkExistedMentorRequestByUserIdAndRequestId(newUserId, newRequestId)) {
                List<UserCertificateDetailDTO> userCertificateDetailDTOList = userCertificateService.userCertificateListByUserId(newUserId);
                List<UserSkillDetailDTO> userSkillDetailDTOList = userSkillService.userSkillListByUserId(newUserId);
                MentorRequestDTO mentorRequestDetail = mentorRequestService.mentorRequestDetail(newRequestId);
                MentorRequestDetailDTO mentorRequestDetailDTO = MentorRequestDetailDTO.builder()
                        .mentorRequestDetail(mentorRequestDetail)
                        .userSkillDetailDTOList(userSkillDetailDTOList)
                        .userCertificateDetailDTOList(userCertificateDetailDTOList)
                        .build();
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), mentorRequestDetailDTO), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.NOT_FOUND_MESSAGES, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("INVALID INPUT", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/approveMentorRequest/{urlRequestId}")
    public ResponseEntity<Object> approveMentorRequest(@RequestBody AcceptOrDenyRequestDTO acceptOrDenyRequestDTO, @PathVariable String urlRequestId) {
        try {
            Long requestId = Long.parseLong(urlRequestId);
            if (mentorRequestService.mentorRequestDetail(requestId).getMentorRequestStatus().equalsIgnoreCase(CommonString.ACCEPTED_STRING) && acceptOrDenyRequestDTO.getStatus().equalsIgnoreCase(CommonString.ACCEPTED_STRING)) {
                return new ResponseEntity<>(new BaseResponse<>("REQUEST IS ALREADY ACCEPTED", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
            }
            if (mentorRequestService.mentorRequestDetail(requestId).getMentorRequestStatus().equalsIgnoreCase(CommonString.REJECTED_STRING) && acceptOrDenyRequestDTO.getStatus().equalsIgnoreCase(CommonString.REJECTED_STRING)) {
                return new ResponseEntity<>(new BaseResponse<>("REQUEST IS ALREADY REJECTED", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
            }
            AcceptOrDenyRequestDTO acceptOrDenyMentorRequest = mentorRequestService.acceptOrDenyMentorRequest(acceptOrDenyRequestDTO, requestId);
            if (acceptOrDenyMentorRequest != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), acceptOrDenyMentorRequest), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("CAN NOT UPDATE MENTOR REQUEST", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("INVALID INPUT", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/courseRequestList")
    public ResponseEntity<Object> getCourseRequestListForEducationStaff() {
        List<ListCourseRequestForEducationStaffDTO> listCourseRequestForEducationStaff = courseRequestService.listCourseForEducationStaff();
        return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), listCourseRequestForEducationStaff), HttpStatus.OK);
    }

    @GetMapping("/viewCourseRequest/{urlCourseRequestId}")
    public ResponseEntity<Object> viewCourseRequestDettail(@PathVariable String urlCourseRequestId) {
        try {
            Long courseRequestId = Long.parseLong(urlCourseRequestId);
            CourseRequestDetailDTO courseRequestDetailDTO = courseRequestService.viewCourseRequestDetailForEducationStaff(courseRequestId);
            if (courseRequestDetailDTO != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), courseRequestDetailDTO), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.NOT_FOUND_MESSAGES, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (IOException e) {
            return new ResponseEntity<>(new BaseResponse<>("CAN NOT LOAD COURSE REQUEST", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("INVALID INPUT", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/approveCourseRequest/{urlRequestId}")
    public ResponseEntity<Object> approveCourseRequest(@RequestBody AcceptOrDenyRequestDTO acceptOrDenyRequestDTO, @PathVariable String urlRequestId) {
        try {
            Long requestId = Long.parseLong(urlRequestId);
            if (courseRequestService.viewCourseRequestDetailForEducationStaff(requestId).getCourseRequestStatus().equalsIgnoreCase(CommonString.ACCEPTED_STRING) && acceptOrDenyRequestDTO.getStatus().equalsIgnoreCase(CommonString.ACCEPTED_STRING)) {
                return new ResponseEntity<>(new BaseResponse<>("REQUEST IS ALREADY ACCEPTED", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
            }
            if (courseRequestService.viewCourseRequestDetailForEducationStaff(requestId).getCourseRequestStatus().equalsIgnoreCase(CommonString.REJECTED_STRING) && acceptOrDenyRequestDTO.getStatus().equalsIgnoreCase(CommonString.REJECTED_STRING)) {
                return new ResponseEntity<>(new BaseResponse<>("REQUEST IS ALREADY REJECTED", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
            }
            AcceptOrDenyRequestDTO acceptOrDenyMentorRequest = courseRequestService.acceptOrDenyCourseRequest(acceptOrDenyRequestDTO, requestId);
            if (acceptOrDenyMentorRequest != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), acceptOrDenyMentorRequest), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("CAN NOT UPDATE COURSE REQUEST", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (
                NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("INVALID INPUT", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        } catch (
                IOException e) {
            return new ResponseEntity<>(new BaseResponse<>("CAN NOT LOAD COURSE REQUEST", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/viewSupportList/{urlPostId}")
    public ResponseEntity<Object> viewAllSupportForEachPost(@PathVariable String urlPostId) {
        try {
            Long postId = Long.parseLong(urlPostId);
            List<ViewSupportListForEachPostDTO> supportList = supportService.viewAllSupportListByPostId(postId);
            if (supportList.size() != 0) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), supportList), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.NOT_FOUND_MESSAGES, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.INVALID_INPUT, HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/profile/edit")
    public ResponseEntity<Object> editProfile(@RequestParam("avatar") MultipartFile avatar,
                                              @RequestParam("profile") String profile) {
        try {
            EditProfileDTO editProfileDTO = objectMapper.readValue(profile, EditProfileDTO.class);
            List<String> errors = new ArrayList<>();
            if (validator.editProfile(editProfileDTO).size() != 0) {
                errors = validator.editProfile(editProfileDTO);
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }
            if (validator.checkDuplicate(null, editProfileDTO.getPhone(), null).size() != 0) {
                errors = validator.checkDuplicate(null, editProfileDTO.getPhone(), null);
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
            UserDetailDTO userDetailDTO = userService.editProfile(editProfileDTO, avatar, customUserDetails.getUsername());
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), userDetailDTO), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/profile")
    public ResponseEntity<Object> menteeDetail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(customUserDetails.getUsername());
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), userDetailDTO), HttpStatus.OK);
    }

    @GetMapping("/supportList")
    public ResponseEntity<Object> supportList() {
        List<SupportListDTO> supportListDTOList = supportService.viewSupportList();
        if (supportListDTOList != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), supportListDTOList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.NOT_FOUND_MESSAGES, HttpStatus.NOT_FOUND.value()), HttpStatus.OK);
        }
    }

    @GetMapping("/support-list")
    public ResponseEntity<Object> filterSupport(@RequestParam(value = "status", required = false) List<String> status,
                                                @RequestParam(value = "startDate", required = false) LocalDate startDate,
                                                @RequestParam(value = "endDate", required = false) LocalDate endDate
    ) {

        List<SupportListDTO> supportListDTOList = supportService.filterSupport(status, startDate, endDate);
        if (supportListDTOList.isEmpty()) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.NOT_FOUND.getReasonPhrase(), HttpStatus.NOT_FOUND.value(), "Không tìm thấy !"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), supportListDTOList), HttpStatus.OK);
    }
}
