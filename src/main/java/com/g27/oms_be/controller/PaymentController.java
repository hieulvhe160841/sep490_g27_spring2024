package com.g27.oms_be.controller;

import com.g27.oms_be.common.CommonString;
import com.g27.oms_be.dto.denomination.DenominationDTO;
import com.g27.oms_be.dto.response.BaseResponse;
import com.g27.oms_be.payment.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/payment")
@CrossOrigin(origins = "*")
public class PaymentController {

    @Autowired
    Payment payment;

    @PostMapping("/create_payment")
    public ResponseEntity<Object> createPayment(@RequestBody DenominationDTO denominationDTO) {
        Object paymentResponse = payment.createPayment(denominationDTO);
        if (paymentResponse != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.CREATE_SUCCESSFULLY, HttpStatus.CREATED.value(), paymentResponse), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.CREATE_FAIL, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
