package com.g27.oms_be.controller;

import com.g27.oms_be.common.TransactionStatus;
import com.g27.oms_be.dto.denominationTransaction.DenominationTransactionDTO;
import com.g27.oms_be.dto.transaction.TransactionDTO;
import com.g27.oms_be.dto.userWallet.UserWalletDTO;
import com.g27.oms_be.entities.DenominationTransaction;
import com.g27.oms_be.entities.Transaction;
import com.g27.oms_be.repository.TransactionRepository;
import com.g27.oms_be.service.denominationTransaction.DenominationTransactionService;
import com.g27.oms_be.service.userWallet.UserWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/payment")
@CrossOrigin(origins = "*")
public class PaymentRequiredURLController {
    @Autowired
    private DenominationTransactionService denominationTransactionService;
    @Autowired
    private UserWalletService userWalletService;
    @Autowired
    private TransactionRepository transactionRepository;

    @GetMapping("/return_url")
    public String returnUrl(@RequestParam(name = "token") String token, @RequestParam(name = "PayerID") String payerId, Model model) {
        DenominationTransactionDTO denominationTransactionDTO = denominationTransactionService.getDenominationTransactionByOrderId(token);
        if (denominationTransactionDTO != null) {
            UserWalletDTO userWalletDTO = userWalletService.updateBalanceForUserWalletAfterDenomination(denominationTransactionDTO.getUserId(), denominationTransactionDTO.getDenominationId(), denominationTransactionDTO.getDenominationTransactionId());
            if (userWalletDTO != null) {
                Transaction transaction = transactionRepository.getReferenceById(denominationTransactionDTO.getTransactionId());
                transaction.setTransactionStatus(TransactionStatus.DONE.name());
                transactionRepository.save(transaction);
                model.addAttribute("userWalletDTO", userWalletDTO);
                return "return";
            }
        }
        return "notification";
    }

    @GetMapping("/cancel_url")
    public String cancelUrl(@RequestParam(name = "token") String token, Model model) {
        DenominationTransactionDTO denominationTransactionDTO = denominationTransactionService.getDenominationTransactionByOrderId(token);
        if (denominationTransactionDTO != null) {
            Transaction transaction = transactionRepository.getReferenceById(denominationTransactionDTO.getTransactionId());
            transaction.setTransactionStatus(TransactionStatus.CANCELED.name());
            transactionRepository.save(transaction);
            return "cancel";
        }
        return "notification";
    }
}
