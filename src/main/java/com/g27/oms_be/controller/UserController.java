package com.g27.oms_be.controller;

import com.g27.oms_be.common.CommonString;
import com.g27.oms_be.dto.certificate.CertificateDTO;
import com.g27.oms_be.dto.course.CourseDTO;
import com.g27.oms_be.dto.course.CourseDetailDTO;
import com.g27.oms_be.dto.response.BaseResponse;
import com.g27.oms_be.dto.skill.SkillDTO;
import com.g27.oms_be.dto.user.common.UserDetailDTO;
import com.g27.oms_be.dto.user.mentor.MentorDetailDTO;
import com.g27.oms_be.dto.user.mentor.MentorListDTO;
import com.g27.oms_be.dto.userWallet.UserViewTheirWalletDTO;
import com.g27.oms_be.service.certificate.CertificateService;
import com.g27.oms_be.service.course.CourseService;
import com.g27.oms_be.service.firebase.FirebaseService;
import com.g27.oms_be.service.skill.SkillService;
import com.g27.oms_be.service.support.SupportService;
import com.g27.oms_be.service.user.UserService;
import com.g27.oms_be.service.userWallet.UserWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private SkillService skillService;
    @Autowired
    private FirebaseService firebaseService;
    @Autowired
    private SupportService supportService;
    @Autowired
    private CertificateService certificateService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private UserWalletService userWalletService;

    @GetMapping("/profile/{username}")
    public ResponseEntity<Object> getUserDetailByUsername(@PathVariable String username) {
        UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(username);
        if (userDetailDTO != null) {
            return new ResponseEntity<>(new BaseResponse<>("OK", 200, userDetailDTO), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse("Không tìm thấy người dùng !", 404), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/skillList")
    public ResponseEntity<Object> getAllSkill() {
        List<SkillDTO> skillDTOList = skillService.getAllSkill();
        return new ResponseEntity<>(new BaseResponse("OK", HttpStatus.OK.value(), skillDTOList), HttpStatus.OK);
    }

    @GetMapping("/top-rate-mentor")
    public ResponseEntity<Object> getTopRateMentor() {
        List<MentorDetailDTO> mentorDetailDTOS = userService.getTopRateMentor();
        return new ResponseEntity<>(new BaseResponse(HttpStatus.OK.toString(), HttpStatus.OK.value(), mentorDetailDTOS), HttpStatus.OK);
    }

    @GetMapping("/list-mentor")
    public ResponseEntity<Object> getAllMentor() {
        List<MentorListDTO> mentorListDTOS = userService.getAllMentorWithSkillAndCertificate();
        return new ResponseEntity<>(new BaseResponse(HttpStatus.OK.toString(), HttpStatus.OK.value(), mentorListDTOS), HttpStatus.OK);
    }
    
    private MediaType getMediaTypeForFileName(String fileName) {
        Map<String, MediaType> mediaTypeMap = new HashMap<>();
        mediaTypeMap.put("jpg", MediaType.IMAGE_JPEG);
        mediaTypeMap.put("jpeg", MediaType.IMAGE_JPEG);
        mediaTypeMap.put("png", MediaType.IMAGE_PNG);
        mediaTypeMap.put("gif", MediaType.IMAGE_GIF);
        mediaTypeMap.put("bmp", MediaType.IMAGE_JPEG);
        mediaTypeMap.put("mp4", MediaType.valueOf("video/mp4"));
        mediaTypeMap.put("avi", MediaType.valueOf("video/x-msvideo"));
        mediaTypeMap.put("pdf", MediaType.APPLICATION_PDF);

        String extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

        return mediaTypeMap.getOrDefault(extension, MediaType.APPLICATION_OCTET_STREAM);
    }

    @GetMapping("/view/{fileName}")
    public ResponseEntity<Object> viewFile(@PathVariable("fileName") String fileName) throws IOException {
        InputStream fileInputStream = firebaseService.view(fileName);
        System.out.println(fileName);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(getMediaTypeForFileName(fileName))
                .body(new InputStreamResource(fileInputStream));
    }

    @GetMapping("/list-mentor/{userName}")
    public ResponseEntity<Object> mentorDetail(@PathVariable("userName") String userName) {
        MentorListDTO mentorListDTO = userService.getMentorDetailAndCertificates(userName);
        if (mentorListDTO != null) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), mentorListDTO), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy  gia sư !", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/available-skill")
    public ResponseEntity<Object> viewAvailableSkills() {
        List<SkillDTO> skillDTOS = skillService.availableSKills();
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), skillDTOS), HttpStatus.OK);
    }

    @GetMapping("/certificate")
    public ResponseEntity<Object> viewCertificates() {
        List<CertificateDTO> certificateDTOS = certificateService.getAllCertificates();
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), certificateDTOS), HttpStatus.OK);
    }

    @GetMapping("/course/list/{userName}")
    public ResponseEntity<Object> courseByMentor(@PathVariable("userName") String username) {
        UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(username);
        if (userDetailDTO == null) {
            return new ResponseEntity<>(new BaseResponse<>("Người dùng không tồn tại!", HttpStatus.BAD_REQUEST.value(), CommonString.INVALID_INPUT), HttpStatus.BAD_REQUEST);
        }
        List<CourseDTO> courseDTOS = courseService.getCoursesOfMentor(username);
        if (courseDTOS == null) {
            return new ResponseEntity<>(new BaseResponse<>("Gia sư này không có khóa học nào!", HttpStatus.NOT_FOUND.value(), "Mentor has no course!"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), courseDTOS), HttpStatus.OK);
    }

    @GetMapping("/user-wallet")
    public ResponseEntity<Object> viewUserWallet() {
        UserViewTheirWalletDTO userViewTheirWalletDTO = userWalletService.menteeViewTheirWallet();
        if (userViewTheirWalletDTO != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), userViewTheirWalletDTO), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy ! ", HttpStatus.NOT_FOUND.value()), HttpStatus.OK);
    }

    @GetMapping("/course/{id}")
    public ResponseEntity<Object> courseDetail(@PathVariable("id") String courseId) {
        try {
            Long id = Long.parseLong(courseId);
            CourseDetailDTO courseDetailDTO = courseService.getCourseDetail(id);
            if (courseDetailDTO != null) {
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), courseDetailDTO), HttpStatus.OK);
            }
            return new ResponseEntity<>(new BaseResponse<>("Khóa học không tồn tại", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        } catch (NumberFormatException n) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/courses")
    public ResponseEntity<Object> getLatestCourses() {
        List<CourseDTO> latestCourses = courseService.latestCourses();
        if (latestCourses != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), latestCourses), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse(" Chưa có khóa học nào mới! ", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/filter-courses")
    public ResponseEntity<Object> filterSearchCourse(@RequestParam(value = "mentorName", required = false) String mentorName,
                                                     @RequestParam(value = "courseName", required = false) String courseName,
                                                     @RequestParam(value = "fromPrice", required = false) String fPrice,
                                                     @RequestParam(value = "toPrice", required = false) String tPrice) {
        try {
            List<CourseDTO> courseDTOS = new ArrayList<>();
            Double fromPrice = null;
            Double toPrice = null;
            if (!fPrice.isEmpty()) {
                fromPrice = Double.parseDouble(fPrice);
            }
            if (!tPrice.isEmpty()) {
                toPrice = Double.parseDouble(tPrice);
            }
            courseDTOS = courseService.filterCourse(mentorName, courseName, fromPrice, toPrice);
            if (courseDTOS.isEmpty()) {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy khóa học !", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), courseDTOS), HttpStatus.OK);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không hợp lệ !", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }
}
