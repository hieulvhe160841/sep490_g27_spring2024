package com.g27.oms_be.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.g27.oms_be.common.CommonString;
import com.g27.oms_be.dto.course.CourseDTO;
import com.g27.oms_be.dto.denomination.DenominationDTO;
import com.g27.oms_be.dto.denominationTransaction.MenteeViewPersonalDenominationTransactionDTO;
import com.g27.oms_be.dto.mentorRequest.MenteeViewOwnRegisAsMentorDTO;
import com.g27.oms_be.dto.mentorRequest.MentorRequestDetailForMenteeDTO;
import com.g27.oms_be.dto.post.PostDTO;
import com.g27.oms_be.dto.post.UpdatePostDTO;
import com.g27.oms_be.dto.post.ViewPostDetailForMenteeDTO;
import com.g27.oms_be.dto.response.BaseResponse;
import com.g27.oms_be.dto.support.*;
import com.g27.oms_be.dto.user.common.BookMentorDTO;
import com.g27.oms_be.dto.user.common.EditProfileDTO;
import com.g27.oms_be.dto.user.common.UserDetailDTO;
import com.g27.oms_be.dto.user.mentor.*;
import com.g27.oms_be.security.CustomUserDetails;
import com.g27.oms_be.service.course.CourseService;
import com.g27.oms_be.service.courseRegisration.CourseRegistrationService;
import com.g27.oms_be.service.denomination.DenominationService;
import com.g27.oms_be.service.denominationTransaction.DenominationTransactionService;
import com.g27.oms_be.service.mentorRequest.MentorRequestService;
import com.g27.oms_be.service.post.PostService;
import com.g27.oms_be.service.post.PostServiceImpl;
import com.g27.oms_be.service.support.SupportService;
import com.g27.oms_be.service.user.UserService;
import com.g27.oms_be.service.userCertificate.UserCertificateService;
import com.g27.oms_be.service.userSkill.UserSkillService;
import com.g27.oms_be.service.userWallet.UserWalletService;
import com.g27.oms_be.validate.Validator;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/mentee")
@CrossOrigin(origins = "*")
public class MenteeController {
    @Autowired
    private PostService postService;
    @Autowired
    private SupportService supportService;
    @Autowired
    Validator validator;
    @Autowired
    private CourseService courseService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserSkillService userSkillService;
    @Autowired
    private UserCertificateService userCertificateService;
    @Autowired
    private MentorRequestService mentorRequestService;
    @Autowired
    private DenominationService denominationService;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private DenominationTransactionService denominationTransactionService;
    @Autowired
    private UserWalletService userWalletService;
    @Autowired
    private CourseRegistrationService courseRegistrationService;

    @PostMapping("/createPost")
    public ResponseEntity<Object> createPost(@Valid @RequestBody PostDTO postDTO, BindingResult bindingResult) {
        BaseResponse baseResponse = new BaseResponse<>();
        List<String> errors = new ArrayList<>();
        if (bindingResult.hasErrors()) {
            baseResponse = validator.checkBindingResult(bindingResult, errors);
            return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
        } else {
            if (Double.parseDouble(postDTO.getPostPrice()) > userWalletService.menteeViewTheirWallet().getBalance()) {
                baseResponse.setMessages("Tạo bài đăng thất bại");
                baseResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
                baseResponse.setData("Số dư của bạn không đủ");
                return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
            }
            PostDTO post = postService.addPost(postDTO);
            if (postService.checkPostExisted(post.getPostId())) {
                baseResponse.setMessages("Tạo bài đăng thành công");
                baseResponse.setStatusCode(HttpStatus.CREATED.value());
                baseResponse.setData(post);
                return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
            } else {
                baseResponse.setMessages("Tạo bài đăng thất bại");
                baseResponse.setStatusCode(HttpStatus.NOT_FOUND.value());
                baseResponse.setData(post);
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }
        }
    }

    @GetMapping("/list-book-mentor")
    public ResponseEntity<Object> listBookMentor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<BookMentorDTO> bookMentorDTOS = supportService.getListBookedMentor(customUserDetails.getUsername());
        if (bookMentorDTOS == null) {
            return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy gia sư đã đặt! ", HttpStatus.NOT_FOUND.value(), "You don't have any booked mentors!"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.toString(), HttpStatus.OK.value(), bookMentorDTOS), HttpStatus.OK);
    }

    @GetMapping("/postListByMentee")
    public ResponseEntity<Object> postListByMentee() {
        List<PostDTO> postDTOList = postService.getAllPostByMenteeUserName();
        if (postDTOList.size() != 0) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), postDTOList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy bài đăng !", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/deletePost/{urlPostId}")
    public ResponseEntity<Object> deletePostByPostId(@PathVariable String urlPostId) {
        try {
            Long postId = Long.parseLong(urlPostId);
            if (postService.checkUserCreatedPostOrNot(postId)) {
                boolean postDeletedExisted = postService.deletePostByPostId(postId);
                if (!postDeletedExisted) {
                    return new ResponseEntity<>(new BaseResponse<>("Xóa bài đăng thành công !", HttpStatus.OK.value()), HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(new BaseResponse<>("Xóa bài đăng thất bại !", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không thể xóa do không tìm thấy bài đăng phù hợp !", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không hợp lệ !", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/updatePost/{urlPostId}")
    public ResponseEntity<Object> updatePostByPostId(@PathVariable String urlPostId, @Valid @RequestBody UpdatePostDTO updatePostDTO, BindingResult bindingResult) {
        try {
            Long postId = Long.parseLong(urlPostId);
            BaseResponse<Object> response = new BaseResponse<>();
            List<String> errors = new ArrayList<>();
            if (bindingResult.hasErrors()) {
                response = validator.checkBindingResult(bindingResult, errors);
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                if (Double.parseDouble(updatePostDTO.getPostPrice()) > userWalletService.menteeViewTheirWallet().getBalance()) {
                    response.setMessages(CommonString.CREATE_FAIL);
                    response.setStatusCode(HttpStatus.BAD_REQUEST.value());
                    response.setData("Số dư của bạn không đủ");
                    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
                }
                if (postService.checkUserCreatedPostOrNot(postId)) {
                    UpdatePostDTO updatePost = postService.updatePostForMenteeByPostId(postId, updatePostDTO);
                    response.setMessages("Cập nhật thành công");
                    response.setStatusCode(HttpStatus.OK.value());
                    response.setData(updatePost);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(new BaseResponse<>("Bài đăng không thể cập nhật", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
                }
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/register-as-mentor-skill")
    public ResponseEntity<Object> registerAsMentorSkill(@Valid @RequestBody MentorSkillDTO mentorSkillDTO, BindingResult bindingResult) {
        List<String> errors = new ArrayList<>();
        if (bindingResult.hasErrors()) {
            errors = validator.responseData(bindingResult);
            return new ResponseEntity<>(new BaseResponse(HttpStatus.BAD_REQUEST.toString(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
        }
        if (mentorRequestService.checkExistWaitingMentorRequest(mentorSkillDTO.getUserName())) {
            return new ResponseEntity<>(new BaseResponse("Bạn đang có 1 yêu cầu với gia sư này !", HttpStatus.BAD_REQUEST.value(), "You already have a waiting regis as mentor request!"), HttpStatus.BAD_REQUEST);
        }
        MentorSkillDTO mentorSkillDTO1 = userSkillService.registerAsMentor(mentorSkillDTO);
        return new ResponseEntity<>(new BaseResponse("Tạo yêu cầu gia sư thành công!", HttpStatus.CREATED.value(), mentorSkillDTO1), HttpStatus.CREATED);
    }

    @PostMapping("/register-as-mentor-certificate")
    public ResponseEntity<Object> registerAsMentorCertificate(@RequestParam("cert") List<String> cert, @RequestParam("file") List<MultipartFile> files) throws IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<String> errors = new ArrayList<>();
        if (cert.size() == 0) {
            errors.add("Chọn ít nhất 1 loại chứng chỉ !");
        }
        if (files.get(0).isEmpty()) {
            errors.add("Không có tệp chứng chỉ nào được tải lên !");
        } else if (files.size() != cert.size()) {
            errors.add("Phải tải lên đủ chứng chỉ!");
        }
        if (!errors.isEmpty()) {
            return new ResponseEntity<>(new BaseResponse(HttpStatus.BAD_REQUEST.toString(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
        }
        MentorCertificateDTO mentorCertificateDTO = userCertificateService.addCertificate(cert, files, customUserDetails.getUsername());
        return new ResponseEntity<>(new BaseResponse("Tạo yêu cầu nộp chứng chỉ thành công !", HttpStatus.CREATED.value(), mentorCertificateDTO), HttpStatus.CREATED);
    }

    @GetMapping("/viewMentorRequest/{urlMentorRequestId}")
    public ResponseEntity<Object> viewMentorRequestDetail(@PathVariable String urlMentorRequestId) {
        try {
            Long mentorRequestId = Long.parseLong(urlMentorRequestId);
            MentorRequestDetailForMenteeDTO mentorRequestDetailForMenteeDTO = mentorRequestService.viewMentorRequestDetailForMentee(mentorRequestId);
            if (mentorRequestDetailForMenteeDTO != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), mentorRequestDetailForMenteeDTO), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy yêu cầu !", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không hợp lệ !", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/viewPostDetail/{urlPostId}")
    public ResponseEntity<Object> viewPostDetailForMentee(@PathVariable String urlPostId) {
        try {
            Long postId = Long.parseLong(urlPostId);
            ViewPostDetailForMenteeDTO viewPostDetailForMenteeDTO = postService.viewPostDetailForMenteeDTO(postId);
            if (viewPostDetailForMenteeDTO != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), viewPostDetailForMenteeDTO), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy thông tin bài đăng!", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không hợp lệ! ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/viewSupportList/{urlPostId}")
    public ResponseEntity<Object> viewAllSupportForEachPost(@PathVariable String urlPostId) {
        try {
            Long postId = Long.parseLong(urlPostId);
            List<ViewSupportListForEachPostDTO> supportList = supportService.viewAllSupportListByPostId(postId);
            if (supportList.size() != 0) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), supportList), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy hỗ trợ bài đăng !", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không hợp lệ !", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/list-mentor/{id}")
    public ResponseEntity<Object> mentorDetail(@PathVariable("id") String urlId) {
        try {
            Long id = Long.parseLong(urlId);
            MentorSupportDTO mentorSupportDTO = supportService.mentorDetail(id);
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), mentorSupportDTO), HttpStatus.OK);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu đầu vào không hợp lệ", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/denominationTransaction")
    public ResponseEntity<Object> viewAllDenominationForTransaction() {
        List<DenominationDTO> denominationDTOList = denominationService.getAlDenominationList();
        if (denominationDTOList != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), denominationDTOList), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>(CommonString.NOT_FOUND_MESSAGES, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @PostMapping("/request/{mentorName}")
    public ResponseEntity<Object> createMentorShipRequestWithoutPost(@PathVariable("mentorName") String mentorName, @RequestParam("duration") String duration) {
        if (userService.findByUserName(mentorName) == null) {
            return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy gia sư!", HttpStatus.BAD_REQUEST.value(), "Không tìm thấy gia sư!"), HttpStatus.BAD_REQUEST);
        }
        try {
            Double timeDuration = Double.parseDouble(duration);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
            if (supportService.checkWhenASupportInProgress(mentorName, customUserDetails.getUsername())) {
                return new ResponseEntity<>(new BaseResponse<>("Bạn vẫn đang có phiên hỗ trợ với gia sư này!", HttpStatus.BAD_REQUEST.value(), "Bạn vẫn đang có phiên hỗ trợ với gia sư này!"), HttpStatus.BAD_REQUEST);
            }
            MentorListDTO mentorListDTO = userService.getMentorDetailAndCertificates(mentorName);
            if (userWalletService.getByUserName(customUserDetails.getUsername()).getBalance() <
                    (mentorListDTO.getMentorDetail().getCostPerHour() * timeDuration * 1.0)) {
                return new ResponseEntity<>(new BaseResponse<>("Số dư của bạn không đủ !", HttpStatus.BAD_REQUEST.value(), "Số dư của bạn không đủ !"), HttpStatus.BAD_REQUEST);
            }
            MentorshipRequestDTO mentorshipRequestDTO = new MentorshipRequestDTO();
            mentorshipRequestDTO.setMentee(customUserDetails.getUsername());
            mentorshipRequestDTO.setMentor(mentorName);
            mentorshipRequestDTO.setDuration(timeDuration);
            MentorshipRequestDTO mentorshipRequestDTO1 = supportService.createSupportByMentee(mentorshipRequestDTO);

            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.CREATED.getReasonPhrase(), HttpStatus.CREATED.value(), mentorshipRequestDTO1), HttpStatus.CREATED);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value(), CommonString.INVALID_INPUT), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/support/{id}")
    public ResponseEntity<Object> acceptOrRejectSupportOfMentor(@PathVariable String id, @RequestBody AcceptOrRejectSupportDTO acceptOrRejectSupportDTO) {
        try {
            Long supportId = Long.parseLong(id);
            AcceptOrRejectSupportDTO acceptOrRejectSupport = supportService.acceptOrRejectSupport(acceptOrRejectSupportDTO, supportId);
            if (acceptOrRejectSupport == null) {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy yêu cầu !", HttpStatus.NOT_FOUND.value(), CommonString.NOT_FOUND_MESSAGES), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(new BaseResponse<>("Gửi yêu cầu chấp nhận/bác bỏ thành công!", HttpStatus.CREATED.value(), acceptOrRejectSupport), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin không hợp lệ ", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/support/{id}/confirm-start")
    public ResponseEntity<Object> confirmStart(@PathVariable("id") String id) {
        try {
            Long supportId = Long.parseLong(id);

            boolean check = supportService.check_Balance_When_Mentee_Start_Session(supportId);
            if (check == true) {
                ConfirmStartSupportDTO confirmStartSupport = supportService.menteeConfirmStart(supportId);
                if (confirmStartSupport != null) {
                    return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), confirmStartSupport), HttpStatus.OK);
                }
                return new ResponseEntity<>(new BaseResponse<>("Phiên trợ giúp không tồn tại", HttpStatus.NOT_FOUND.value(), "Phiên trợ giúp không tồn tại"), HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Số dư hiện tại của bạn không đủ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu đầu vào không hợp lệ", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/support/{id}/confirm-end")
    public ResponseEntity<Object> confirmEnd(@PathVariable("id") String id) {
        try {
            Long supportId = Long.parseLong(id);
            ConfirmEndSupportDTO confirmEndSupport = supportService.menteeConfirmEnd(supportId);
            if (confirmEndSupport != null) {
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), confirmEndSupport), HttpStatus.OK);
            }
            return new ResponseEntity<>(new BaseResponse<>("Phiên trợ giúp không tồn tại", HttpStatus.NOT_FOUND.value(), "Phiên trợ giúp không tồn tại"), HttpStatus.NOT_FOUND);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu đầu vào không hợp lệ", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/transaction-list")
    public ResponseEntity<Object> personalDenominationTransactionList() {
        List<MenteeViewPersonalDenominationTransactionDTO> denominationTransactionDTOList = denominationTransactionService.getAllListOfDenominationTransactionHaveMade();
        if (denominationTransactionDTOList != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), denominationTransactionDTOList), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>(CommonString.NOT_FOUND_MESSAGES, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @PutMapping("/rating")
    public ResponseEntity<Object> ratingForEachSupport(@RequestParam String supportId, @RequestBody RatingSupportDTO ratingSupportDTO) {
        try {
            Long parsedSupportId = Long.parseLong(supportId);
            boolean checkResult = supportService.ratingForSupportBySupportId(parsedSupportId, ratingSupportDTO.getRating(), ratingSupportDTO.getFeedback());
            if (checkResult == true) {
                return new ResponseEntity<>(new BaseResponse<>("Tạo đánh giá thành công", HttpStatus.CREATED.value(), ratingSupportDTO), HttpStatus.CREATED);
            }
            return new ResponseEntity<>(new BaseResponse<>("Tạo đánh giá thất bại do không tìm thấy phiên trợ giúp phù hợp", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Thông tin đầu vào không hợp lệ", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/profile/edit")
    public ResponseEntity<Object> editProfile(@RequestParam("avatar") MultipartFile avatar,
                                              @RequestParam("profile") String profile) {
        try {
            EditProfileDTO editProfileDTO = objectMapper.readValue(profile, EditProfileDTO.class);
            List<String> errors = new ArrayList<>();
            if (validator.editProfile(editProfileDTO).size() != 0) {
                errors = validator.editProfile(editProfileDTO);
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
            UserDetailDTO userDetailDTO = userService.editProfile(editProfileDTO, avatar, customUserDetails.getUsername());

            if (!editProfileDTO.getPhone().equalsIgnoreCase(userDetailDTO.getPhone()) && validator.checkDuplicate(null, editProfileDTO.getPhone(), null).size() != 0) {
                errors = validator.checkDuplicate(null, editProfileDTO.getPhone(), null);
                System.out.println(errors);
                return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(), errors), HttpStatus.BAD_REQUEST);
            }

            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), userDetailDTO), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/profile")
    public ResponseEntity<Object> menteeDetail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        UserDetailDTO userDetailDTO = userService.getUserDetailByUserName(customUserDetails.getUsername());
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), userDetailDTO), HttpStatus.OK);
    }

    @GetMapping("/support")
    public ResponseEntity<Object> listMentorshipRequest() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<SupportOfEachMentee> list = supportService.viewMentorShipRequest(customUserDetails.getUsername());
        if (list != null) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), list), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>("Không tim thấy yêu cầu hỗ trợ nào", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/support/{urlSupportId}")
    public ResponseEntity<Object> supportDetail(@PathVariable String urlSupportId) {
        try {
            Long supportId = Long.parseLong(urlSupportId);
            ViewSupportDetailDTO viewSupportDetail = supportService.viewSupportDetail(supportId);
            if (viewSupportDetail != null) {
                return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), viewSupportDetail), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy thông tin yêu cầu hỗ trợ !", HttpStatus.NOT_FOUND.value()), HttpStatus.OK);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không hợp lệ !", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/regis-as-mentor/list")
    public ResponseEntity<Object> menteeViewOwnRegisAsMentorRequests() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<MenteeViewOwnRegisAsMentorDTO> list = mentorRequestService.getMentorRequestOfMentee(customUserDetails.getUsername());
        if (list == null) {
            return new ResponseEntity<>(new BaseResponse<>("Không tìm thấy yêu cầu đăng kí làm gia sư nào!", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), list), HttpStatus.OK);
    }

    @PutMapping("/regis-as-mentor/confirm")
    public ResponseEntity<Object> menteeConfirmRegisAsMentor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        String message = mentorRequestService.updateRole(customUserDetails.getUsername());
        return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), message), HttpStatus.OK);
    }

    @GetMapping("/suggest-mentor")
    public ResponseEntity<Object> getSuggestMentor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<MentorDetailDTO> mentorDetailDTOS = supportService.suggestMentor(customUserDetails.getUsername());
        if (mentorDetailDTOS.size() == 0) {
            return new ResponseEntity<>(new BaseResponse("Chưa tìm thấy gia sư phù hợp ", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new BaseResponse(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), mentorDetailDTOS), HttpStatus.OK);
    }

    @GetMapping("/suggest-course")
    public ResponseEntity<Object> getSuggestCourse() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<CourseDTO> mentorDetailDTOS = courseService.suggestCourse(customUserDetails.getUsername());
        if (mentorDetailDTOS.size() == 0) {
            return new ResponseEntity<>(new BaseResponse("Hiện tại chưa tìm thấy khóa học nào phù hợp với bạn", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new BaseResponse(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), mentorDetailDTOS), HttpStatus.OK);
    }

    @PostMapping("/purchase-course/{urlCourseId}")
    public ResponseEntity<Object> menteePurchaseCourse(@PathVariable("urlCourseId") String urlCourseId) {
        try {
            Long courseId = Long.parseLong(urlCourseId);
            String message = courseRegistrationService.menteePurchaseCourse(courseId);
            return new ResponseEntity<>(new BaseResponse<>(message, HttpStatus.OK.value()), HttpStatus.OK);
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(new BaseResponse<>("Dữ liệu không hợp lệ !", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/list-course-purchased")
    public ResponseEntity<Object> listOfCourseHaveBeenPurchased() {
        List<CourseDTO> purchasedCourseList = courseRegistrationService.listOfCourseThatMenteePurchased();
        if (purchasedCourseList != null) {
            return new ResponseEntity<>(new BaseResponse<>(CommonString.OK_MESSAGES, HttpStatus.OK.value(), purchasedCourseList), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse("Bạn chưa đăng ký khóa học nào", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }


    @GetMapping("/filter-support")
    public ResponseEntity<Object> filterMentorShipRequest(@RequestParam(value = "status", required = false) List<String> status,
                                                          @RequestParam(value = "startDate", required = false) LocalDate startDate,
                                                          @RequestParam(value = "endDate", required = false) LocalDate endDate) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        List<SupportOfEachMentee> list = supportService.filterMentorshipRequestMentee(customUserDetails.getUsername(), status, startDate, endDate);
        if (!list.isEmpty()) {
            return new ResponseEntity<>(new BaseResponse<>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value(), list), HttpStatus.OK);
        }
        return new ResponseEntity<>(new BaseResponse<>("Không tim thấy yêu cầu hỗ trợ nào", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }
}
