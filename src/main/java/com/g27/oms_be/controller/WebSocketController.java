package com.g27.oms_be.controller;

import com.g27.oms_be.dto.socket.SocketMessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
@CrossOrigin(origins = "*")
public class WebSocketController {
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public WebSocketController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public void sendStartSessionMessage(SocketMessageDTO socketMessageDTO, String username) {
        this.simpMessagingTemplate.convertAndSend("/topic/sessionStart", socketMessageDTO);
        System.out.println("Socket is sent to " + username);
    }

    public void sendEndSessionMessage(SocketMessageDTO socketMessageDTO, String username) {
        this.simpMessagingTemplate.convertAndSend("/topic/sessionEnd", socketMessageDTO);
    }

    public void sendNotifySessionIsStarted(SocketMessageDTO socketMessageDTO) {
        this.simpMessagingTemplate.convertAndSend("/topic/notifySessionStart", socketMessageDTO);
    }
}
