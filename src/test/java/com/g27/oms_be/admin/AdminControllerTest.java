//package com.g27.oms_be.admin;
//
//import com.g27.oms_be.controller.AdminController;
//import com.g27.oms_be.dto.response.BaseResponse;
//import com.g27.oms_be.dto.skill.SkillDTO;
//import com.g27.oms_be.dto.user.admin.AddStaffDTO;
//import com.g27.oms_be.dto.user.admin.UserListDTO;
//import com.g27.oms_be.service.skill.SkillService;
//import com.g27.oms_be.service.user.UserService;
//import com.g27.oms_be.validate.Validator;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.validation.BindingResult;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.ArgumentMatchers.*;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//public class AdminControllerTest {
//    @Mock
//    private UserService userService;
//    @Mock
//    private SkillService skillService;
//    @Mock
//    private BindingResult bindingResult;
//    @Mock
//    private Validator validator;
//    @InjectMocks
//    private AdminController adminController;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.openMocks(this);
//    }
//
//    @Test
//    public void getAllUser_UsersExist_ReturnsOk() {
//        List<UserListDTO> userList = new ArrayList<>();
//        userList.add(new UserListDTO()); // Assuming UserListDTO can be instantiated like this
//        when(userService.getAllUser()).thenReturn(userList);
//
//        ResponseEntity<Object> response = adminController.getAllUser();
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertNotNull(response.getBody());
//    }
//
//    @Test
//    public void getAllUser_NoUsersExist_ReturnsNotFound() {
//        when(userService.getAllUser()).thenReturn(new ArrayList<>());
//
//        ResponseEntity<Object> response = adminController.getAllUser();
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertNotNull(response.getBody());
//    }
//
//    @Test
//    public void addStaff_ValidData_ReturnsCreated() {
//        AddStaffDTO addStaffDTO = new AddStaffDTO();
//        when(bindingResult.hasErrors()).thenReturn(false);
//        when(userService.existsByUserName(anyString())).thenReturn(false);
//        when(userService.existsByPhone(anyString())).thenReturn(false);
//        when(userService.addStaff(addStaffDTO)).thenReturn(addStaffDTO);
//
//        ResponseEntity<Object> response = adminController.addStaff(addStaffDTO, bindingResult);
//        assertEquals(HttpStatus.CREATED, response.getStatusCode());
//        assertNotNull(response.getBody());
//    }
//
//    @Test
//    public void addStaff_ValidationError_ReturnsBadRequest() {
//        BindingResult bindingResult = mock(BindingResult.class);
//        when(bindingResult.hasErrors()).thenReturn(true);
//
//        when(validator.checkBindingResult(eq(bindingResult), any()))
//                .thenReturn(new BaseResponse<>("Validation failed", 400, new ArrayList<>()));
//
//        ResponseEntity<Object> response = adminController.addStaff(new AddStaffDTO(), bindingResult);
//
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//        assertNotNull(response.getBody());
//    }
//
//    @Test
//    public void testUpdateStatus_Success() {
//        String username = "mentee1";
//        when(userService.updateStatus(username)).thenReturn("Status of user is updated");
//
//        ResponseEntity<Object> response = adminController.updateStatus(username);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertNotNull(response.getBody());
//    }
//
//    @Test
//    public void testSkillDetail_Success() {
//        String urlSkillId = "1";
//        Long skillId = 1L;
//        SkillDTO skillDTO = new SkillDTO();
//        when(skillService.skillDetail(skillId)).thenReturn(skillDTO);
//
//        ResponseEntity<Object> response = adminController.skillDetail(urlSkillId);
//
//        assertEquals(skillService.skillDetail(skillId), ((BaseResponse<?>) response.getBody()).getData());
//    }
//
//    @Test
//    public void testSkillDetail_Success2() {
//        String urlSkillId = "2";
//        Long skillId = 2L;
//        SkillDTO skillDTO = new SkillDTO();
//        when(skillService.skillDetail(skillId)).thenReturn(skillDTO);
//
//        ResponseEntity<Object> response = adminController.skillDetail(urlSkillId);
//
//        assertEquals(skillService.skillDetail(skillId), ((BaseResponse<?>) response.getBody()).getData());
//    }
//
//    @Test
//    public void testSkillDetail_BadRequest() {
//        String urlSkillId = "";
//
//        ResponseEntity<Object> response = adminController.skillDetail(urlSkillId);
//
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//        assertEquals("Thông tin đầu vào không hợp lệ", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testSkillDetail_BadRequest2() {
//        String urlSkillId = "invalidId";
//
//        ResponseEntity<Object> response = adminController.skillDetail(urlSkillId);
//
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//        assertEquals("Thông tin đầu vào không hợp lệ", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testSkillDetail_NonExistingSkill_ReturnsNotFound() {
//        Long nonExistingSkillId = 123L;
//        SkillService skillServiceMock = mock(SkillService.class);
//        when(skillServiceMock.skillDetail(nonExistingSkillId)).thenReturn(null);
//
//        ResponseEntity<Object> response = adminController.skillDetail(nonExistingSkillId.toString());
//
//        assertEquals("Không tìm thấy ngôn ngữ", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testSkillDetail_NonExistingSkill_ReturnsNotFound2() {
//        Long nonExistingSkillId = -123L;
//        SkillService skillServiceMock = mock(SkillService.class);
//        when(skillServiceMock.skillDetail(nonExistingSkillId)).thenReturn(null);
//
//        ResponseEntity<Object> response = adminController.skillDetail(nonExistingSkillId.toString());
//
//        assertEquals("Không tìm thấy ngôn ngữ", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//}
