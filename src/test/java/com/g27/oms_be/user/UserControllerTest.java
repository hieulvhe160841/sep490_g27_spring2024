//package com.g27.oms_be.user;
//
//import com.g27.oms_be.common.CommonString;
//import com.g27.oms_be.controller.UserController;
//import com.g27.oms_be.dto.course.CourseDTO;
//import com.g27.oms_be.dto.course.CourseDetailDTO;
//import com.g27.oms_be.dto.response.BaseResponse;
//import com.g27.oms_be.dto.user.common.UserDetailDTO;
//import com.g27.oms_be.service.course.CourseService;
//import com.g27.oms_be.service.support.SupportService;
//import com.g27.oms_be.service.user.UserService;
//import com.g27.oms_be.validate.Validator;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.validation.BindingResult;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//public class UserControllerTest {
//    @Mock
//    private UserService userService;
//    @Mock
//    private SupportService supportService;
//    @Mock
//    private CourseService courseService;
//    @Mock
//    private BindingResult bindingResult;
//    @Mock
//    private Validator validator;
//    @InjectMocks
//    private UserController userController;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.openMocks(this);
//    }
//
//    @Test
//    public void testCourseDetail_BadRequest() {
//        String courseId = "invalidId";
//
//        ResponseEntity<Object> response = userController.courseDetail(courseId);
//
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//        assertEquals("Dữ liệu không hợp lệ", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testCourseDetail_NotFound() {
//        String courseId = "123";
//        Long id = 123L;
//        when(courseService.getCourseDetail(id)).thenReturn(null);
//
//        ResponseEntity<Object> response = userController.courseDetail(courseId);
//
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertEquals("Khóa học không tồn tại", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testCourseDetail_Success() {
//        String courseId = "1";
//        Long id = 1L;
//        CourseDetailDTO courseDetailDTO = new CourseDetailDTO();
//        courseDetailDTO.setCourseId(2L);
//        when(courseService.getCourseDetail(id)).thenReturn(courseDetailDTO);
//
//        ResponseEntity<Object> response = userController.courseDetail(courseId);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals(HttpStatus.OK.getReasonPhrase(), ((BaseResponse<?>) response.getBody()).getMessages());
//        assertEquals(courseDetailDTO.getCourseId(), ((BaseResponse<CourseDetailDTO>) response.getBody()).getData().getCourseId());
//    }
//
//    @Test
//    public void testCourseDetail_Success2() {
//        String courseId = "3";
//        Long id = 3L;
//        CourseDetailDTO courseDetailDTO = new CourseDetailDTO();
//        courseDetailDTO.setCourseId(3L);
//        when(courseService.getCourseDetail(id)).thenReturn(courseDetailDTO);
//
//        ResponseEntity<Object> response = userController.courseDetail(courseId);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals(HttpStatus.OK.getReasonPhrase(), ((BaseResponse<?>) response.getBody()).getMessages());
//        assertEquals(courseDetailDTO.getCourseId(), ((BaseResponse<CourseDetailDTO>) response.getBody()).getData().getCourseId());
//    }
//
//    @Test
//    public void testFilterSearchCourse_Success() {
//        String mentorName = "van";
//        String courseName = "";
//        String fPrice = "100000";
//        String tPrice = "300000";
//        Double fromPrice = Double.parseDouble(fPrice);
//        Double toPrice = Double.parseDouble(tPrice);
//        List<CourseDTO> courseDTOS = courseService.filterCourse(mentorName, courseName, fromPrice, toPrice);
//        courseDTOS.add(new CourseDTO(1L, "course1", "", 100000D, "", ""));
//        courseDTOS.add(new CourseDTO(2L, "course2", "", 200000D, "", ""));
//
//        when(courseService.filterCourse(mentorName, courseName, fromPrice, toPrice)).thenReturn(courseDTOS);
//
//        ResponseEntity<Object> response = userController.filterSearchCourse(mentorName, courseName, fPrice, tPrice);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals(CommonString.OK_MESSAGES, ((BaseResponse<?>) response.getBody()).getMessages());
//        assertEquals(courseDTOS, ((BaseResponse<List<CourseDTO>>) response.getBody()).getData());
//        assertNotNull(response.getBody());
//    }
//
//    @Test
//    public void testFilterSearchCourse_NotFound() {
//        String mentorName = "testMentor";
//        String courseName = "testCourse";
//        String fPrice = "100000";
//        String tPrice = "300000";
//        Double fromPrice = Double.parseDouble(fPrice);
//        Double toPrice = Double.parseDouble(tPrice);
//        when(courseService.filterCourse(mentorName, courseName, fromPrice, toPrice)).thenReturn(new ArrayList<>());
//
//        ResponseEntity<Object> response = userController.filterSearchCourse(mentorName, courseName, fPrice, tPrice);
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertEquals("Không tìm thấy khóa học !", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testFilterSearchCourse_InvalidData() {
//        String mentorName = "";
//        String courseName = "";
//        String fromPrice = "invalid";
//        String toPrice = "100";
//
//        ResponseEntity<Object> response = userController.filterSearchCourse(mentorName, courseName, fromPrice,
//                toPrice);
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//        assertEquals("Dữ liệu không hợp lệ !", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testCourseByMentor_Success() {
//        String username = "mentor";
//        UserDetailDTO userDetailDTO = new UserDetailDTO();
//        List<CourseDTO> courseDTOS = new ArrayList<>();
//        courseDTOS.add(new CourseDTO(/* setup courseDTO here */));
//        when(userService.getUserDetailByUserName(username)).thenReturn(userDetailDTO);
//        when(courseService.getCoursesOfMentor(username)).thenReturn(courseDTOS);
//
//        ResponseEntity<Object> response = userController.courseByMentor(username);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals(HttpStatus.OK.getReasonPhrase(), ((BaseResponse<?>) response.getBody()).getMessages());
//        assertEquals(courseDTOS, ((BaseResponse<List<CourseDTO>>) response.getBody()).getData());
//    }
//
//    @Test
//    public void testCourseByMentor_Success2() {
//        String username = "mentor1";
//        UserDetailDTO userDetailDTO = new UserDetailDTO();
//        List<CourseDTO> courseDTOS = new ArrayList<>();
//        courseDTOS.add(new CourseDTO());
//        when(userService.getUserDetailByUserName(username)).thenReturn(userDetailDTO);
//        when(courseService.getCoursesOfMentor(username)).thenReturn(courseDTOS);
//
//        ResponseEntity<Object> response = userController.courseByMentor(username);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals(HttpStatus.OK.getReasonPhrase(), ((BaseResponse<?>) response.getBody()).getMessages());
//        assertEquals(courseDTOS, ((BaseResponse<List<CourseDTO>>) response.getBody()).getData());
//    }
//
//    @Test
//    public void testCourseByMentor_NoCoursesFound() {
//        String username = "existingUser";
//        UserDetailDTO userDetailDTO = new UserDetailDTO();
//        when(userService.getUserDetailByUserName(username)).thenReturn(userDetailDTO);
//        when(courseService.getCoursesOfMentor(username)).thenReturn(null);
//
//        ResponseEntity<Object> response = userController.courseByMentor(username);
//
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertEquals("Gia sư này không có khóa học nào!", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testCourseByMentor_UserNotFound() {
//        String username = "nonExistentUser";
//        when(userService.getUserDetailByUserName(username)).thenReturn(null);
//
//        ResponseEntity<Object> response = userController.courseByMentor(username);
//
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//        assertEquals("Người dùng không tồn tại!", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testCourseByMentor_UserNotFound2() {
//        String username = "nonExistentUser";
//        when(userService.getUserDetailByUserName(username)).thenReturn(null);
//
//        ResponseEntity<Object> response = userController.courseByMentor(username);
//
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//        assertEquals("Người dùng không tồn tại!", ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//}
//
//
