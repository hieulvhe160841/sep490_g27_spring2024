//package com.g27.oms_be.mentor;
//
//import com.g27.oms_be.common.CommonString;
//import com.g27.oms_be.controller.MentorController;
//import com.g27.oms_be.dto.courseRequest.CourseRequestDetailDTO;
//import com.g27.oms_be.dto.post.ViewPostDetailForMentorDTO;
//import com.g27.oms_be.dto.response.BaseResponse;
//import com.g27.oms_be.dto.support.ViewSupportDetailDTO;
//import com.g27.oms_be.service.courseRequest.CourseRequestService;
//import com.g27.oms_be.service.mentorRequest.MentorRequestService;
//import com.g27.oms_be.service.post.PostService;
//import com.g27.oms_be.service.support.SupportService;
//import com.g27.oms_be.service.user.UserService;
//import com.g27.oms_be.validate.Validator;
//import jakarta.validation.constraints.Positive;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.validation.BindingResult;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.*;
//
//public class MentorControllerTest {
//    @Mock
//    private UserService userService;
//    @Mock
//    private SupportService supportService;
//    @Mock
//    private CourseRequestService courseRequestService;
//    @Mock
//    private BindingResult bindingResult;
//    @Mock
//    private Validator validator;
//    @InjectMocks
//    private MentorController mentorController;
//    @Mock
//    private MentorRequestService mentorRequestService;
//    @Mock
//    private PostService postService;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.openMocks(this);
//    }
//
//    @Test
//    public void testSupportDetail_WhenSupportExists1() {
//        ViewSupportDetailDTO mockSupportDetail = new ViewSupportDetailDTO();
//        String urlSupportId = "1";
//        Long supportId = 1L;
//
//        when(supportService.viewSupportDetailForMentor(supportId)).thenReturn(mockSupportDetail);
//
//        ResponseEntity<Object> responseEntity = mentorController.supportDetail(urlSupportId);
//
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertNotNull(responseEntity.getBody());
//    }
//
//    @Test
//    public void testSupportDetail_WhenSupportExists2() {
//        ViewSupportDetailDTO mockSupportDetail = new ViewSupportDetailDTO();
//        String urlSupportId = "2";
//        Long supportId = 2L;
//        when(supportService.viewSupportDetailForMentor(supportId)).thenReturn(mockSupportDetail);
//
//        ResponseEntity<Object> responseEntity = mentorController.supportDetail(urlSupportId);
//
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertNotNull(responseEntity.getBody());
//    }
//
//    @Test
//    public void testSupportDetail_WhenSupportDoesNotExist1() {
//        String urlSupportId = "-2";
//        Long supportId = -2L;
//        when(supportService.viewSupportDetailForMentor(supportId)).thenReturn(null);
//        ResponseEntity<Object> response = mentorController.supportDetail(urlSupportId);
//        assertEquals("Không tìm thấy phiên gia sư phù hợp", ((BaseResponse<?>) response.getBody()).getMessages());
//        assertNull(((BaseResponse<?>) response.getBody()).getData());
//    }
//
//    @Test
//    public void testSupportDetail_WhenSupportDoesNotExist2() {
//        String urlSupportId = "0";
//        Long supportId = 0L;
//        when(supportService.viewSupportDetailForMentor(supportId)).thenReturn(null);
//        ResponseEntity<Object> response = mentorController.supportDetail(urlSupportId);
//        assertEquals("Không tìm thấy phiên gia sư phù hợp", ((BaseResponse<?>) response.getBody()).getMessages());
//        assertNull(((BaseResponse<?>) response.getBody()).getData());
//    }
//
//    @Test
//    public void testSupportDetail_WithInvalidInput() {
//        String urlSupportId = "abc";
//        ResponseEntity<Object> response = mentorController.supportDetail(urlSupportId);
//
//        assertEquals("Thông tin đầu vào không hợp lệ", ((BaseResponse<?>) response.getBody()).getMessages());
//        assertNotNull(response.getBody());
//    }
//
//
//    @Test
//    void viewCourseRequestDetail_ValidCourseRequestId_ReturnsCourseRequestDetail1() {
//        String urlCourseRequestId = "1";
//        Long courseRequestId = Long.parseLong(urlCourseRequestId);
//        CourseRequestDetailDTO courseRequestDetailDTO = new CourseRequestDetailDTO();
//        when(courseRequestService.viewCourseRequestDetailForMentor(courseRequestId)).thenReturn(courseRequestDetailDTO);
//
//        ResponseEntity<Object> responseEntity = mentorController.viewCourseRequestDetail(urlCourseRequestId);
//
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(courseRequestDetailDTO, ((BaseResponse<?>) responseEntity.getBody()).getData());
//        verify(courseRequestService, times(1)).viewCourseRequestDetailForMentor(courseRequestId);
//    }
//
//    @Test
//    void viewCourseRequestDetail_ValidCourseRequestId_ReturnsCourseRequestDetail2() {
//        String urlCourseRequestId = "2";
//        Long courseRequestId = Long.parseLong(urlCourseRequestId);
//        CourseRequestDetailDTO courseRequestDetailDTO = new CourseRequestDetailDTO();
//        when(courseRequestService.viewCourseRequestDetailForMentor(courseRequestId)).thenReturn(courseRequestDetailDTO);
//
//        ResponseEntity<Object> responseEntity = mentorController.viewCourseRequestDetail(urlCourseRequestId);
//
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(courseRequestDetailDTO, ((BaseResponse<?>) responseEntity.getBody()).getData());
//    }
//
//    @Test
//    void viewCourseRequestDetail_InvalidCourseRequestId_ReturnsBadRequest() {
//        String urlCourseRequestId = "invalidId";
//        ResponseEntity<Object> responseEntity = mentorController.viewCourseRequestDetail(urlCourseRequestId);
//
//        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
//        verifyNoInteractions(courseRequestService);
//    }
//
//    @Test
//    void viewCourseRequestDetail_NonexistentCourseRequest_ReturnsNotFound() {
//        String urlCourseRequestId = "123";
//        Long courseRequestId = Long.parseLong(urlCourseRequestId);
//        when(courseRequestService.viewCourseRequestDetailForMentor(courseRequestId)).thenReturn(null);
//
//        ResponseEntity<Object> responseEntity = mentorController.viewCourseRequestDetail(urlCourseRequestId);
//
//        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
//        verify(courseRequestService, times(1)).viewCourseRequestDetailForMentor(courseRequestId);
//    }
//    @Test
//    public void testViewPostDetailForMentor_WithValidPostId_ReturnsPostDetail() {
//        Long postId = 1L;
//        ViewPostDetailForMentorDTO postDetail = new ViewPostDetailForMentorDTO();
//        when(postService.viewPostDetailForMentorByPostId(postId)).thenReturn(postDetail);
//
//        ResponseEntity<Object> response = mentorController.viewPostDetailForMentor("1");
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals(CommonString.OK_MESSAGES, ((BaseResponse) response.getBody()).getMessages());
//        assertEquals(HttpStatus.OK.value(), ((BaseResponse) response.getBody()).getStatusCode());
//        assertEquals(postDetail, ((BaseResponse) response.getBody()).getData());
//    }
//    @Test
//    public void testViewPostDetailForMentor_WithInvalidPostId_ReturnsBadRequestResponse() {
//        when(postService.viewPostDetailForMentorByPostId(Mockito.anyLong())).thenReturn(null);
//
//        ResponseEntity<Object> response = mentorController.viewPostDetailForMentor("invalidId");
//
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//        assertEquals("Thông tin đầu vào không hợp lệ", ((BaseResponse) response.getBody()).getMessages());
//        assertEquals(HttpStatus.BAD_REQUEST.value(), ((BaseResponse) response.getBody()).getStatusCode());
//    }
//    @Test
//    public void testViewPostDetailForMentor_WithNonExistentPostId_ReturnsNotFoundResponse() {
//        Long postId = 0L;
//        when(postService.viewPostDetailForMentorByPostId(postId)).thenReturn(null);
//
//        ResponseEntity<Object> response = mentorController.viewPostDetailForMentor("0");
//
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertEquals("Không tìm thấy bài đăng phù hợp nào", ((BaseResponse) response.getBody()).getMessages());
//        assertEquals(HttpStatus.NOT_FOUND.value(), ((BaseResponse) response.getBody()).getStatusCode());
//    }
//}
