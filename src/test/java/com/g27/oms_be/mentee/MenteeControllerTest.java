//package com.g27.oms_be.mentee;
//
//import com.g27.oms_be.common.CommonString;
//import com.g27.oms_be.controller.MenteeController;
//import com.g27.oms_be.dto.mentorRequest.MenteeViewOwnRegisAsMentorDTO;
//import com.g27.oms_be.dto.response.BaseResponse;
//import com.g27.oms_be.dto.support.ConfirmStartSupportDTO;
//import com.g27.oms_be.dto.support.RatingSupportDTO;
//import com.g27.oms_be.dto.user.common.BookMentorDTO;
//import com.g27.oms_be.dto.user.mentor.MentorSkillDTO;
//import com.g27.oms_be.entities.MentorRequest;
//import com.g27.oms_be.repository.MentorRequestRepository;
//import com.g27.oms_be.security.CustomUserDetails;
//import com.g27.oms_be.service.mentorRequest.MentorRequestService;
//import com.g27.oms_be.service.post.PostService;
//import com.g27.oms_be.service.support.SupportService;
//import com.g27.oms_be.service.userSkill.UserSkillService;
//import com.g27.oms_be.validate.Validator;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.validation.BindingResult;
//
//import java.util.*;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyString;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//public class MenteeControllerTest {
//    @Mock
//    private UserSkillService userSkillService;
//    @Mock
//    private SupportService supportService;
//    @Mock
//    private PostService postService;
//    @Mock
//    private BindingResult bindingResult;
//    @Mock
//    private Validator validator;
//    @InjectMocks
//    private MenteeController menteeController;
//    @Mock
//    private MentorRequestService mentorRequestService;
//    @Mock
//    private MentorRequestRepository mentorRequestRepository;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.openMocks(this);
//    }
//
//    @Test
//    public void testConfirmStart_InvalidInput() {
//        String id = "invalidId";
//        ResponseEntity<Object> response = menteeController.confirmStart(id);
//
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertEquals(CommonString.INVALID_INPUT, ((BaseResponse<?>) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testConfirmStart_NotFound() {
//        String id = "123";
//        Long supportId = 123L;
//        when(supportService.menteeConfirmStart(supportId)).thenReturn(null);
//
//        ResponseEntity<Object> response = menteeController.confirmStart(id);
//
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertEquals("Support is not exist", ((BaseResponse<?>) response.getBody()).getData());
//    }
//
//    @Test
//    public void testConfirmStart_NotFound2() {
//        String id = "-1";
//        Long supportId = -1L;
//        when(supportService.menteeConfirmStart(supportId)).thenReturn(null);
//
//        ResponseEntity<Object> response = menteeController.confirmStart(id);
//
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertEquals("Support is not exist", ((BaseResponse<?>) response.getBody()).getData());
//    }
//
//    @Test
//    public void testConfirmStart_Success() {
//        String id = "1";
//        Long supportId = 1L;
//        ConfirmStartSupportDTO confirmStartSupportDTO = new ConfirmStartSupportDTO();
//        when(supportService.menteeConfirmStart(supportId)).thenReturn(confirmStartSupportDTO);
//
//        ResponseEntity<Object> response = menteeController.confirmStart(id);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals(confirmStartSupportDTO, ((BaseResponse<ConfirmStartSupportDTO>) response.getBody()).getData());
//    }
//
//    @Test
//    public void testConfirmStart_Success2() {
//        String id = "2";
//        Long supportId = 2L;
//        ConfirmStartSupportDTO confirmStartSupportDTO = new ConfirmStartSupportDTO();
//        when(supportService.menteeConfirmStart(supportId)).thenReturn(confirmStartSupportDTO);
//
//        ResponseEntity<Object> response = menteeController.confirmStart(id);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertEquals(confirmStartSupportDTO, ((BaseResponse<ConfirmStartSupportDTO>) response.getBody()).getData());
//    }
//
//    @Test
//    void getMentorRequestOfMentee_ReturnsEmptyList_WhenNoRequestsFound1() {
//        String username = "menteeHasNoRequest";
//        when(mentorRequestRepository.findRegisAsMentorRequestByMentee(username)).thenReturn(new ArrayList<>());
//
//        List<MenteeViewOwnRegisAsMentorDTO> result = mentorRequestService.getMentorRequestOfMentee(username);
//
//        assertNotNull(result);
//        assertTrue(result.isEmpty());
//    }
//
//    @Test
//    void getMentorRequestOfMentee_ReturnsEmptyList_WhenNoRequestsFound2() {
//        String username = "";
//        Mockito.when(mentorRequestRepository.findRegisAsMentorRequestByMentee(username)).thenReturn(new ArrayList<>());
//
//        List<MenteeViewOwnRegisAsMentorDTO> result = mentorRequestService.getMentorRequestOfMentee(username);
//
//        assertNotNull(result);
//        assertTrue(result.isEmpty());
//    }
//
//    @Test
//    void getMentorRequestOfMentee_ReturnsList_WhenRequestsFound1() {
//        String username = "mentee";
//        List<MentorRequest> mentorRequests = new ArrayList<>();
//        MentorRequest request1 = new MentorRequest();
//        request1.setMentorRequestId(1L);
//        request1.setMentorRequestFeedback("Feedback 1");
//        request1.setMentorRequestStatus("Pending");
//        mentorRequests.add(request1);
//        MentorRequest request2 = new MentorRequest();
//        request2.setMentorRequestId(2L);
//        request2.setMentorRequestFeedback("Feedback 2");
//        request2.setMentorRequestStatus("Approved");
//        mentorRequests.add(request2);
//
//        when(mentorRequestRepository.findRegisAsMentorRequestByMentee(username)).thenReturn(mentorRequests);
//
//        List<MenteeViewOwnRegisAsMentorDTO> result = mentorRequestService.getMentorRequestOfMentee(username);
//
//        assertNotNull(result);
//    }
//
//    @Test
//    void getMentorRequestOfMentee_ReturnsList_WhenRequestsFound2() {
//        String username = "newMentee";
//        List<MentorRequest> mentorRequests = new ArrayList<>();
//        MentorRequest request1 = new MentorRequest();
//        request1.setMentorRequestId(1L);
//        request1.setMentorRequestFeedback("Feedback 1");
//        request1.setMentorRequestStatus("Pending");
//        mentorRequests.add(request1);
//
//        when(mentorRequestRepository.findRegisAsMentorRequestByMentee(username)).thenReturn(mentorRequests);
//
//        List<MenteeViewOwnRegisAsMentorDTO> result = mentorRequestService.getMentorRequestOfMentee(username);
//
//        assertNotNull(result);
//    }
//
//    @Test
//    public void testRegisterAsMentorSkill_ValidInput_ReturnsCreated1() {
//        MentorSkillDTO mentorSkillDTO = new MentorSkillDTO();
//        mentorSkillDTO.setUserName("mentee1");
//        mentorSkillDTO.setExperience("experience1");
//        mentorSkillDTO.setEducationLevel("educationLevel1");
//        Set<String> skills = new HashSet<>();
//        skills.add("English");
//        skills.add("Korean");
//        mentorSkillDTO.setSkills(skills);
//        mentorSkillDTO.setCostPerHour(50.0);
//
//
//        BindingResult bindingResult = mock(BindingResult.class);
//        when(bindingResult.hasErrors()).thenReturn(false);
//
//        when(mentorRequestService.checkExistWaitingMentorRequest(anyString())).thenReturn(false);
//
//        when(userSkillService.registerAsMentor(any(MentorSkillDTO.class))).thenReturn(mentorSkillDTO);
//
//        // Act
//        ResponseEntity<Object> response = menteeController.registerAsMentorSkill(mentorSkillDTO, bindingResult);
//
//        // Assert
//        assertEquals(HttpStatus.CREATED, response.getStatusCode());
//        assertEquals("Tạo yêu cầu gia sư thành công!", ((BaseResponse) response.getBody()).getMessages());
//        assertEquals(mentorSkillDTO, ((BaseResponse) response.getBody()).getData());
//    }
//
//    @Test
//    public void testRegisterAsMentorSkill_ValidInput_ReturnsCreated2() {
//        MentorSkillDTO mentorSkillDTO = new MentorSkillDTO();
//        mentorSkillDTO.setUserName("mentee2");
//        mentorSkillDTO.setExperience("experience2");
//        mentorSkillDTO.setEducationLevel("educationLevel2");
//        Set<String> skills = new HashSet<>();
//        skills.add("French");
//        mentorSkillDTO.setSkills(skills);
//        mentorSkillDTO.setCostPerHour(150.0);
//
//        BindingResult bindingResult = mock(BindingResult.class);
//        when(bindingResult.hasErrors()).thenReturn(false);
//
//        when(mentorRequestService.checkExistWaitingMentorRequest(anyString())).thenReturn(false);
//
//        when(userSkillService.registerAsMentor(any(MentorSkillDTO.class))).thenReturn(mentorSkillDTO);
//
//        ResponseEntity<Object> response = menteeController.registerAsMentorSkill(mentorSkillDTO, bindingResult);
//
//        assertEquals(HttpStatus.CREATED, response.getStatusCode());
//        assertEquals("Tạo yêu cầu gia sư thành công!", ((BaseResponse) response.getBody()).getMessages());
//        assertEquals(mentorSkillDTO, ((BaseResponse) response.getBody()).getData());
//    }
//
//    @Test
//    public void testRegisterAsMentorSkill_MentorRequestExists_ReturnsBadRequest() {
//        MentorSkillDTO mentorSkillDTO = new MentorSkillDTO();
//        mentorSkillDTO.setUserName("mentee3");
//        mentorSkillDTO.setExperience("experience3");
//        mentorSkillDTO.setEducationLevel("educationLevel3");
//        Set<String> skills = new HashSet<>();
//        skills.add("English");
//        skills.add("Korean");
//        mentorSkillDTO.setSkills(skills);
//        mentorSkillDTO.setCostPerHour(50.0);
//
//        BindingResult bindingResult = mock(BindingResult.class);
//        when(bindingResult.hasErrors()).thenReturn(false);
//
//        when(mentorRequestService.checkExistWaitingMentorRequest(anyString())).thenReturn(true);
//
//        ResponseEntity<Object> response = menteeController.registerAsMentorSkill(mentorSkillDTO, bindingResult);
//
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//        assertEquals("Bạn đang có 1 yêu cầu với gia sư này !", ((BaseResponse) response.getBody()).getMessages());
//    }
//
//    @Test
//    public void testRegisterAsMentorSkill_InvalidInput_ReturnsBadRequest() {
//        MentorSkillDTO mentorSkillDTO = new MentorSkillDTO();
//        mentorSkillDTO.setUserName("mentee4");
//        mentorSkillDTO.setExperience("experience4");
//        mentorSkillDTO.setEducationLevel("educationLevel4");
//        Set<String> skills = new HashSet<>();
//        skills.add("French");
//        skills.add("Korean");
//        mentorSkillDTO.setSkills(skills);
//        mentorSkillDTO.setCostPerHour(-50.0);
//
//        BindingResult bindingResult = mock(BindingResult.class);
//        when(bindingResult.hasErrors()).thenReturn(true);
//
//        ResponseEntity<Object> response = menteeController.registerAsMentorSkill(mentorSkillDTO, bindingResult);
//        assertNotNull(((BaseResponse) response.getBody()).getData());
//    }
//
//    @Test
//    void ratingForEachSupport_ValidSupportId_ReturnsCreatedResponse1() {
//        String supportId = "5";
//        Long parsedSupportId = 5L;
//        RatingSupportDTO ratingSupportDTO = new RatingSupportDTO();
//        ratingSupportDTO.setRating(4D);
//        ratingSupportDTO.setFeedback("good!");
//        when(supportService.ratingForSupportBySupportId(parsedSupportId, ratingSupportDTO.getRating(), ratingSupportDTO.getFeedback())).thenReturn(true);
//
//        ResponseEntity<Object> responseEntity = menteeController.ratingForEachSupport(supportId, ratingSupportDTO);
//
//        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
//        assertEquals(ratingSupportDTO, ((BaseResponse) responseEntity.getBody()).getData());
//    }
//
//    @Test
//    void ratingForEachSupport_ValidSupportId_ReturnsCreatedResponse2() {
//        String urlSupportId = "1";
//        Long parsedSupportId = 1L;
//        RatingSupportDTO ratingSupportDTO = new RatingSupportDTO();
//        ratingSupportDTO.setRating(3D);
//        ratingSupportDTO.setFeedback("good!");
//        when(supportService.ratingForSupportBySupportId(parsedSupportId, ratingSupportDTO.getRating(), ratingSupportDTO.getFeedback())).thenReturn(true);
//
//        ResponseEntity<Object> responseEntity = menteeController.ratingForEachSupport(urlSupportId, ratingSupportDTO);
//
//        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
//        assertEquals(ratingSupportDTO, ((BaseResponse) responseEntity.getBody()).getData());
//    }
//
//    @Test
//    void ratingForEachSupport_InvalidSupportId_ReturnsBadRequest1() {
//        String urlSupportId = "abc";
//        RatingSupportDTO ratingSupportDTO = new RatingSupportDTO();
//        ratingSupportDTO.setRating(3D);
//        ratingSupportDTO.setFeedback("good!");
//        ResponseEntity<Object> responseEntity = menteeController.ratingForEachSupport(urlSupportId, ratingSupportDTO);
//
//        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
//        assertEquals("Thông tin đầu vào không hợp lệ", ((BaseResponse) responseEntity.getBody()).getMessages());
//    }
//
//    @Test
//    void ratingForEachSupport_NotFoundSupport_ReturnsNotFoundResponse() {
//        String urlSupportId = "12345678910";
//        Long parsedSupportId = 12345678910L;
//        RatingSupportDTO ratingSupportDTO = new RatingSupportDTO();
//        when(supportService.ratingForSupportBySupportId(parsedSupportId, ratingSupportDTO.getRating(), ratingSupportDTO.getFeedback())).thenReturn(false);
//
//        ResponseEntity<Object> responseEntity = menteeController.ratingForEachSupport(urlSupportId, ratingSupportDTO);
//
//        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
//        assertEquals("Tạo đánh giá thất bại do không tìm thấy phiên trợ giúp phù hợp", ((BaseResponse) responseEntity.getBody()).getMessages());
//    }
//    @Test
//    void listBookMentor_ReturnsOk() {
//        String username = "testUser";
//        CustomUserDetails userDetails = new CustomUserDetails();
//        userDetails.setUserName(username);
//        Authentication authentication = mock(Authentication.class);
//        SecurityContext securityContext = mock(SecurityContext.class);
//
//        when(authentication.getPrincipal()).thenReturn(userDetails);
//        when(securityContext.getAuthentication()).thenReturn(authentication);
//        SecurityContextHolder.setContext(securityContext);
//
//        List<BookMentorDTO> bookMentorDTOS = Collections.singletonList(new BookMentorDTO());
//        when(supportService.getListBookedMentor(username)).thenReturn(bookMentorDTOS);
//
//        ResponseEntity<Object> responseEntity = menteeController.listBookMentor();
//
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//    }
//
//}
