//package com.g27.oms_be.payment;
//
//import com.g27.oms_be.controller.PaymentController;
//import com.g27.oms_be.dto.denomination.DenominationDTO;
//import com.g27.oms_be.dto.response.BaseResponse;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.mockito.Mockito.when;
//
//public class PaymentControllerTest {
//    @Mock
//    private Payment paymentService;
//
//    @InjectMocks
//    private PaymentController paymentController;
//
//    @BeforeEach
//    public void init() {
//        MockitoAnnotations.openMocks(this);
//    }
//
//    @Test
//    public void createPayment_Successful_Returns200() {
//        DenominationDTO mockDto = new DenominationDTO();
//        mockDto.setDenominationId(1L);
//        mockDto.setValue(50.0);
//        mockDto.setAmount(100.0);
//
//        when(paymentService.createPayment(mockDto)).thenReturn(new Object());
//
//        ResponseEntity<Object> response = paymentController.createPayment(mockDto);
//
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertNotNull(response.getBody());
//
//    }
//
//    @Test
//    public void createPayment_Failure_Returns500() {
//
//        DenominationDTO mockDto = new DenominationDTO();
//        mockDto.setDenominationId(1L);
//        mockDto.setValue(50.0);
//        mockDto.setAmount(100.0);
//
//        when(paymentService.createPayment(mockDto)).thenReturn(null);
//
//
//        ResponseEntity<Object> response = paymentController.createPayment(mockDto);
//
//
//        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
//        assertNotNull(response.getBody());
//
//    }
//
//
//    @Test
//    public void createPayment_InvalidInput_ReturnsBadRequest() {
//        DenominationDTO mockDto = new DenominationDTO();  // Assuming validation fails due to missing or invalid fields
//        when(paymentService.createPayment(mockDto)).thenThrow(new IllegalArgumentException("Invalid input"));
//
//        ResponseEntity<Object> response = null;
//        try {
//            response = paymentController.createPayment(mockDto);
//        } catch (IllegalArgumentException e) {
//            response = new ResponseEntity<>(new BaseResponse<>("Invalid input", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
//        }
//
//        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
//    }
//
//    @Test
//    public void createPayment_ServiceThrowsException_ReturnsInternalServerError() {
//        DenominationDTO mockDto = new DenominationDTO();
//        mockDto.setDenominationId(1L);
//        mockDto.setValue(50.0);
//        mockDto.setAmount(100.0);
//
//        when(paymentService.createPayment(mockDto)).thenThrow(new RuntimeException("Unexpected error"));
//
//        ResponseEntity<Object> response = null;
//        try {
//            response = paymentController.createPayment(mockDto);
//        } catch (RuntimeException e) {
//            response = new ResponseEntity<>(new BaseResponse<>("Internal server error", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//
//        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
//    }
//}
